<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class Database{
	
	public function __construct(){
		$this->Config = require 'Config.php';
		$this->databaseConfig = $this->Config['database'];
		$this->defaultValue = $this->databaseConfig['defaultValue'];
		$this->db = null;
	}
	
	private function openConnection(){
		$dsn = $this->databaseConfig['driver'] . 
			   ':host=' . $this->databaseConfig['host'] . 
			   ';port=' . $this->databaseConfig['port'] . 
			   ';dbname=' . $this->databaseConfig['dbname'];	
		$opt[PDO::ATTR_PERSISTENT] = $this->databaseConfig['persistent'];
		try{$this->db = new PDO($dsn, $this->databaseConfig['user'], $this->databaseConfig['password'], $opt);}
		catch(PDOexception $err){
			// echo $this->databaseConfig['errorMsg'];
			echo $err->getMessage();
			die;
		}
	}
	
	private function closeConnection(){
		$this->db = null;
	}
	
	private function sql_debug($sql_string, array $params = null){
		if(!empty($params)){
			$indexed = $params == array_values($params);
			foreach($params as $k=>$v){
				if (is_object($v)){
					if ($v instanceof \DateTime) $v = $v->format('Y-m-d H:i:s');
					else continue;
				}
				else if(is_string($v)) $v="'$v'";
				else if($v === null) $v='NULL';
				else if(is_array($v)) $v = implode(',', $v);
	
				if($indexed){
					$sql_string = preg_replace('/\?/', $v, $sql_string, 1);
				}
				else{
					if($k[0] != ':') $k = ':'.$k;
					$sql_string = str_replace($k,$v,$sql_string);
				}
			}
		}
		return $sql_string;
	}
	
	public function getDataTabel($tabel, $id = array()){
		set_time_limit(0);
		if(!empty($id)){
			$data = $this->getData('SELECT * FROM '.$tabel.' WHERE ('.$id[0].' = ?) ', array($id[1]));
			if($data['count'] > 0) $result =  $data['value'][0];
			else $result = $this->getTabel($tabel);
		}
		else
			$result = $this->getTabel($tabel);
		return $result;
	}
	
	public function getTabel($tabel){
		$result = $this->getData('SHOW COLUMNS FROM '.$tabel);
		$defaultValue = $this->defaultValue;
		$dataTabel = array();
		foreach($result['value'] as $kol){$dataTabel[$kol['Field']] = '';}
		foreach($dataTabel as $key => $value){if(isset($defaultValue[$key])) $dataTabel[$key] = $defaultValue[$key];}
		return $dataTabel;
    }
	
	public function getData($query, $arrData = array()){
		if(is_null($this->db)) $this->openConnection();
		$sql_stat = $this->db->prepare($query);
		$sql_stat->execute($arrData);
		$sql_value = $sql_stat->fetchAll(PDO::FETCH_ASSOC);
		$sql_count = $sql_stat->rowCount();		
		$sql_query = $this->sql_debug($query, $arrData);
		$this->closeConnection();
		return array(
			'value' => $sql_value,
			'count' => $sql_count,
			'query' => $sql_query.';',
		);
	}
	
	public function save($tabel, $arrData){
		if(is_null($this->db)) $this->openConnection();
		foreach($arrData as $key => $value) $keys[] = ':' . $key;
		$valTable = implode(', ',$keys);
		$query = 'INSERT INTO ' . $tabel . ' VALUES (' . $valTable . ')';
		$error = 0;
		$message = '';
		try{
			$sql_stat = $this->db->prepare($query);
			$error = $sql_stat->execute($arrData);
			$message = $sql_stat->errorInfo()[2];
		}
		catch(Exception $err){}
		$this->closeConnection();
		$sql_query = $this->sql_debug($query, $arrData);
		return array(
			'error' => $error,
			'message' => $message,
			'query' => $sql_query.';',
		);
	}
	
	public function save_update($tabel, $arrData){
		if(is_null($this->db)) $this->openConnection();
		foreach($arrData as $key => $value) $keys[] = $key . '= :' . $key;
		$valTable = implode(', ',$keys);
		$query = 'INSERT INTO ' . $tabel . ' SET ' . $valTable . ' ON DUPLICATE KEY UPDATE ' . $valTable;
		$error = 0;
		$message = '';
		try{
			$sql_stat = $this->db->prepare($query);
			$error = $sql_stat->execute($arrData);
			$message = $sql_stat->errorInfo()[2];
		}
		catch(Exception $err){}
		$this->closeConnection();
		$sql_query = $this->sql_debug($query, $arrData);
		return array(
			'error' => $error,
			'message' => $message,
			'query' => $sql_query.';',
		);
	}
	
	public function save_update_berita($tabel, $arrData){
		if(is_null($this->db)) $this->openConnection();
		foreach($arrData as $key => $value) $keys[] = $key . '= :' . $key;
		$valTable = implode(', ',$keys);
		$query = 'INSERT INTO ' . $tabel . ' SET ' . $valTable . ' ON DUPLICATE KEY UPDATE ' . $valTable;
		$error = 0;
		$message = '';
		try{
			$sql_stat = $this->db->prepare($query);
			$sql_stat->bindParam(':isi_berita', $arrData['isi_berita'], PDO::PARAM_STR);
			$error = $sql_stat->execute($arrData);
			$message = $sql_stat->errorInfo()[2];
		}
		catch(Exception $err){}
		$this->closeConnection();
		$sql_query = $this->sql_debug($query, $arrData);
		return array(
			'error' => $error,
			'message' => $message,
			'query' => $sql_query.';',
		);
	}
	
	public function update($tabel, $arrData, $idKey){
		if(is_null($this->db)) $this->openConnection();
		foreach($arrData as $key => $value) $keys1[] = $key . ' = :' . $key;
		$valTable = implode(', ',$keys1);
		foreach($idKey as $key => $value) $keys2[] = '(' . $key . '= :' . $key .')';
		$keyTable = implode(' AND ',$keys2);
		$query = 'UPDATE ' . $tabel . ' SET ' . $valTable . ' WHERE ' . $keyTable;
		$error = 0;
		$message = '';
		try{
			$sql_stat = $this->db->prepare($query);
			$error = $sql_stat->execute(array_merge($arrData, $idKey));
			$message = $sql_stat->errorInfo()[2];
		}
		catch(Exception $err){}
		$this->closeConnection();
		$sql_query = $this->sql_debug($query, array_merge($arrData, $idKey));
		return array(
			'error' => $error,
			'message' => $message,
			'query' => $sql_query.';',
		);
	}
	
	public function delete($tabel, $idKey){
		if(is_null($this->db)) $this->openConnection();
		foreach($idKey as $key => $value) $keys[] = '(' . $key . '= :' . $key .')';
		$keyTable = implode(' AND ',$keys);
		$query = 'DELETE FROM ' . $tabel . ' WHERE ' . $keyTable;
		$error = 0;
		$message = '';
		try{
			$sql_stat = $this->db->prepare($query);
			$error = $sql_stat->execute($idKey);
			$message = $sql_stat->errorInfo()[2];
		}
		catch(Exception $err){}
		$this->closeConnection();
		$sql_query = $this->sql_debug($query, $idKey);
		return array(
			'error' => $error,
			'message' => $message,
			'query' => $sql_query.';',
		);
	}
	
}

?>