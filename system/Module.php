<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class Module{
	
	public function __construct(){
		session_start();
		$this->Config = require 'Config.php';
		$this->templates = $this->Config['template'];
		$this->defaultTemplate = '';
		$this->project = $this->Config['project'];
		$this->selfArr = explode('/', rtrim($_SERVER['PHP_SELF'], '/'));
        $this->selfKey = array_search(INDEX_FILE, $this->selfArr);
		$this->protocol = $this->isHttps() ? 'https://':'http://';
        // $this->basePath = 'http://' . $_SERVER['HTTP_HOST'] . implode('/', array_slice($this->selfArr, 0, $this->selfKey)) . '/';
        $this->basePath = $this->protocol . $_SERVER['HTTP_HOST'] . implode('/', array_slice($this->selfArr, 0, $this->selfKey)) . '/';
		if(!empty($this->Config['setting'])){
			foreach($this->Config['setting'] as $set => $value) $this->{$set} = $value;
		}
	}

	public function isHttps() {
		if (isset($_SERVER['HTTPS'])) {
			if(strtolower($_SERVER['HTTPS']) == 'on') return true;
			if($_SERVER['HTTPS'] == '1') return true;
		}
		elseif (isset($_SERVER['SERVER_PORT']) && ($_SERVER['SERVER_PORT'] == '443')) {
			return true;
        }
        elseif (isset($_SERVER['HTTP_X_FORWARDED_PORT']) && ($_SERVER['HTTP_X_FORWARDED_PORT'] == '443')) {
			return true;
		}
		return false;
	}
	
	public function setTemplate($template){
		$this->defaultTemplate = $template;
	}
	
	public function getSetting($set){
		return $this->{$set};
	}
	
	public function getComponent(){
		if($handle = opendir(COMP)){
			while (false !== ($file = readdir($handle))){
				if (preg_match('/.php\z/i', $file)){
					require_once COMP . $file;
				}
			}
			closedir($handle);
		}
	}
	
	public function showView($fileView, $data = array()){
		extract($data, EXTR_SKIP);
		$this->viewPath =  APP . $this->defaultTemplate . '/view/' . $fileView . '.' . $this->defaultTemplate . '.php';
		$this->templatePath = $this->basePath . 'template/' . $this->templates[$this->defaultTemplate]['basePath'];
		$this->getComponent();
		require_once TEMPLATE . $this->templates[$this->defaultTemplate]['basePath'] . 'index.php';
		unset($data);
	}
	
	public function subView($fileView, $data = array()){
		extract($data, EXTR_SKIP);
		$this->getComponent();
		$this->templatePath = $this->basePath . 'template/' . $this->templates[$this->defaultTemplate]['basePath'];
		require APP . $this->defaultTemplate . '/view/' . $fileView  . '.' . $this->defaultTemplate . '.php';
		unset($data);
	}
	
	public function setSession($name, $data){
		$_SESSION[$this->project][$name] = $data;
	}
	
	public function getSession($name){
		return isset($_SESSION[$this->project][$name]) ? $_SESSION[$this->project][$name] : '';
	}
	
	public function delSession($name){
		if(isset($_SESSION[$this->project][$name])) unset($_SESSION[$this->project][$name]);
	}
	
	public function desSession(){
		if(isset($_SESSION[$this->project])) unset($_SESSION[$this->project]);
	}
	
}

?>