<?php
return array(
    /* Website Configuration */
    'project' => 'TAS',
    'setting' => array(
        'web_title' => 'Traffic Accident Services Sat Lantas Polres Klaten',
        'web_author' => 'Hanif Muhammad',
        'web_description' => 'Traffic Accident Services Sat Lantas Polres Klaten',
        'web_header' => 'POLRES KLATEN',
        'web_footer' => '&copy; '.date('Y').' satlantaspolresklaten.id',
        'admin_header' => 'Panel Konfigurasi',
        'admin_footer' => '&copy; '.date('Y').' satlantaspolresklaten.id',
        'dir_upload_image' => UPLOAD.'image',
        'dir_upload_lampiran' => UPLOAD.'lampiran',
        'file_type_image' => 'jpeg|jpg|png',
        'file_type_lampiran' => 'pdf|jpg|jpeg',
        'max_size' => 10485760, // 10mb,
        'bulanAktif' => date('m'),
        'tahunAktif' => date('Y'),
        // 'pilihanBulan' => array('' => 'Semua Bulan', '1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'),
        'pilihanBulan' => array('1' => 'Januari', '2' => 'Februari', '3' => 'Maret', '4' => 'April', '5' => 'Mei', '6' => 'Juni', '7' => 'Juli', '8' => 'Agustus', '9' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember'),
        'pilihanTahun' => array(date('Y') => date('Y')),
        'pilihanInfoGrafis' => array(
            'kondisi' => 'Kondisi Korban',
            'waktu' => 'Rentang Waktu',
            'usia' => 'Rentang Usia',
            'sim' => 'Jenis SIM',
        ),
        'pilihanKategoriRanmor' => array(
            'pribadi' => 'Kendaraan Pribadi',
            'dinas' => 'Kendaraan Dinas',
            'umum' => 'Angkutan Umum',
        ),
        'pilihanJenisRanmor' => array(
            'spm' => 'Sepeda Motor',
            'kbm-tidak-bermotor' => 'Kendaraan tidak bermotor',
            'kbm-penumpang' => 'Mobil Penumpang',
            'kbm-barang' => 'Mobil Barang',
            'kbm-khusus' => 'Kendaraan Khusus',
        ),
        'pilihanJenisKecelakaan' => array(
            'beruntun' => 'Beruntun',
            'depan-belakang' => 'Depan Belakang',
            'depan-depan' => 'Depan Depan',
            'depan-samping' => 'Depan Samping',
            'samping-samping' => 'Samping Samping',
            'tabrak-lari' => 'Tabrak Lari',
            'tabrak-manusia' => 'Tabrak Manusia',
            'tabrak-hewan' => 'Tabrak Hewan',
            'tunggal' => 'Tunggal',
            'kecelakaan-kerja' => 'Kecelakaan Kerja',
            'kecelakaan-non-kerja' => 'Kecelakaan Non Kerja',

        ),
        'pilihanJenisProfesi' => array(
            'wiraswasta' => 'Wiraswasta',
            'supir' => 'Supir',
            'petani' => 'Petani',
            'pelajar' => 'Pelajar',
            'pedagang' => 'Pedagang',
            'mahasiswa' => 'Mahasiswa',
            'karyawan-swasta' => 'Karyawan Swasta',
            'Buruh' => 'Buruh',
            'tni-polri' => 'TNI/Polri',
            'lainnya' => 'Lainnya',
            'belum-diketahui' => 'Belum Diketahui',
        ),
        'pilihanJenisAgama' => array(
            'islam' => 'Islam',
            'kristen' => 'Kristen',
            'katholik' => 'Katholik',
            'hindu' => 'Hindu',
            'budha' => 'Budha',
        ),
        'pilihanJenisGolda' => array(
            'A' => 'Gol. Darah A',
            'AB' => 'Gol. Darah AB',
            'B' => 'Gol. Darah B',
            'O' => 'Gol. Darah O',
            '-' => 'Tidak Diketahui',
        ),
        'pilihanJenisKelamin' => array(
            'laki-laki' => 'Laki-laki',
            'perempuan' => 'Perempuan',
        ),
        'pilihanJenisPerkawinan' => array(
            'kawin' => 'Kawin',
            'belum-kawin' => 'Belum Kawin',
        ),
        'pilihanStatusKewarganegaraan' => array(
            'wni' => 'WNI',
            'wna' => 'WNA',
        ),
        'pilihanKondisiKorban' => array(
            'meninggal-dunia' => 'Meninggal Dunia',
            'luka-ringan' => 'Luka Ringan',
            'luka-berat' => 'Luka Berat',
        ),
        'pilihanStatusUploadBerkas' => array(
            '' => 'Sudah/Belum',
            '1' => 'Sudah',
            '0' => 'Belum',
        ),
        'pilihanProgressUploadBerkas' => array(
            '1' => 'Selesai',
            '0' => 'Proses',
        ),
        'pilihanStatusPelapor' => array(
            'korban' => 'Korban',
            'pelaku' => 'Diduga Pelaku',
            'saksi' => 'Saksi/Pemberi Info',
        ),
        'pilihanStatusLaporanMasyarakat' => array(
            'menunggu' => 'Menunggu',
            'proses' => 'Sedang diproses',
            'selesai' => 'Selesai',
        ),
        'pilihanClassColorStatusLaporanMasyarakat' => array(
            'menunggu' => 'warning',
            'proses' => 'info',
            'selesai' => 'success',
        ),
        'pilihanPublishLaporanMasyarakat' => array('Tidak', 'Ya'),
        'pilihanInconPublishLaporanMasyarakat' => array('<i class="fa fa-eye-slash"></i>', '<i class="fa fa-eye"></i>'),
        'status_rekam_medis' => array(
            'baru' => 'Baru',
            'diterima' => 'Tindak Lanjuti',
            'ditolak' => 'Ditolak',
            'non-lp' => 'Non LP',
        ),
        'jenis_rekam_medis' => array(
            'permintaan', // Polres meminta data rekam medis ke Rumah Sakit
            'pengajuan', // Rumah Sakit mengirim data rekam medis ke Polres
        ),
    ),
    /* Database Configuration */
    'database' => array(
        'driver' => 'mysql',
        'host' => $_ENV['MYSQL_HOST'] ?? 'localhost',
        'port' => $_ENV['MYSQL_PORT'] ?? '3306',
        'user' => $_ENV['MYSQL_USER'] ?? 'satlant2',
        'password' => $_ENV['MYSQL_PASS'] ?? 'E5LfE28OD+N{',
        'dbname' => $_ENV['MYSQL_DBNAME'] ?? 'satlant2_database',
        
        // 'port' => 3306,
        // 'host' => 'localhost',
        // 'user' => 'root',
        // 'password' => 'root',
        // 'dbname' => 'dbweb_tas',
        // 'host' => 'satlantaspolresklaten.id',
        // 'user' => 'u5193315',
        // 'password' => '?wbos8rzQO',
        // 'dbname' => 'u5193315_database',
        'charset' => 'utf8',
        'collate' => 'utf8_general_ci',
        'persistent' => false,
        'errorMsg' => 'Maaf, Gagal terhubung dengan database',
        'defaultValue' =>
            array(
                'id_berita' => 'BRT.'.date('YmdHis'),
                'id_data_identitas' => 'IDT.'.date('YmdHis'),
                'id_laporan_kepolisian' => 'LPK.'.date('YmdHis'),
                'id_laporan_masyarakat' => 'LPM.'.date('YmdHis'),
                'id_rekam_medis' => 'RMD.'.date('YmdHis'),
                'id_surat_pengantar' => 'SRP.'.date('YmdHis'),
                'id_surat_jaminan' => 'SRJ.'.date('YmdHis'),
                'id_kwintansi' => 'KWT.'.date('YmdHis'),
                'id_pembayaran' => 'PMB.'.date('YmdHis'),
                'id_operator' => 'OP.XXX',
                'id_satuan_kerja' => 'SK.XXX',
                'prioritas_asuransi' => '0',
                'nominal_kwintansi' => '0',
                'nominal_pembayaran' => '0',
                'pin_akses' => substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5),
                'tanggal_masuk' => date('Y-m-d'),
                'tanggal_laporan_masyarakat' => date('Y-m-d'),
                'datetime' => date('Y-m-d h:i:s'),
                'tanggal_waktu_kejadian' => array( // Value type text contains json
                    'tanggal' => date('Y-m-d'),
                    'waktu' => date('H:i:s')
                ),
                'tempat_tanggal_lahir' => array( // Value type text contains json
                    'tempat' => '',
                    'tanggal' => date('Y-m-d'),
                ),
                'tanggal_umur_pasien' => array( // Value type text contains json
                    'tanggal' => '',
                    'umur' => '',
                ),
                'tempat_kejadian' => array( // Value type text contains json
                    'jalan' => '',
                    'kabupaten' => '',
                    'kecamatan' => '',
                    'kel_desa' => '',
                ),
                'alamat_data_identitas' => array( // Value type text contains json
                    'alamat' => '',
                    'kabupaten' => '',
                    'kecamatan' => '',
                    'kel_desa' => '',
                    'rt_rw' => '',
                ),
                'info_kondisi_korban' => array(
                    'meninggal_dunia' => 'Meninggal Dunia',
                    'luka_ringan' => 'Luka Ringan',
                    'luka_berat' => 'Luka Berat',
                ),
                'info_jenis_usia' => array(
                    'anak' => 'Anak/Remaja',
                    'dewasa' => 'Dewasa',
                    'lanjut_usia' => 'Lanjut Usia',
                ),
                'info_jenis_sim' => array(
                    'sim_a' => 'SIM A',
                    'sim_b1' => 'SIM B I',
                    'sim_b2' => 'SIM B II',
                    'sim_c' => 'SIM C',
                    'sim_d' => 'SIM D',
                    'sim_a_umum' => 'SIM A UMUM',
                    'sim_b1_umum' => 'SIM B I UMUM',
                    'sim_b2_umum' => 'SIM B II UMUM',
                ),
                'info_rentang_waktu' => array(
                    'r0_3' => '00.00 - 03.00',
                    'r3_6' => '03.00 - 06.00',
                    'r6_9' => '06.00 - 09.00',
                    'r9_12' => '09.00 - 12.00',
                    'r12_15' => '12.00 - 15.00',
                    'r15_18' => '15.00 - 18.00',
                    'r18_21' => '18.00 - 21.00',
                    'r21_0' => '21.00 - 00.00',
                ),
                'status_laporan_masyarakat' => 'menunggu',
                'publish_laporan_masyarakat' => 0, // Tidak terpublish

                // DEFAULT VALUE FORM
                // 'nomer_lp' => 'LP/2019/07/12',
                // 'tanggal_lp' => date('Y-m-d'),
            ),
    ),
    /* Error Message Configuration */
    'errMsg' => array(
        'profil' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Profil gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Profil telah disimpan', 'type' => 'info'),
            )
        ),
        'berita' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Berita gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Berita telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Berita gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Berita telah dihapus', 'type' => 'info'),
            ),
            'upload' => array('status' => 'error', 'title' => 'Maaf', 'text' => 'File not found', 'type' => 'danger'),
        ),
        'laporan' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Laporan gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Laporan telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Laporan gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Laporan telah dihapus', 'type' => 'info'),
            ),
            'upload' => array('status' => 'error', 'title' => 'Maaf', 'text' => 'File not found', 'type' => 'danger'),
        ),
        'masyarakat' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Laporan gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Laporan telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Laporan gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Laporan telah dihapus', 'type' => 'info'),
            ),
            'upload' => array('status' => 'error', 'title' => 'Maaf', 'text' => 'File not found', 'type' => 'danger'),
        ),
        'operator' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Operator gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Operator telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Operator gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Operator telah dihapus', 'type' => 'info'),
            )
        ),
        'asuransi' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Jasa Asuransi gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Jasa Asuransi telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Jasa Asuransi gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Jasa Asuransi telah dihapus', 'type' => 'info'),
            )
        ),
        'layanan_kesehatan' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Layanan Kesehatan gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Layanan Kesehatan telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Layanan Kesehatan gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Layanan Kesehatan telah dihapus', 'type' => 'info'),
            )
        ),
        'info_laka' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Info Laka gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Info Laka telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Layanan Kesehatan gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Layanan Kesehatan telah dihapus', 'type' => 'info'),
            )
        ),
        'rekam_medis' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Surat Keterangan Laka gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Surat Keterangan Laka telah disimpan', 'type' => 'info'),
            ),
            'rujukan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Rujukan Data Surat Keterangan Laka gagal dikirim', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Rujukan Data Surat Keterangan Laka telah dikirim', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Surat Keterangan Laka gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Surat Keterangan Laka telah dihapus', 'type' => 'info'),
            )
        ),
        'kwintansi' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Kwintansi gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Kwintansi telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Kwintansi gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Kwintansi telah dihapus', 'type' => 'info'),
            )
        ),
        'surat_jaminan' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Surat Jaminan gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Surat Jaminan telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Surat Jaminan gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Surat Jaminan telah dihapus', 'type' => 'info'),
            )
        ),
        'bukti_pembayaran' => array(
            'simpan' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Bukti Pembayaran gagal disimpan', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Bukti Pembayaran telah disimpan', 'type' => 'info'),
            ),
            'hapus' => array(
                array('status' => 'error', 'title' => 'Maaf', 'text' => 'Data Bukti Pembayaran gagal dihapus', 'type' => 'danger'),
                array('status' => 'success', 'title' => 'Sukses', 'text' => 'Data Bukti Pembayaran telah dihapus', 'type' => 'info'),
            )
        ),
    ),
    /* Navbar Configuration */
    'navbar_backend' => array(
        'UK.001' => 'navbar_backend_polres',
        'UK.002' => 'navbar_backend_asuransi',
        'UK.003' => 'navbar_backend_kesehatan',
    ),
    'navbar_backend_polres' => array(
        'Dashboard' => array(
            'icon' => '',
            'link' => 'dashboard',
        ),
        'Profil' => array(
            'icon' => '',
            'link' => 'profil',
        ),
        // 'Berita' => array(
        //     'icon' => '',
        //     'link' => 'berita',
        // ),
        'Laporan' => array(
            'icon' => '',
            'link' => 'laporan',
        ),
        // 'Masyarakat' => array(
        //     'icon' => '',
        //     'link' => 'masyarakat',
        // ),
        'Operator' => array(
            'icon' => '',
            'link' => 'operator',
        ),
        'Asuransi' => array(
            'icon' => '',
            'link' => 'asuransi',
        ),
        'Layanan Kesehatan' => array(
            'icon' => '',
            'link' => 'layanan_kesehatan',
        ),
        'Info Laka' => array(
            'icon' => '',
            'link' => 'info_laka',
        ),
    ),
    'navbar_backend_asuransi' => array(
        'Dashboard' => array(
            'icon' => '',
            'link' => 'dashboard',
        ),
        'Profil' => array(
            'icon' => '',
            'link' => 'profil',
        ),
        // 'Upload Bukti Pembayaran' => array(
        //     'icon' => '',
        //     'link' => 'upload_bukti_pembayaran',
        // ),
        'Laporan' => array(
            'icon' => '',
            'link' => 'laporan',
        ),
    ),
    'navbar_backend_kesehatan' => array(
        'Dashboard' => array(
            'icon' => '',
            'link' => 'dashboard',
        ),
        'Profil' => array(
            'icon' => '',
            'link' => 'profil',
        ),
        'Surat Keterangan Laka' => array(
            'icon' => '',
            'link' => 'rekam_medis',
        ),
        'Laporan' => array(
            'icon' => '',
            'link' => 'laporan',
        ),
    ),
    'navbar_frontend' => array(
        'Beranda' => array(
            'link' => '',
        ),
        'Profil' => array(
            'link' => 'profil',
        ),
        'Berita' => array(
            'link' => 'berita',
        ),
        'Info Masyarakat' => array(
            'link' => 'masyarakat',
        ),
    ),
    /* Template Configuration */
    'template' => array(
        'frontend' => array(
            'basePath' => 'auston/', // Path template
        ),
        'backend' => array(
            'basePath' => 'appui-27/', // Path template
        ),
    ),
);
/* ---------------------- */
?>
