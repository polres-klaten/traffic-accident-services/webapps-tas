<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: include system
 * @Description : 
 */
class Main{
    
    public function __construct(){
         // Set default timezone
        date_default_timezone_set('Asia/Jakarta');
        define('APP', dirname(__FILE__) . '/application/');
		define('COMP', dirname(__FILE__) . '/component/');
        define('SYSTEM', dirname(__FILE__) . '/system/');
        define('UPLOAD', dirname(__FILE__) . '/upload/');
        define('TEMPLATE', dirname(__FILE__) . '/template/');
        define('INDEX_FILE', basename(__FILE__));
        define('COOKIE_EXP', (3600 * 24)); // 24 Jam / 1 Hari

		$this->redirectHTTPS();
        // Running Application
        $this->init();
	}
    
    public function init(){
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        
        // Load Library
        require_once SYSTEM . 'Database.php';
        require_once SYSTEM . 'Module.php';
		require_once SYSTEM . 'Files.php';
		
		$librarys['db'] = new Database();
		$librarys['mod'] = new Module();
		$librarys['file'] = new Files();
		
		// $this->isAdmin = false;
		$this->isAdmin = true;
		// $_SESSION['admin_path'] = session_id();
		$_SESSION['admin_path'] = 'admin-panel';
		$params['admin_path'] = $_SESSION['admin_path'];
		$params['module'] = '';
        $params['method'] = '';
		$params['id'] = '';
		
		if($_GET){
			$this->params = explode('/', $_GET['params']);
			if($this->params[0] === $params['admin_path']){
				$this->isAdmin = true;
				$params['module'] = !empty($this->params[1]) ? $this->params[1] : '';
				$params['method'] = !empty($this->params[2]) ? $this->params[2] : '';
				$params['id'] = !empty($this->params[3]) ? $this->params[3] : '';
			}
			else{
				$params['module'] = !empty($this->params[0]) ? $this->params[0] : '';
				$params['method'] = !empty($this->params[1]) ? $this->params[1] : '';
				$params['id'] = !empty($this->params[2]) ? $this->params[2] : '';
			}
		}

		require_once APP . 'service.php';
        
		if($this->isAdmin)
			require_once APP . 'backend.php';
		else
			require_once APP . 'frontend.php';
    }

	public function redirectHTTPS() {
		if ($_SERVER['HTTPS'] != 'on') {
			$redirect = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
			header('Location:'.$redirect);
		}
	}
    
}

new Main();
?>
