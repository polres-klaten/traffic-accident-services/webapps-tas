<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class service{

    public function __construct($librarys, $params){
        foreach($librarys as $library => $value) $this->{$library} = $value;
        foreach($params as $param => $value) $this->{$param} = $value;
        $this->link_backend = $this->mod->basePath.$this->admin_path;
        $this->link_frontend = $this->mod->basePath;
        $this->link_file_image = $this->link_frontend.'upload/image/';
        $this->link_file_lampiran = $this->link_frontend.'upload/lampiran/';
        $this->berkas_file = array(
			'berkas_ahli_waris',
			'berkas_laporan_kepolisian',
			'berkas_laporan_skkl',
			'berkas_data_identitas',
            'berkas_surat_pengantar',
            'berkas_rekam_medis',
            'berkas_kwintansi',
            'berkas_surat_jaminan',
            'berkas_pembayaran',
		);
    }

    /** Global Function */

    protected function sendSMS($nohp = '', $pin = ''){
        $userkey = "ha06kf"; // userkey lihat di zenziva
        $passkey = "hw11jymhks"; // set passkey di zenziva
        $message = "Salam TAS Polres Klaten, Silahkan Cek Perkembangan Layanan Asuransi Anda, dengan masukan pin : $pin pada aplikasi TAS atau akses satlantaspolresklaten.id";
        // $message = "Salam TAS Polres Klaten akses Info perkembangan asuransi kamu di www.satlantaspolresklaten.id atau di playstore TAS KLATEN masuk ke fitur layanan asuransi masukan PIN XXXXXX Salam TAS Klaten menuju Polri Promoter";
        // $telepon = '08176586181';
        $message = urlencode($message);
        $url = "https://reguler.zenziva.net/apps/smsapi.php";
        $curlHandle = curl_init();
        curl_setopt($curlHandle, CURLOPT_URL, $url);
        curl_setopt($curlHandle, CURLOPT_POSTFIELDS, 'userkey='.$userkey.'&passkey='.$passkey.'&nohp='.$nohp.'&pesan='.$message);
        curl_setopt($curlHandle, CURLOPT_HEADER, 0);
        curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
        curl_setopt($curlHandle, CURLOPT_POST, 1);
        $xml = curl_exec($curlHandle);
        curl_close($curlHandle);
        $XMLdata = new SimpleXMLElement($xml);
        $result['saldo'] = $XMLdata->message[0]->balance;
        $result['status'] = strtolower($XMLdata->message[0]->text);
        return $result;
    }

    protected function cekSMS(){
        $xml = file_get_contents('https://reguler.zenziva.net/apps/smsapibalance.php?userkey=r3k4fu&passkey=sipadanjateng');
        $object = simplexml_load_string($xml);
        $result['saldo'] = $object->message->value;
        $result['expired'] = $object->message->text;
        return $result;
    }

    protected function uploadLampiran($file){
        ini_set('memory_limit', '-1');
        $result['status'] = 'success';
        $result['errorMsg'] = 'file tidak dilampirkan';
        $result['UploadFile'] = '';
        if(!empty($file['name'])){
            $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
            $opt_upload = array(
                'fileName' => 'Berkas_'.date('dmY').rand(1,9999).'.'.$ext,
                'fileType' => $this->mod->file_type_lampiran,
                'maxSize' => $this->mod->max_size,
                'folder' => $this->mod->dir_upload_lampiran,
                'session' => false,
                'session' => true,
            );
            $result = $this->file->upload($file, $opt_upload);
        }
        return $result;
    }

    protected function uploadImage($file){
        ini_set('memory_limit', '-1');
        $result['status'] = 'success';
        $result['errorMsg'] = 'file tidak dilampirkan';
        $result['UploadFile'] = '';
        if(!empty($file['files']['name'])){
            $ext = pathinfo($file['files']['name'], PATHINFO_EXTENSION);
            $opt_upload = array(
                'fileName' => date('dmYHis').'.'.$ext,
                'fileType' => $this->mod->file_type_image,
                'maxSize' => $this->mod->max_size,
                'folder' => $this->mod->dir_upload_image,
                'session' => false,
            );
            $result = $this->file->upload($file['files'], $opt_upload);
            if($result['status'] == 'success'){
                $src = $this->mod->dir_upload_image.'/'.$result['UploadFile'];
                $dst = $this->mod->dir_upload_image.'/Image_'.$result['UploadFile'];
                $result['UploadFile'] = 'Image_'.$result['UploadFile'];
                FUNC::resizeImage(600, $src, $ext, $dst);
            }
        }
        return $result;
    }

    protected function debugResponse($data){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
    }

    protected function showResponse($errorMsg){
        header("HTTP/1.1 200");
        header("Content-Type:application/json");
        echo json_encode($errorMsg);
    }

    protected function getAutoKode($id, $tabel){
        set_time_limit(0);
        $data = $this->db->getTabel($tabel);
        $kode = explode('.', $data[$id]);
        $dataTabel = $this->db->getData('SELECT * FROM '.$tabel.' WHERE ('.$id.' LIKE ?) ORDER BY '.$id.' DESC LIMIT 1', array($kode[0].'%'));
        if($dataTabel['count'] > 0){
            $kode = explode('.', $dataTabel['value'][0][$id]);
            $number = intval($kode[1])+1;
            $data[$id] = $kode[0].'.'.sprintf('%0'.strlen($kode[1]).'s', $number);
        }else{
            $data[$id] = $kode[0].'.01';
        }
        return $data;
    }

    protected function createQuery($data){
        $result = array();
        foreach ($data as $key => $value) { $query = '('.$value.' LIKE ?)'; array_push($result, $query);}
        return implode(' AND ', $result);
    }

    protected function convertIndexToAssoc($format, $data){
		$index = 0;
		foreach ($format as $key => $val) {$format[$key] = $data[$index++];}
		return $format;
	}

	protected function isAssoc(array $arr){
        if(array() == $arr) return false;
        return array_keys($arr) !== range(0, count($arr) - 1);
	}
	
	protected function extractJSON($data, $link = true){
		foreach ($data as $key => $value) {
			if(!is_array($value)){
				$json = json_decode($value, true);
				if($json) $data[$key] = $json;
			}

			// Create link
			if($link){
				foreach ($this->berkas_file as $keys => $values) {
					if(($key == $values) && !empty($value)) $data[$key] = '(Download: <a href="'.$this->link_file_lampiran.$value.'" target="_blank">'.$value.'</a>)';
				}
			}
		}
		return $data;
	}

    /** Custom Function */

    protected function login($input){
        set_time_limit(0);
        $idKey = array($input['username'], FUNC::encryptor($input['password']));
        $dataLogin = $this->db->getData('SELECT * FROM tref_operator a, tref_satuan_kerja b WHERE (a.satuan_kerja_id=b.id_satuan_kerja) AND (nrp_nip_operator = ?) AND (password_operator = ?)', $idKey);
        $result['status'] = false;
        if($dataLogin['count'] > 0){
            session_regenerate_id();
            $result['dataLogin'] = $dataLogin['value'][0];
            $result['dataLogin']['session_id'] = md5(session_id());
            $result['dataLogin']['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
            $result['status'] = true;
        }
        return $result;
    }
    
    protected function logout(){
        $this->mod->desSession();
        echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'\'" />';
    }

    protected function checkSessionPolres(){
        // Redirect ke dashboard jika user login selain unit polres
        if($this->session_login['unit_kerja_id'] !== 'UK.001'){
            echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'\'" />';
            die;
        }
    }

    protected function checkSessionAsuransi(){
        // Redirect ke dashboard jika user login selain unit asuransi
        if($this->session_login['unit_kerja_id'] !== 'UK.002'){
            echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'\'" />';
            die;
        }
    }

    protected function checkSessionKesehatan(){
        // Redirect ke dashboard jika user login selain unit kesehatan
        if($this->session_login['unit_kerja_id'] !== 'UK.003'){
            echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'\'" />';
            die;
        }
    }

    protected function pilInfoKabKecDes($dataForm){
        $data['pil_kabupaten_tempat_kejadian'] = array('' => '-') + $this->getPilihanKabupaten();
        $data['pil_kecamatan_tempat_kejadian'] = array('' => '-') + $this->getPilihanKecamatan($dataForm['laporan_kepolisian']['tempat_kejadian']['kabupaten']);
        $data['pil_desa_tempat_kejadian'] = array('' => '-') + $this->getPilihanKelDesa($dataForm['laporan_kepolisian']['tempat_kejadian']['kecamatan']);
        $data['pil_kabupaten_alamat_data_identitas'] = array('' => '-') + $this->getPilihanKabupaten();
        $data['pil_kecamatan_alamat_data_identitas'] = array('' => '-') + $this->getPilihanKecamatan($dataForm['data_identitas']['alamat_data_identitas']['kabupaten']);
        $data['pil_desa_alamat_data_identitas'] = array('' => '-') + $this->getPilihanKelDesa($dataForm['data_identitas']['alamat_data_identitas']['kecamatan']);
        return $data;
    }

    protected function berkasPolres($formLaporan){
        $dataForm['laporan_kepolisian'] = $this->extractJSON($formLaporan);
        $dataForm['data_identitas'] = $this->extractJSON($this->getDataLaporanIdentitas($formLaporan['id_laporan_kepolisian']));
        $dataForm['surat_pengantar'] = $this->getDataSuratPengantar($formLaporan['id_laporan_kepolisian']);
        foreach ($dataForm['surat_pengantar'] as $key => $value) {$dataForm['surat_pengantar'][$key] = $this->extractJSON($value);}
        return $dataForm;
    }

    protected function berkasAsuransi($formLaporan){
        $result = array('surat_jaminan' => array(), 'bukti_pembayaran' => array());
        if(empty($formLaporan['satker_tujuan_laporan_kepolisian'])) return $result;
        $id_asuransi = explode(',', $formLaporan['satker_tujuan_laporan_kepolisian']);
		$pil_jasa_asuransi = $this->getPilihanSatuanKerja('UK.002');
		foreach ($id_asuransi as $key => $val) {
			$surat_jaminan = $this->db->getData('SELECT * FROM `tref_surat_jaminan` surat_jaminan LEFT JOIN `tref_satuan_kerja` satker ON (satker.`id_satuan_kerja`=surat_jaminan.`satker_asal_surat_jaminan`) WHERE (satker.`id_satuan_kerja`= ?) AND (surat_jaminan.`laporan_kepolisian_id` = ?);', array($val, $formLaporan['id_laporan_kepolisian']));
			if($surat_jaminan['count'] > 0){
				$value = $surat_jaminan['value'][0];
				$result['surat_jaminan'][$key] = array(
                    'id_satker' => $value['id_satuan_kerja'],
                    'nama_asuransi' => $value['nama_satuan_kerja'],
					'keterangan' => $value['keterangan_surat_jaminan'],
                    'file_lampiran' => '(Download: <a href="'.$this->link_file_lampiran.$value['berkas_surat_jaminan'].'" target="_blank">'.$value['berkas_surat_jaminan'].'</a>)',
                    'action' => '<a href="javascript:app.showInbox(\''.$formLaporan['id_laporan_kepolisian'].'\', \'surat-jaminan\')" class="btn btn-effect-ripple btn-info" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i> Edit</a>',
				);
			}else{
				$result['surat_jaminan'][$key] = array(
                    'id_satker' => $val,
					'nama_asuransi' => $pil_jasa_asuransi[$val],
					'keterangan' => '',
                    'file_lampiran' => '',
                    'action' => '',
				);
			}
			$bukti_pembayaran = $this->db->getData('SELECT * FROM `tref_pembayaran` bukti_pembayaran JOIN `tref_satuan_kerja` satker ON (satker.`id_satuan_kerja`=bukti_pembayaran.`satker_asal_pembayaran`) WHERE (satker.`id_satuan_kerja`= ?) AND (bukti_pembayaran.`laporan_kepolisian_id` = ?);', array($val, $formLaporan['id_laporan_kepolisian']));
			if($bukti_pembayaran['count'] > 0){
				$value = $bukti_pembayaran['value'][0];
				$result['bukti_pembayaran'][$key] = array(
                    'id_pembayaran' => $value['id_pembayaran'],
                    'id_satker' => $value['id_satuan_kerja'],
					'nama_asuransi' => $value['nama_satuan_kerja'],
					'nominal' => 'Rp. '.FUNC::rupiah($value['nominal_pembayaran']),
                    'file_lampiran' => '(Download: <a href="'.$this->link_file_lampiran.$value['berkas_pembayaran'].'" target="_blank">'.$value['berkas_pembayaran'].'</a>)',
                    'action' => '<a href="javascript:app.showInbox(\''.$formLaporan['id_laporan_kepolisian'].'\', \'bukti-pembayaran\')" class="btn btn-effect-ripple btn-info" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i> Edit</a>',
				);
			}else{
				$result['bukti_pembayaran'][$key] = array(
                    'id_pembayaran' => '',
                    'id_satker' => $val,
					'nama_asuransi' => $pil_jasa_asuransi[$val],
					'nominal' => 'Rp. 0',
                    'file_lampiran' => '',
                    'action' => '',
				);
			}
		}
		return $result;
    }

    protected function berkasKesehatan($formLaporan){
        $dataForm['rekam_medis'] = $this->extractJSON($this->getDataRekamMedis($formLaporan['rekam_medis_id']));
        $dataForm['kwintansi'] = $this->getDataKwintansi($formLaporan['id_laporan_kepolisian']);
		foreach ($dataForm['kwintansi'] as $key => $value) {$dataForm['kwintansi'][$key] = $this->extractJSON($value);}
        return $dataForm;
    }

    protected function checkPIN($pin){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE (pin_akses = ?)', array($pin));
        if($result['count'] > 0){
            return $result['value'][0];
        }else{
            return array();
        }
    }

    protected function getInboxRekamMedis($jenis){
        set_time_limit(0);
		$result = $this->db->getData('SELECT *,
        (SELECT nama_satuan_kerja FROM `tref_satuan_kerja` b WHERE (a.satker_asal_rekam_medis=b.id_satuan_kerja)) AS satker_asal,
        (SELECT nama_satuan_kerja FROM `tref_satuan_kerja` b WHERE (a.satker_tujuan_rekam_medis=b.id_satuan_kerja)) AS satker_tujuan
        FROM `tref_rekam_medis` a WHERE (jenis_rekam_medis = ?) ORDER BY id_rekam_medis DESC;', array($jenis));
        return $result['value'];
    }

    protected function getInboxKwintansi($satker){
        /**
         * Inbox pembuatan kwintansi untuk rumah sakit
         * Data yang muncul jika status berkas rekam medis sudah dibuat dan kwintansi belum diupload
         */
        // set_time_limit(0);
		// $result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE (satker_asal_rekam_medis = ?) AND (status_berkas_rekam_medis = ?) AND (status_berkas_kwintansi = ?)', array($satker, 1, 0));
        // echo $result['query'];
        // return $result['value'];

        /** Revisi
         * Inbox pembuatan kwintansi untuk rumah sakit
         * Data yang muncul jika status berkas surat jaminan sudah diupload dan berkas kwintansi belum diupload
         */
        set_time_limit(0);
		$result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE (satker_asal_rekam_medis = ?) AND (status_berkas_rekam_medis = ?) AND (status_berkas_surat_jaminan = ?) AND (status_berkas_kwintansi = ?)', array($satker, 1, 1, 0));
        // echo $result['query'];
        return $result['value'];
    }

    protected function getInboxLaporanKepolisian($satker){
        /**
         * Inbox laporan kepolisian untuk asuransi
         * Data yang muncul jika status berkas asuransi (surat jaminan dan bukti bayar) belum diupload
         */
        set_time_limit(0);
		$result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE FIND_IN_SET(?, satker_tujuan_laporan_kepolisian) AND ((status_berkas_surat_jaminan = ?) OR (status_berkas_pembayaran = ?))', array($satker, 0, 0));
        return $result['value'];
    }

    protected function getInboxSuratJaminan($satker){
        /**
         * Inbox surat jaminan untuk asuransi
         * Data yang muncul jika status berkas laporan kepolisian dan rekam medis sudah diupload
         * Data yang nantinya ditampilkan jika nominal kwintansi dikurangi pembayaran kurang dari 0
         */
        set_time_limit(0);
        $retVal = array();
        // $result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE FIND_IN_SET(?, satker_tujuan_laporan_kepolisian) AND (NOT FIND_IN_SET(?, satker_asal_surat_jaminan)) AND (status_berkas_polres = ?) AND (status_berkas_rekam_medis = ?)', array($satker, $satker, 1, 1));
        $result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE FIND_IN_SET(?, satker_tujuan_laporan_kepolisian) AND (NOT FIND_IN_SET(?, satker_asal_surat_jaminan)) AND (status_berkas_polres = ?)', array($satker, $satker, 1));
        foreach ($result['value'] as $key => $value) {
            // Check Sisa Pembayaran
            // $kwintansi = intval($value['nominal_kwintansi']);
            // $pembayaran = intval($value['nominal_pembayaran']);
            // if(($kwintansi != 0) && ($kwintansi - $pembayaran) > 0){
                array_push($retVal, $value);
            // }
        }
        return $retVal;
        return $result;
    }

    protected function getInboxBuktiPembayaran($satker){
        /**
         * Inbox bukti pembayaran untuk asuransi
         * Data yang nantinya ditampilkan jika data surat jaminan dan kwintansi sudah diupload
         */
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM view_resume_laporan_kepolisian WHERE FIND_IN_SET(?, satker_tujuan_laporan_kepolisian) AND (FIND_IN_SET(?, satker_asal_surat_jaminan)) AND (NOT FIND_IN_SET(?, satker_asal_pembayaran)) AND (status_berkas_kwintansi = ?)', array($satker, $satker, $satker, 1));
        return $result['value'];
    }

    protected function getPilihanUnitKerja(){
        set_time_limit(0);
        $data = $this->db->getData('SELECT * FROM tref_unit_kerja ORDER BY id_unit_kerja ASC', array());
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['id_unit_kerja']] = $kol['nama_unit_kerja'];}
        }else{
            $dataArr = array('' => 'Data Kosong');
        }
        return $dataArr;
    }

    protected function getPilihanSatuanKerja($unit = ''){
        set_time_limit(0);
        $data = $this->db->getData('SELECT * FROM tref_satuan_kerja WHERE (unit_kerja_id LIKE ?) ORDER BY prioritas_asuransi ASC, id_satuan_kerja ASC', array('%'.$unit.'%'));
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['id_satuan_kerja']] = $kol['nama_satuan_kerja'];}
        }else{
            $dataArr = array('' => 'Data Kosong');
        }
        return $dataArr;
    }

    protected function getPilihanKabupaten(){
        set_time_limit(0);
        $data = $this->db->getData('SELECT * FROM tref_kabkota ORDER BY id_kabkota ASC', array());
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['id_kabkota']] = $kol['nama_kabkota'];}
        }else{
            $dataArr = array('' => 'Data Kosong');
        }
        return $dataArr;
    }

    protected function getPilihanKecamatan($kabupaten = ''){
        set_time_limit(0);
        $data = $this->db->getData('SELECT * FROM tref_kecamatan WHERE (kabkota_id = ?) ORDER BY id_kecamatan ASC', array($kabupaten));
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['id_kecamatan']] = $kol['nama_kecamatan'];}
        }else{
            $dataArr = array('' => 'Data Kosong');
        }
        return $dataArr;
    }

    protected function getPilihanKelDesa($kecamatan = ''){
        set_time_limit(0);
        $data = $this->db->getData('SELECT * FROM tref_keldesa WHERE (kecamatan_id = ?) ORDER BY id_keldesa ASC', array($kecamatan));
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['id_keldesa']] = $kol['nama_keldesa'];}
        }else{
            $dataArr = array('' => 'Data Kosong');
        }
        return $dataArr;
    }

    protected function getPilihanTahun(){
        set_time_limit(0);
        $data = $this->db->getData('SELECT tahun FROM tref_info_laka GROUP BY tahun ASC');
        if($data['count'] > 0){
            foreach ($data['value'] as $kol){$dataArr[$kol['tahun']] = $kol['tahun'];}
        }else{
            $dataArr = array(date('Y') => date('Y'));
        }
        return $dataArr;
    }

    protected function getIdUnit($id_satker){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_satuan_kerja', array('id_satuan_kerja', $id));
        return $result;
    }

    protected function getInfoProfil($id = 'SK.001'){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_satuan_kerja', array('id_satuan_kerja', $id));
        return $result;
    }

    protected function getInfoLaka($input){
        set_time_limit(0);
        $tahun = '%'.$input['tahun'].'%';
        $bulan = '%'.$input['bulan'].'%';
		$result = $this->db->getData('SELECT
        IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_kondisi_korban`, "$.luka_ringan"))), 0) AS jumlah_lr,
        IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_kondisi_korban`, "$.luka_berat"))), 0) AS jumlah_lb,
        IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_kondisi_korban`, "$.meninggal_dunia"))), 0) AS jumlah_md
        FROM `tref_info_laka`
        WHERE (tahun LIKE ?) AND (bulan LIKE ?)', array($tahun, $bulan));
        return $result['value'][0];
    }

    protected function getInfoGrafisLaka($input){
        // Ambil data kecamtan di kabupaten klaten
        $data = $this->db->getData('SELECT GROUP_CONCAT(id_kecamatan) AS id_kecamatan, GROUP_CONCAT(nama_kecamatan) AS nama_kecamatan FROM tref_kecamatan WHERE (kabkota_id = ?) ORDER BY id_kecamatan ASC', array('3310'));
        $input['id_kecamatan'] = explode(',', $data['value'][0]['id_kecamatan']);
        $input['nama_kecamatan'] = explode(',', $data['value'][0]['nama_kecamatan']);
        switch ($input['info_grafis']) {
			case 'kondisi':
				$result = $this->getInfoGrafisLakaKondisi($input);
				break;

			case 'waktu':
				$result = $this->getInfoGrafisLakaWaktu($input);
				break;
			
			case 'usia':
				$result = $this->getInfoGrafisLakaUsia($input);
				break;
				
			case 'sim':
				$result = $this->getInfoGrafisLakaSIM($input);
				break;
			
			default:
				$result = $this->getInfoGrafisLaka($input);
				break;
        }
        return $result;
    }

    protected function getInfoDummyGrafisLaka($input){
        set_time_limit(0);
        $laporanLaka = array(
            'title' => 'Tahun '.$input['tahun'],
            'subtitle' => 'Grafik Laka Lantas',
            'tooltip' => 'Laka',
            'height' => 'auto',
            'category' => array(), 
            'series' => array('name' => '', 'data' => array())
        );
		return $laporanLaka;
    }

    protected function getInfoGrafisLakaKondisi($input){
        set_time_limit(0);
        $laporanLaka = array(
            'title' => 'Tahun '.$input['tahun'],
            'subtitle' => 'Grafik Laka Lantas Berdasarkan Kondisi Korban',
            'tooltip' => 'Laka',
            'height' => '1000px',
            'category' => $input['nama_kecamatan'], 
            'series' => array()
        );

        $index = 0;
        $kondisi = $this->mod->Config['database']['defaultValue']['info_kondisi_korban'];
        foreach ($kondisi as $key => $value) {
            $laporanLaka['series'][$index] = array('name' => $value, 'data' => array());
            foreach ($input['id_kecamatan'] as $keys => $values) {
                // array_push($laporanLaka['series'][$index]['data'], rand(1, 100)); // Data Dummy
                $result = $this->db->getData('SELECT IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_kondisi_korban`, "$.'.$key.'"))), 0) AS jumlah FROM `tref_info_laka` WHERE (tahun = ?) AND (kecamatan = ?)', array($input['tahun'], $values));
                array_push($laporanLaka['series'][$index]['data'], intval($result['value'][0]['jumlah']));
            }
            $index++;
        }
		return $laporanLaka;
    }

    protected function getInfoGrafisLakaUsia($input){
        set_time_limit(0);
        $laporanLaka = array(
            'title' => 'Tahun '.$input['tahun'],
            'subtitle' => 'Grafik Laka Lantas Berdasarkan Jenis Usia',
            'tooltip' => 'Laka',
            'height' => '1000px',
            'category' => $input['nama_kecamatan'], 
            'series' => array()
        );
        
        $index = 0;
        $usia = $this->mod->Config['database']['defaultValue']['info_jenis_usia'];
        foreach ($usia as $key => $value) {
            $laporanLaka['series'][$index] = array('name' => $value, 'data' => array());
            foreach ($input['id_kecamatan'] as $keys => $values) {
                // array_push($laporanLaka['series'][$index]['data'], rand(1, 100)); // Data Dummy
                $result = $this->db->getData('SELECT IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_jenis_usia`, "$.'.$key.'"))), 0) AS jumlah FROM `tref_info_laka` WHERE (tahun = ?) AND (kecamatan = ?)', array($input['tahun'], $values));
                array_push($laporanLaka['series'][$index]['data'], intval($result['value'][0]['jumlah']));
            }
            $index++;
        }
		return $laporanLaka;
    }

    protected function getInfoGrafisLakaSIM($input){
        set_time_limit(0);
        $laporanLaka = array(
            'title' => 'Tahun '.$input['tahun'],
            'subtitle' => 'Grafik Laka Lantas Berdasarkan Jenis SIM',
            'tooltip' => 'Laka',
            'height' => '2000px',
            'category' => $input['nama_kecamatan'], 
            'series' => array()
        );
        
        $index = 0;
        $sim = $this->mod->Config['database']['defaultValue']['info_jenis_sim'];
        foreach ($sim as $key => $value) {
            $laporanLaka['series'][$index] = array('name' => $value, 'data' => array());
            foreach ($input['id_kecamatan'] as $keys => $values) {
                // array_push($laporanLaka['series'][$index]['data'], rand(1, 100)); // Data Dummy
                $result = $this->db->getData('SELECT IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_jenis_sim`, "$.'.$key.'"))), 0) AS jumlah FROM `tref_info_laka` WHERE (tahun = ?) AND (kecamatan = ?)', array($input['tahun'], $values));
                array_push($laporanLaka['series'][$index]['data'], intval($result['value'][0]['jumlah']));
            }
            $index++;
        }
		return $laporanLaka;
    }

    protected function getInfoGrafisLakaWaktu($input){
        set_time_limit(0);
        $laporanLaka = array(
            'title' => 'Tahun '.$input['tahun'],
            'subtitle' => 'Grafik Laka Lantas Berdasarkan Rentang Waktu',
            'tooltip' => 'Laka',
            'height' => '2000px',
            'category' => $input['nama_kecamatan'], 
            'series' => array()
        );
        
        $index = 0;
        $waktu = $this->mod->Config['database']['defaultValue']['info_rentang_waktu'];
        foreach ($waktu as $key => $value) {
            $laporanLaka['series'][$index] = array('name' => $value, 'data' => array());
            foreach ($input['id_kecamatan'] as $keys => $values) {
                // array_push($laporanLaka['series'][$index]['data'], rand(1, 100)); // Data Dummy
                $result = $this->db->getData('SELECT IFNULL(SUM(JSON_UNQUOTE(JSON_EXTRACT(`info_rentang_waktu`, "$.'.$key.'"))), 0) AS jumlah FROM `tref_info_laka` WHERE (tahun = ?) AND (kecamatan = ?)', array($input['tahun'], $values));
                array_push($laporanLaka['series'][$index]['data'], intval($result['value'][0]['jumlah']));
            }
            $index++;
        }
		return $laporanLaka;
    }

    protected function getJumlahOperator(){
        set_time_limit(0);
		$result = $this->db->getData('SELECT a.`id_unit_kerja`, a.`nama_unit_kerja`, (SELECT COUNT(*) FROM `tref_operator` b, `tref_satuan_kerja` c WHERE (b.`satuan_kerja_id`=c.`id_satuan_kerja`) AND (c.`unit_kerja_id`=a.`id_unit_kerja`)) AS jumlah FROM `tref_unit_kerja` a', array());
        return $result['value'];
    }

    protected function getBeritaTerbaru(){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_berita ORDER BY id_berita DESC LIMIT 5', array());
        return $result['value'];
    }

    protected function getDetailBerita($id = ''){
        set_time_limit(0);
        $link_berita = explode('.', $id)[0];
        $result = $this->db->getData('SELECT * FROM tref_berita WHERE (link_berita = ?)', array($link_berita));
        if($result['count'] > 0){
            return $result['value'][0];
        }else{
            return array();
        }
    }

    protected function getDetailLaporanKepolisian($id = ''){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_laporan_kepolisian WHERE (id_laporan_kepolisian = ?)', array($id));
        if($result['count'] > 0){
            return $result['value'][0];
        }else{
            return $this->getDataLaporanKepolisian(array());
        }
    }

    protected function getDataBerita($id = ''){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_berita', array('id_berita', $id));
        $result['form_title'] = empty($id) ? 'Tambah Berita' : 'Edit Berita';
        return $result;
    }

    protected function getDataSatuanKerja($id = ''){
        set_time_limit(0);
        $result = empty($id) ? $this->getAutoKode('id_satuan_kerja', 'tref_satuan_kerja') : $this->db->getDataTabel('tref_satuan_kerja', array('id_satuan_kerja', $id));
        $result['form_title'] = empty($id) ? 'Tambah Satuan Kerja' : 'Edit Satuan Kerja';
        return $result;
    }

    protected function getDataOperator($id = ''){
        set_time_limit(0);
        $result = empty($id) ? $this->getAutoKode('id_operator', 'tref_operator') : $this->db->getDataTabel('tref_operator', array('id_operator', $id));
        $result['form_title'] = empty($id) ? 'Tambah Operator' : 'Edit Operator';
        return $result;
    }

    protected function getDataInfoLaka($id = ''){
        set_time_limit(0);
        $result = empty($id) ? $this->getAutoKode('id_info_laka', 'tref_info_laka') : $this->db->getDataTabel('tref_info_laka', array('id_info_laka', $id));
        $result['form_title'] = empty($id) ? 'Tambah Info Laka' : 'Edit Info Laka';
        return $result;
    }

    protected function getDataLaporanMasyarakat($id = ''){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_laporan_masyarakat', array('id_laporan_masyarakat', $id));
        $result['form_title'] = empty($id) ? 'Tambah Laporan Masyarakat' : 'Edit Laporan Masyarakat';
        return $result;
    }

    protected function getDataLaporanKepolisian($id){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', $id));
        $result['form_title'] = empty($id) ? 'Buat Laporan Kepolisian' : 'Edit Laporan Kepolisian';
        return $result;
    }

    protected function getDataRekamMedis($id){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_rekam_medis', array('id_rekam_medis', $id));
        $result['form_title'] = empty($id) ? 'Buat Surat Keterangan Laka' : 'Edit Surat Keterangan Laka';
        return $result;
    }

    protected function getDataLaporanIdentitas($id){
        set_time_limit(0);
        $result = $this->db->getDataTabel('tref_data_identitas', array('laporan_kepolisian_id', $id));
        return $result;
    }

    protected function getDataInfoGrafisLaka($input){
        set_time_limit(0);
        $result = array();
        $form = $this->db->getTabel('tref_info_laka');
        $result['pilihan'] = $form;
        $data = $this->db->getData('SELECT * FROM tref_info_laka WHERE (tahun = ?) AND (bulan = ?) AND (kecamatan = ?)', array($input['tahun'], $input['bulan'], $input['kecamatan']));
        if($data['count'] > 0){
            $result['form'] = $this->extractJSON($data['value'][0], false);
        }else{
            foreach ($form as $key => $value) {
                if(isset($input[$key]) && !is_array($input[$key])) $form[$key] = $input[$key];
                if(is_array($value)){
                    // Set default value = 0
                    array_walk($value, function($v, $k) use (&$form, $key) {$form[$key][$k] = 0;});
                }
            }
            $result['form'] = $form;
            // return $result;
        }

        // Buat daftar pilihan sesuai field array
        foreach ($result['pilihan'] as $key => $value) { 
            if(is_array($value)) $result['pilihan'][$key] = $value; 
            else unset($result['pilihan'][$key]);
        }

        return $result;
    }

    // Design form multiple upload (tabel view)
    protected function getDataSuratPengantar($id){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_surat_pengantar WHERE (laporan_kepolisian_id = ?)', array($id));
        if($result['count'] > 0){
            $results = $result['value'];
            array_push($results, $this->db->getTabel('tref_surat_pengantar')); // Tambahan data rows
            return $results;
        }else{
            return array($this->db->getTabel('tref_surat_pengantar'));
        }
    }

    // Design form multiple upload (tabel view)
    protected function getDataKwintansi($id){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_kwintansi WHERE (laporan_kepolisian_id = ?)', array($id));
        if($result['count'] > 0){
            $results = $result['value'];
            array_push($results, $this->db->getTabel('tref_kwintansi')); // Tambahan data rows
            return $results;
        }else{
            return array($this->db->getTabel('tref_kwintansi'));
        }
    }

    protected function getFormSuratJaminan($laporan, $satker){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_surat_jaminan WHERE (laporan_kepolisian_id = ?) AND (satker_asal_surat_jaminan = ?)', array($laporan, $satker));
        if($result['count'] > 0){
            return $result['value'][0];
        }else{
            $form = $this->db->getTabel('tref_surat_jaminan');
            $form['laporan_kepolisian_id'] = $laporan;
            return $form;
        }
    }

    protected function getFormBuktiPembayaran($laporan, $satker){
        set_time_limit(0);
        $result = $this->db->getData('SELECT * FROM tref_pembayaran WHERE (laporan_kepolisian_id = ?) AND (satker_asal_pembayaran = ?)', array($laporan, $satker));
        if($result['count'] > 0){
            return $result['value'][0];
        }else{
            $form = $this->db->getTabel('tref_pembayaran');
            $form['laporan_kepolisian_id'] = $laporan;
            return $form;
        }
    }

    protected function getTabelBerita($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_berita WHERE (judul_berita LIKE ?)';
        $q_value = 'SELECT * FROM tref_berita WHERE (judul_berita LIKE ?) ORDER BY id_berita DESC';
        $idKey = array($cari);
        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelSatuanKerja($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $unit = '%'.$data['unit'].'%';
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_satuan_kerja a, tref_unit_kerja b WHERE (a.unit_kerja_id=b.id_unit_kerja) AND (nama_satuan_kerja LIKE ?) AND (a.unit_kerja_id LIKE ?)';
        $q_value = 'SELECT * FROM tref_satuan_kerja a, tref_unit_kerja b WHERE (a.unit_kerja_id=b.id_unit_kerja) AND (nama_satuan_kerja LIKE ?) AND (unit_kerja_id LIKE ?) ORDER BY id_satuan_kerja ASC';
        $idKey = array($cari, $unit);
        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelOperator($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $unit = '%'.$data['unit'].'%';
        $satker = '%'.$data['satker'].'%';
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_operator a, tref_satuan_kerja b WHERE (a.satuan_kerja_id=b.id_satuan_kerja) AND (nama_operator LIKE ?) AND (a.satuan_kerja_id LIKE ?) AND (b.unit_kerja_id LIKE ?)';
        $q_value = 'SELECT * FROM tref_operator a, tref_satuan_kerja b WHERE (a.satuan_kerja_id=b.id_satuan_kerja) AND (nama_operator LIKE ?) AND (a.satuan_kerja_id LIKE ?) AND (b.unit_kerja_id LIKE ?) ORDER BY id_operator ASC';
        $idKey = array($cari, $satker, $unit);
        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelInfoLaka($data){
        set_time_limit(0);
        $page = $data['page'];
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_info_laka';
        $q_value = 'SELECT * FROM tref_info_laka ORDER BY bulan, tahun ASC';
        $idKey = array();
        $batas = 12;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelLaporanMasyarakat($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $status = '%'.$data['status'].'%';
        $publish = '%'.$data['publish'].'%';
        // $bulan = '%'.$data['bulan'].'%';
        // $tahun = '%'.$data['tahun'].'%';
        $kondisi = array(
            'nama_masyarakat', 
            'noktp_masyarakat', 
            'nohp_masyarakat', 
            'isi_laporan_masyarakat',
            'status_laporan_masyarakat',
            'publish_laporan_masyarakat',
            // 'CAST(MONTH(tanggal_laporan_masyarakat) AS CHAR)',
            // 'CAST(YEAR(tanggal_laporan_masyarakat) AS CHAR)',
        );
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_laporan_masyarakat WHERE '.$this->createQuery($kondisi);
        $q_value = 'SELECT * FROM tref_laporan_masyarakat WHERE '.$this->createQuery($kondisi).' ORDER BY id_laporan_masyarakat DESC';
        $idKey = array($cari, $cari, $cari, $cari, $status, $publish);
        // $idKey = array($cari, $cari, $cari, $cari, $status, $publish, $bulan, $tahun);
        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelRekamMedis($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $status = '%'.$data['status'].'%';
        $kondisi = array(
            'nama_pasien', 
            'diagnosa_pasien', 
            'ruang_bangsal', 
            'dokter_pemeriksa',
            'status_rekam_medis',
        );
        $q_count = 'SELECT COUNT(*) AS jml FROM tref_rekam_medis WHERE '.$this->createQuery($kondisi).' AND (satker_asal_rekam_medis = "'.$this->session_login['id_satuan_kerja'].'")';
        $q_value = 'SELECT * FROM tref_rekam_medis WHERE '.$this->createQuery($kondisi).' AND (satker_asal_rekam_medis = "'.$this->session_login['id_satuan_kerja'].'") ORDER BY id_rekam_medis DESC';
        $idKey = array($cari, $cari, $cari, $cari, $status);
        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        return $result;
    }

    protected function getTabelLaporanKepolisian($data){
        set_time_limit(0);
        $page = $data['page'];
        $cari = '%'.$data['cari'].'%';
        $bulan = '%'.$data['bulan'].'%';
        $tahun = '%'.$data['tahun'].'%';
        $status_polres = '%'.$data['status_polres'].'%';
        $status_rekam_medis = '%'.$data['status_rekam_medis'].'%';
        $status_kwintansi = '%'.$data['status_kwintansi'].'%';
        $status_surat_jaminan = '%'.$data['status_surat_jaminan'].'%';
        $status_pembayaran = '%'.$data['status_pembayaran'].'%';
        $kondisi = array(
            'nomer_lp', 
            'tanggal_lp', 
            'tempat_kejadian',
            'jenis_kecelakaan',
            'ketegori_ranmor',
            'jenis_ranmor',
            'kondisi_korban',
            'kronologi_kejadian',
            'status_berkas_polres',
            'status_berkas_rekam_medis',
            'status_berkas_kwintansi',
            'status_berkas_surat_jaminan',
            'status_berkas_pembayaran',
            // 'CAST(MONTH(tanggal_waktu_kejadian) AS CHAR)',
            // 'CAST(YEAR(tanggal_waktu_kejadian) AS CHAR)',
        );
        
        if($this->session_login['unit_kerja_id'] == 'UK.001') // Polres
            $query_satker = ''; 
        
        if($this->session_login['unit_kerja_id'] == 'UK.002') // Asuransi
            $query_satker = ' AND (FIND_IN_SET("'.$this->session_login['id_satuan_kerja'].'", satker_tujuan_laporan_kepolisian))'; 
        
        if($this->session_login['unit_kerja_id'] == 'UK.003') // Rumah Sakit
            $query_satker = ' AND (satker_asal_rekam_medis = "'.$this->session_login['id_satuan_kerja'].'")'; 

        $q_count = 'SELECT COUNT(*) AS jml FROM view_resume_laporan_kepolisian WHERE '.$this->createQuery($kondisi).$query_satker;
        $q_value = 'SELECT *, (status_berkas_polres AND status_berkas_rekam_medis AND status_berkas_kwintansi AND status_berkas_surat_jaminan AND status_berkas_pembayaran) AS status_progress FROM view_resume_laporan_kepolisian WHERE '.$this->createQuery($kondisi).$query_satker.' ORDER BY id_laporan_kepolisian DESC';
        $idKey = array($cari, $cari, $cari, $cari, $cari, $cari, $cari, $cari, $status_polres, $status_rekam_medis, $status_kwintansi, $status_surat_jaminan, $status_pembayaran);
        // $idKey = array($cari, $cari, $cari, $cari, $cari, $cari, $cari, $cari, $status_polres, $status_rekam_medis, $status_kwintansi, $status_surat_jaminan, $status_pembayaran, $bulan, $tahun);

        $batas = 10;
        $posisi = ($page - 1) * $batas;
        $jmlData = $this->db->getData($q_count, $idKey);
        $dataArr = $this->db->getData($q_value.' LIMIT '.$posisi.','.$batas, $idKey);
        $result['no'] = $posisi + 1;
        $result['page'] = $page;
        $result['batas'] = $batas;
        $result['jmlData'] = $jmlData['value'][0]['jml'];
        $result['dataTabel'] = $dataArr['value'];
        $result['query'] = $dataArr['query'];
        $result['query'] = '';
        $result['status_polres'] = $data['status_polres'];
        $result['status_rekam_medis'] = $data['status_rekam_medis'];
        $result['status_kwintansi'] = $data['status_kwintansi'];
        $result['status_surat_jaminan'] = $data['status_surat_jaminan'];
        $result['status_pembayaran'] = $data['status_pembayaran'];
        return $result;
    }
    
}

new service($librarys, $params);

// CREATE OR REPLACE VIEW view_laporan_kepolisian AS
// SELECT 
// lp.*,
// di.*,
// rm.*,
// CAST(JSON_UNQUOTE(JSON_EXTRACT(lp.`tanggal_waktu_kejadian`, "$.tanggal")) AS DATE) AS tanggal_kejadian,
// CAST(JSON_UNQUOTE(JSON_EXTRACT(lp.`tanggal_waktu_kejadian`, "$.waktu")) AS TIME) AS waktu_kejadian,
// GROUP_CONCAT(IF(sj.`satker_asal_surat_jaminan` IS NULL, "", sj.`satker_asal_surat_jaminan`)) AS satker_asal_surat_jaminan,
// GROUP_CONCAT(IF(pb.`satker_asal_pembayaran` IS NULL, "", pb.`satker_asal_pembayaran`)) AS satker_asal_pembayaran,
// IF(SUM(kw.`nominal_kwintansi`) IS NULL, 0, SUM(kw.`nominal_kwintansi`)) AS `nominal_kwintansi`,
// IF(SUM(pb.`nominal_pembayaran`) IS NULL, 0, SUM(pb.`nominal_pembayaran`)) AS `nominal_pembayaran`,
// GROUP_CONCAT(IF(sp.`berkas_surat_pengantar` = "", null, sp.`berkas_surat_pengantar`)) AS berkas_surat_pengantar, 
// IF(GROUP_CONCAT(IF(sp.`berkas_surat_pengantar` = "", null, sp.`berkas_surat_pengantar`)) IS NULL, 0, 1) status_berkas_surat_pengantar,
// IF(IF(lp.`berkas_ahli_waris` = "", null, lp.`berkas_ahli_waris`) IS NULL, 0, 1) AS status_berkas_ahli_waris,
// IF(IF(lp.`berkas_laporan_kepolisian` = "", null, lp.`berkas_laporan_kepolisian`) IS NULL, 0, 1) AS status_berkas_laporan_kepolisian,
// IF(IF(lp.`berkas_laporan_skkl` = "", null, lp.`berkas_laporan_skkl`) IS NULL, 0, 1) AS status_berkas_laporan_skkl,
// IF(IF(di.`berkas_data_identitas` = "", null, di.`berkas_data_identitas`) IS NULL, 0, 1) AS status_berkas_data_identitas,
// IF(IF(rm.`berkas_rekam_medis` = "", null, rm.`berkas_rekam_medis`) IS NULL, 0, 1) AS status_berkas_rekam_medis,
// IF(IF(kw.`berkas_kwintansi` = "", null, kw.`berkas_kwintansi`) IS NULL, 0, 1) AS status_berkas_kwintansi,
// IF(IF(sj.`berkas_surat_jaminan` = "", null, sj.`berkas_surat_jaminan`) IS NULL, 0, 1) AS status_berkas_surat_jaminan,
// IF(IF(pb.`berkas_pembayaran` = "", null, pb.`berkas_pembayaran`) IS NULL, 0, 1) AS status_berkas_pembayaran
// FROM `tref_laporan_kepolisian` lp
// LEFT JOIN `tref_rekam_medis` rm ON (rm.`id_rekam_medis`=lp.`rekam_medis_id`)
// LEFT JOIN `tref_data_identitas` di ON (di.`laporan_kepolisian_id`=lp.`id_laporan_kepolisian`)
// LEFT JOIN `tref_surat_pengantar` sp ON (sp.`laporan_kepolisian_id`=lp.`id_laporan_kepolisian`)
// LEFT JOIN `tref_surat_jaminan` sj ON (sj.`laporan_kepolisian_id`=lp.`id_laporan_kepolisian`)
// LEFT JOIN `tref_kwintansi` kw ON (kw.`laporan_kepolisian_id`=lp.`id_laporan_kepolisian`)
// LEFT JOIN `tref_pembayaran` pb ON (pb.`laporan_kepolisian_id`=lp.`id_laporan_kepolisian`)
// GROUP by lp.`id_laporan_kepolisian` ORDER BY lp.`id_laporan_kepolisian` DESC;

// CREATE OR REPLACE VIEW view_resume_laporan_kepolisian AS
// SELECT
// vlp.*,
// (vlp.`status_berkas_ahli_waris` OR vlp.`status_berkas_laporan_kepolisian` OR vlp.`status_berkas_surat_pengantar` OR vlp.`status_berkas_data_identitas` OR vlp.`status_berkas_laporan_skkl`) AS status_berkas_polres
// FROM `view_laporan_kepolisian` vlp;


?>
