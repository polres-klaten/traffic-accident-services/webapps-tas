<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class backend extends service{

    public function __construct($librarys, $params){
        parent::__construct($librarys, $params);
        $this->librarys = $librarys;
        $this->params = $params;
        $this->mod->setTemplate('backend');
        $this->mod->getComponent();
        $this->load();
    }
    
    private function load(){
        $this->session_login = $this->mod->getSession('SESSION_LOGIN');
        $this->data['link_backend'] = $this->link_backend;
        $this->data['link_frontend'] = $this->link_frontend;
        $this->data['link_file_image'] = $this->link_file_image;
        $this->data['link_file_lampiran'] = $this->link_file_lampiran;
        $this->data['link_logout'] = $this->link_backend.'/logout';
        if(!empty($this->session_login)){
            $this->navbar_config = $this->session_login['unit_kerja_id'];
            $this->navbar_backend = $this->mod->Config['navbar_backend'][$this->navbar_config];
            $this->data['navbar'] = $this->mod->Config[$this->navbar_backend];
            $this->data['session_login'] = $this->session_login;
            $this->data['active_modul'] = $this->module;
            $this->data['jumlahOperator'] = $this->getJumlahOperator();
            
            $this->data['pil_status_rekam_medis'] = array('' => 'Semua Status') + $this->mod->status_rekam_medis;
            $this->data['pil_jasa_asuransi'] = $this->getPilihanSatuanKerja('UK.002');
            $this->data['pil_jasa_kesehatan'] = $this->getPilihanSatuanKerja('UK.003');
            $this->data['mimes_image'] = $this->file->getMimeTypes($this->mod->file_type_image);
            $this->data['mimes_lampiran'] = $this->file->getMimeTypes($this->mod->file_type_lampiran);
            $this->data['keterangan_upload_image'] = '*) File Type : '.$this->mod->file_type_image.', Max Size : '.($this->mod->max_size / 1024 /1024).'Mb';
            $this->data['keterangan_upload_lampiran'] = '*) File Type : '.$this->mod->file_type_lampiran.', Max Size : '.($this->mod->max_size / 1024 /1024).'Mb';
            
			switch($this->module){
                case '' : require_once 'backend/dashboard.php'; break;
				case 'index' : require_once 'backend/dashboard.php'; break;
				case 'dashboard' : require_once 'backend/dashboard.php'; break;
				case 'profil' : require_once 'backend/profil.php'; break;
                case 'berita' : require_once 'backend/berita.php'; break;
                case 'laporan' : require_once 'backend/laporan.php'; break;
                case 'masyarakat' : require_once 'backend/masyarakat.php'; break;
                case 'operator' : require_once 'backend/operator.php'; break;
                case 'asuransi' : require_once 'backend/asuransi.php'; break;
                case 'layanan_kesehatan' : require_once 'backend/layanan_kesehatan.php'; break;
                case 'info_laka' : require_once 'backend/info_laka.php'; break;
                case 'rekam_medis' : require_once 'backend/rekam_medis.php'; break;
                case 'kwintansi' : require_once 'backend/kwintansi.php'; break;
                case 'surat_jaminan' : require_once 'backend/surat_jaminan.php'; break;
                case 'bukti_pembayaran' : require_once 'backend/bukti_pembayaran.php'; break;
                case 'logout' : $this->logout(); break;
                case 'sms' : $this->sendSMS(); break;
				default : $this->mod->showView('page/error', $this->data);
			}
        }
        else{
            require_once 'backend/login.php';
        }
    }
}

new backend($librarys, $params);
?>
