<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class frontend extends service{

    public function __construct($librarys, $params){
        parent::__construct($librarys, $params);
        $this->librarys = $librarys;
        $this->params = $params;
        $this->mod->setTemplate('frontend');
		$this->mod->getComponent();
        $this->load();
    }
    
    private function load(){
        $this->data['link_backend'] = $this->link_backend;
        $this->data['link_frontend'] = $this->link_frontend;
        $this->data['link_file_image'] = $this->link_file_image;
        $this->data['link_file_lampiran'] = $this->link_file_lampiran;
        $this->data['link_berita'] = $this->link_frontend.'berita/';
        $this->data['navbar'] = $this->mod->Config['navbar_frontend'];
        $this->data['dataProfil'] = $this->getInfoProfil();
        switch($this->module){
            case '' : require_once 'frontend/dashboard.php'; break;
            case 'layanan' : require_once 'frontend/layanan.php'; break;
            case 'masyarakat' : require_once 'frontend/masyarakat.php'; break;
            case 'berita' : require_once 'frontend/berita.php'; break;
            case 'api' : require_once 'frontend/api.php'; break;
            case 'profil' : $this->mod->showView('profil', $this->data); break;
            default : $this->mod->subView($this->module, $this->data);
        }
    }
}

new frontend($librarys, $params);
?>
