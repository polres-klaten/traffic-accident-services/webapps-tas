<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class dashboard extends backend{

    public function __construct($librarys, $params){
        parent::__construct($librarys, $params);
        $this->data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
		$this->data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $method = $this->method;
        $data = $this->data;
        // $this->mod->desSession();
        // $this->debugResponse($_SESSION); die;
        $data['infoLaka'] = $this->getInfoLaka(array('tahun' => $this->mod->tahunAktif, 'bulan' => ''));
        switch ($method) {
            case '':
                switch ($this->session_login['unit_kerja_id']) {
                    case 'UK.001':
                        $this->dashboard_polres($this->id, $data);
                        break;

                    case 'UK.002':
                        $this->dashboard_asuransi($this->id, $data);
                        break;

                    case 'UK.003':
                        $this->dashboard_kesehatan($this->id, $data);
                        break;
                    
                    default:
                        $this->mod->showView('page/error', $data);
                        break;
                }
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
        header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
    }
    
    private function dashboard_polres($id, $data){
        $data['inboxRekamMedis'] = $this->getInboxRekamMedis('pengajuan');
        $this->mod->showView(__CLASS__.'/dashboard-polres', $data);
    }

    private function dashboard_asuransi($id, $data){
        $data['pil_jasa_kesehatan'] = $this->getPilihanSatuanKerja('UK.003');
        $data['inboxLaporanKepolisian'] = $this->getInboxLaporanKepolisian($this->session_login['id_satuan_kerja']);
        $data['inboxSuratJaminan'] = $this->getInboxSuratJaminan($this->session_login['id_satuan_kerja']);
        $data['inboxBuktiPembayaran'] = $this->getInboxBuktiPembayaran($this->session_login['id_satuan_kerja']);
        $this->mod->showView(__CLASS__.'/dashboard-asuransi', $data);
    }

    private function dashboard_kesehatan($id, $data){
        $data['pil_jasa_asuransi'] = $this->getPilihanSatuanKerja('UK.002');
        $data['inboxRekamMedis'] = $this->getInboxRekamMedis('permintaan');
        $data['inboxKwintansi'] = $this->getInboxKwintansi($this->session_login['id_satuan_kerja']);
        $this->mod->showView(__CLASS__.'/dashboard-kesehatan', $data);
    }

    private function statistikInfoLaka(){
        $info_laka = $this->getInfoLaka($_POST);
        $this->showResponse($info_laka);
    }

    private function inbox($id, $data){
        $result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		switch ($id) {
			case 'action':
				if(isset($_POST['id'])){
					$inbox_session = array(
                        'id' => $_POST['id'],
                        'action' => $_POST['action'],
                    );
					$this->mod->setSession('SESSION_INBOX', $inbox_session);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/'.__CLASS__.'/inbox/'.$inbox_session['action'];
				}
				$this->showResponse($result);
                break;

            case 'laporan':
                $inbox_session = $this->mod->getSession('SESSION_INBOX');
				$formLaporan = $this->getDataLaporanKepolisian($inbox_session['id']);
				if(empty($formLaporan)){
					// Check jika data laporan kosong maka redirect ke menu laporan
					echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'/'.__CLASS__.'\'" />'; die;
				}
				$dataForm = $this->berkasPolres($formLaporan);
				$dataForm += $this->berkasKesehatan($formLaporan);
				$dataForm += $this->berkasAsuransi($formLaporan);
				// $this->debugResponse($dataForm); die;
                $data = $this->data;
                $data += $this->pilInfoKabKecDes($dataForm);
				$data['dataForm'] = $dataForm;
                $data['page_title'] = 'Detail Laporan';
				$this->mod->showView('laporan/detail', $data);
                break;
                
            case 'rekam-medis':
                $inbox_session = $this->mod->getSession('SESSION_INBOX');
                $formRekamMedis = $this->getDataRekamMedis($inbox_session['id']);
                $formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('rekam_medis_id', $inbox_session['id']));
                $dataForm['rekam_medis'] = $this->extractJSON($formRekamMedis);
                $dataForm += $this->berkasPolres($formLaporan);
                // $this->debugResponse($dataForm); die;
                $data = $this->data;
                $data += $this->pilInfoKabKecDes($dataForm);
                $data['dataForm'] = $dataForm;
                $data['page_title'] = 'Kiriman Surat Keterangan Laka';
                // check user access
                switch ($this->session_login['unit_kerja_id']) {
                    case 'UK.001':
                        $this->mod->showView(__CLASS__.'/inbox-rekam-medis', $data);
                        break;

                    case 'UK.003':
                        $data['dataForm']['rekam_medis']['status_rekam_medis'] = 'diterima';
                        $data['link_script'] = $this->link_backend.'/rekam_medis/script';
                        $this->mod->showView('rekam_medis/form', $data);
                        break;

                    default:
                        $this->mod->showView('page/error', $data);
                        break;
                }
                break;

            case 'kwintansi':
                $inbox_session = $this->mod->getSession('SESSION_INBOX');
                $formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', $inbox_session['id']));
                $dataForm = $this->berkasPolres($formLaporan);
                $dataForm += $this->berkasKesehatan($formLaporan);
                $dataForm += $this->berkasAsuransi($formLaporan);
                $dataForm['formKwintansi'] = $this->extractJSON($dataForm['kwintansi'][0]);
                $dataForm['formKwintansi']['laporan_kepolisian_id'] = $formLaporan['id_laporan_kepolisian'];
                // $this->debugResponse($dataForm); die;
                $data = $this->data;
                $data += $this->pilInfoKabKecDes($dataForm);
                $data['dataForm'] = $dataForm;
                $data['page_title'] = 'Kwintansi Pembayaran';
                // check user access
                switch ($this->session_login['unit_kerja_id']) {
                    case 'UK.003':
                        $this->mod->showView(__CLASS__.'/inbox-kwintansi', $data);
                        break;

                    default:
                        $this->mod->showView('page/error', $data);
                        break;
                }
                break;

            case 'surat-jaminan':
                $inbox_session = $this->mod->getSession('SESSION_INBOX');
                $formSuratJaminan = $this->getFormSuratJaminan( $inbox_session['id'], $this->session_login['id_satuan_kerja']);
                $formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', $inbox_session['id']));
                $dataForm = $this->berkasPolres($formLaporan);
                $dataForm += $this->berkasKesehatan($formLaporan);
                $dataForm += $this->berkasAsuransi($formLaporan);
                $dataForm['formSuratJaminan'] = $this->extractJSON($formSuratJaminan);
                // $this->debugResponse($dataForm); die;
                $data = $this->data;
                $data += $this->pilInfoKabKecDes($dataForm);
                $data['dataForm'] = $dataForm;
                $data['page_title'] = 'Surat Jaminan';
                // check user access
                switch ($this->session_login['unit_kerja_id']) {
                    case 'UK.002':
                        $this->mod->showView(__CLASS__.'/inbox-surat-jaminan', $data);
                        break;

                    default:
                        $this->mod->showView('page/error', $data);
                        break;
                }
                break;

            case 'bukti-pembayaran':
                $inbox_session = $this->mod->getSession('SESSION_INBOX');
                $formBuktiPembayaran = $this->getFormBuktiPembayaran( $inbox_session['id'], $this->session_login['id_satuan_kerja']);
                $formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', $inbox_session['id']));
                $dataForm = $this->berkasPolres($formLaporan);
                $dataForm += $this->berkasKesehatan($formLaporan);
                $dataForm += $this->berkasAsuransi($formLaporan);
                $dataForm['formBuktiPembayaran'] = $this->extractJSON($formBuktiPembayaran);
                // $this->debugResponse($dataForm); die;
                $data = $this->data;
                $data += $this->pilInfoKabKecDes($dataForm);
                $data['dataForm'] = $dataForm;
                $data['page_title'] = 'Bukti Pembayaran';
                // check user access
                switch ($this->session_login['unit_kerja_id']) {
                    case 'UK.002':
                        $this->mod->showView(__CLASS__.'/inbox-bukti-pembayaran', $data);
                        break;

                    default:
                        $this->mod->showView('page/error', $data);
                        break;
                }
                break;
			
			default:
                $this->mod->showView('page/error', $data);
				break;
		}
    }

    private function action($id, $data){
        $result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		switch ($id) {
            case 'rekam-medis':
                // update status rekam medis
                $_POST['pesan'] = ($_POST['status'] == 'non-lp') ? 'Dibayar umum' : $_POST['pesan'];
                $update = array('status_rekam_medis' => $_POST['status'], 'keterangan_lainnya' => $_POST['pesan']);
                $result = $this->db->update('tref_rekam_medis', $update, array('id_rekam_medis' => $_POST['id']));
                // jika status rekam medis diterima maka redirect ke form rekam medis
                if($_POST['status'] == 'diterima'){
                    // buat laporan kepolisian
                    $formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', ''));
                    $formLaporan['rekam_medis_id'] = $_POST['id'];
                    $formLaporan['satker_asal_laporan_kepolisian'] = $this->session_login['id_satuan_kerja'];
                    $dataLaporan = $this->extractJSON($formLaporan);
                    foreach ($dataLaporan as $key => $value) {if(is_array($value)) $dataLaporan[$key] = json_encode($dataLaporan[$key]);}
                    $result = $this->db->save_update('tref_laporan_kepolisian', $dataLaporan);
                    // buat laporan identitas
                    $formIdentitas = $this->db->getDataTabel('tref_data_identitas', array('id_data_identitas', ''));
                    $formIdentitas['laporan_kepolisian_id'] = $formLaporan['id_laporan_kepolisian'];
                    $dataIdentitas = $this->extractJSON($formIdentitas);
                    foreach ($dataIdentitas as $key => $value) {if(is_array($value)) $dataIdentitas[$key] = json_encode($dataIdentitas[$key]);}
                    $result = $this->db->save_update('tref_data_identitas', $dataIdentitas);

                    $this->mod->setSession('SESSION_FORM_LAPORAN', $formLaporan['id_laporan_kepolisian']);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/laporan/form';
                }else{
                    $result['status'] = 'success';
                    $result['dataUrl'] = $this->link_backend.'/'.__CLASS__;
                }
                
                $this->showResponse($result);
                break;
			
			default:
                $this->showResponse($result);
				break;
		}
    }

    private function simpan($id){
        $result = array(
			'status' => 'error',
			'dataUrl' => ''
        );
        
        switch ($id) {
            case 'kwintansi':
                $data = $this->db->getDataTabel('tref_kwintansi', array('id_kwintansi', $_POST['id_kwintansi']));
                foreach($data as $key => $value){
                    // Check input post
                    if(is_array($value)){
                        $data[$key] = $this->convertIndexToAssoc($data[$key], $_POST[$key]);
                        $data[$key] = json_encode($data[$key]);
        
                    }else{
                        if(isset($_POST[$key])) $data[$key] = $_POST[$key];
                    }
        
                    // check input files
                    if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
                        $upload = $this->uploadLampiran($_FILES[$key]);
                        $data[$key] = $upload['UploadFile'];
                    }
                }
                $data['satker_asal_kwintansi'] = $this->session_login['id_satuan_kerja'];
                $result = $this->db->save_update('tref_kwintansi', $data);
                $this->error_msg = $this->mod->Config['errMsg']['kwintansi'];
                $this->showResponse($this->error_msg['simpan'][$result['error']]);
                break;

            case 'surat-jaminan':
                $data = $this->db->getDataTabel('tref_surat_jaminan', array('id_surat_jaminan', $_POST['id_surat_jaminan']));
                foreach($data as $key => $value){
                    // Check input post
                    if(is_array($value)){
                        $data[$key] = $this->convertIndexToAssoc($data[$key], $_POST[$key]);
                        $data[$key] = json_encode($data[$key]);
        
                    }else{
                        if(isset($_POST[$key])) $data[$key] = $_POST[$key];
                    }
        
                    // check input files
                    if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
                        $upload = $this->uploadLampiran($_FILES[$key]);
                        $data[$key] = $upload['UploadFile'];
                    }
                }
                $data['satker_asal_surat_jaminan'] = $this->session_login['id_satuan_kerja'];
                $result = $this->db->save_update('tref_surat_jaminan', $data);
                $this->error_msg = $this->mod->Config['errMsg']['surat_jaminan'];
                $this->showResponse($this->error_msg['simpan'][$result['error']]);
                break;

            case 'bukti-pembayaran':
                $data = $this->db->getDataTabel('tref_pembayaran', array('id_pembayaran', $_POST['id_pembayaran']));
                foreach($data as $key => $value){
                    // Check input post
                    if(is_array($value)){
                        $data[$key] = $this->convertIndexToAssoc($data[$key], $_POST[$key]);
                        $data[$key] = json_encode($data[$key]);
        
                    }else{
                        if(isset($_POST[$key])) $data[$key] = $_POST[$key];
                    }
        
                    // check input files
                    if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
                        $upload = $this->uploadLampiran($_FILES[$key]);
                        $data[$key] = $upload['UploadFile'];
                    }
                }
                $data['satker_asal_pembayaran'] = $this->session_login['id_satuan_kerja'];
                $result = $this->db->save_update('tref_pembayaran', $data);
                $this->error_msg = $this->mod->Config['errMsg']['bukti_pembayaran'];
                $this->showResponse($this->error_msg['simpan'][$result['error']]);
                break;
            
            default:
                $this->showResponse($result);
                break;
        }
	}

}

new dashboard($this->librarys, $this->params);
?>
