<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class info_laka extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionPolres();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$this->link_script = $this->link_backend.'/'.__CLASS__.'/script';
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Info Laka';
				$data['link_script'] = $this->link_script;
				$data['pil_kecamatan'] = $this->getPilihanKecamatan('3310'); // Kabupaten Klaten
				$data['pil_tahun'] = $this->getPilihanTahun();
				$data['pil_bulan'] = $this->mod->pilihanBulan;
				$this->mod->showView(__CLASS__.'/info_laka', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function charts(){
		$result = $this->getInfoGrafisLaka($_POST);
		$this->showResponse($result);
	}

	private function form(){
		$data['dataForm'] = $this->getDataInfoGrafisLaka($_POST);
		$data['form_title'] = "Update Info Laka";
		$this->mod->subView(__CLASS__.'/form', $data);
	}
	
	private function simpan(){
		$data = $this->getDataInfoGrafisLaka($_POST);
		foreach($data['form'] as $key => $value){
			// Check input post
			if(is_array($value)){
				$data['form'][$key] = $this->convertIndexToAssoc($data['form'][$key], $_POST[$key]);
				$data['form'][$key] = json_encode($data['form'][$key]);

			}else{
				if(isset($_POST[$key])) $data['form'][$key] = $_POST[$key];
			}
		}
		$result = $this->db->save_update('tref_info_laka', $data['form']);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
	}

}

new info_laka($this->librarys, $this->params);
?>
