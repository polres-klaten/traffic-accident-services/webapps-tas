<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class login extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['link_validate'] = $this->link_backend.'/'.__CLASS__.'/validate';
				$data['username'] = ''; //'0987655';
				$data['password'] = ''; //'hanif';
				$this->mod->showView('page/login', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function validate(){
        $login = $this->login($_POST);
		if($login['status'] === true){
			$this->mod->setSession('SESSION_LOGIN', $login['dataLogin']);
			$result['status'] = 1;
			$result['pesan'] = 'Sukses, Login Berhasil';
		}
		else{
			$result['status'] = 0;
			$result['pesan'] = 'Maaf, Login Gagal';
		}

		$this->showResponse($result);
	}

}

new login($this->librarys, $this->params);
?>


