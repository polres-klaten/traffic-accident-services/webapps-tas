<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class layanan_kesehatan extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionPolres();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$this->link_script = $this->link_backend.'/layanan_kesehatan/script';
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Daftar Layanan Kesehatan';
				$data['link_script'] = $this->link_script;
				$this->mod->showView(__CLASS__.'/layanan_kesehatan', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$_POST['unit'] = 'UK.003';
		$data = $this->getTabelSatuanKerja($_POST);
		$this->mod->subView(__CLASS__.'/tabel', $data);
	}

	private function form(){
		$data['dataForm'] = $this->getDataSatuanKerja($_POST['id']);
		$data['dataForm']['unit_kerja'] = 'UK.003';
		$data['dataForm']['form_title'] = str_replace('Satuan Kerja', 'Layanan Kesehatan', $data['dataForm']['form_title']);
		$this->mod->subView(__CLASS__.'/form', $data);
	}
	
	private function simpan(){
		$data = $this->getDataSatuanKerja($_POST['id_satuan_kerja']);
		foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
		$data['unit_kerja_id'] = 'UK.003';
		unset($data['form_title']);
		$result = $this->db->save_update('tref_satuan_kerja', $data);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
	}

	private function hapus(){
		$result = $this->db->delete('tref_satuan_kerja', array('id_satuan_kerja' => $_POST['id']));
		$this->showResponse($this->error_msg['hapus'][$result['error']]);
	}

}

new layanan_kesehatan($this->librarys, $this->params);
?>
