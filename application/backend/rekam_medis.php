<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class rekam_medis extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionKesehatan();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$this->data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
		$this->data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Daftar Surat Keterangan Laka';
				$this->mod->showView(__CLASS__.'/rekam_medis', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$_POST['unit'] = 'UK.002';
		$data = $this->getTabelRekamMedis($_POST);
		$this->mod->subView(__CLASS__.'/tabel', $data);
	}

	private function form($id){
		$result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		switch ($id) {
			case 'action':
				if(isset($_POST['id'])){
					$this->mod->setSession('SESSION_FORM_RM', $_POST['id']);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/'.__CLASS__.'/form';
				}
				$this->showResponse($result);
				break;
			
			default:
				$IDRekamMedis = $this->mod->getSession('SESSION_FORM_RM');
				$formRekamMedis = $this->getDataRekamMedis($IDRekamMedis);
				$formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('rekam_medis_id', $formRekamMedis['id_rekam_medis']));
				// $this->debugResponse($_SESSION); die;
				if(empty($formRekamMedis)){
					// Check jika data laporan kosong maka redirect ke menu laporan
					echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'/'.__CLASS__.'\'" />'; die;
				}
				$dataForm['rekam_medis'] = $this->extractJSON($formRekamMedis);
				$dataForm += $this->berkasPolres($formLaporan);
				
				$data = $this->data;
				$data['dataForm'] = $dataForm;
				$data['page_title'] = $dataForm['rekam_medis']['form_title'];
				// $this->debugResponse($dataForm); die;
				$this->mod->showView(__CLASS__.'/form', $data);
				break;
		}
	}

	private function detail($id){
		$result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		switch ($id) {
			case 'action':
				if(isset($_POST['id'])){
					$this->mod->setSession('SESSION_FORM_RM', $_POST['id']);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/'.__CLASS__.'/detail';
				}
				$this->showResponse($result);
				break;
			
			default:
				$IDRekamMedis = $this->mod->getSession('SESSION_FORM_RM');
				$formRekamMedis = $this->getDataRekamMedis($IDRekamMedis);
				$formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('rekam_medis_id', $formRekamMedis['id_rekam_medis']));
				// $this->debugResponse($_SESSION); die;
				if(empty($formRekamMedis)){
					// Check jika data laporan kosong maka redirect ke menu laporan
					echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'/'.__CLASS__.'\'" />'; die;
				}
				$dataForm['rekam_medis'] = $this->extractJSON($formRekamMedis);
				$dataForm += $this->berkasPolres($formLaporan);
				
				$data = $this->data;
				$data += $this->pilInfoKabKecDes($dataForm);
				$data['dataForm'] = $dataForm;
				$data['page_title'] = 'Detail Surat Keterangan Laka';
				// $this->debugResponse($dataForm); die;
				$this->mod->showView(__CLASS__.'/detail', $data);
				break;
		}
	}
	
	private function simpan(){
		$formRekamMedis = $this->db->getDataTabel('tref_rekam_medis', array('id_rekam_medis', $_POST['id_rekam_medis']));
		$data = $this->extractJSON($formRekamMedis);
		foreach($data as $key => $value){
			// Check input post
			if(is_array($value)){
				$data[$key] = $this->convertIndexToAssoc($data[$key], $_POST[$key]);
				$data[$key] = json_encode($data[$key]);

			}else{
				if(isset($_POST[$key])) $data[$key] = $_POST[$key];
			}

			// check input files
			if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
				$upload = $this->uploadLampiran($_FILES[$key]);
				$data[$key] = $upload['UploadFile'];
			}
		}

		// Check jika asal dan tujuan rekam medis kosong, maka rs mengajukan rm ke polres
		if(empty($data['satker_asal_rekam_medis']) && empty($data['satker_tujuan_rekam_medis'])){
			$data['status_rekam_medis'] = 'baru';
			$data['jenis_rekam_medis'] = 'pengajuan';
			$data['satker_asal_rekam_medis'] = $this->session_login['id_satuan_kerja'];
			$data['satker_tujuan_rekam_medis'] 	= 'SK.001';
		}

		$result = $this->db->save_update('tref_rekam_medis', $data);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
	}

	private function hapus(){
		$result = $this->db->delete('tref_rekam_medis', array('id_rekam_medis' => $_POST['id']));
		$this->showResponse($this->error_msg['hapus'][$result['error']]);
	}

	private function rujukan($id){
		switch ($id) {
			case 'form':
				$data['id_laporan_kepolisian'] = $_POST['id'];
				$data['pil_rujukan'] = array('' => 'PILIH LAYANAN KESEHATAN') + $this->getPilihanSatuanKerja('UK.003');
				unset($data['pil_rujukan'][$this->session_login['id_satuan_kerja']]);
				$data['form_title'] = 'Rujukan Surat Keterangan Laka';
				$this->mod->subView(__CLASS__.'/form-rujukan', $data);
				break;

			case 'simpan':
				$pil_rujukan = $this->getPilihanSatuanKerja('UK.003');
				$id_rekam_medis_rujukan = 'RMD.'.date('YmdHis');
				$formLaporan = $this->getDataLaporanKepolisian($_POST['id_laporan_kepolisian']);
				$formRekamMedis = $this->getDataRekamMedis($formLaporan['rekam_medis_id']);
				$formRekamMedisRujukan = $formRekamMedis;
				$formLaporan['rekam_medis_id'] = $id_rekam_medis_rujukan;
				$formRekamMedisRujukan['id_rekam_medis'] = $id_rekam_medis_rujukan;
				$formRekamMedisRujukan['satker_asal_rekam_medis'] = $_POST['satker_rujukan'];
				$formRekamMedis['keterangan_lainnya'] = 'Telah dirujuk ke '.$pil_rujukan[$_POST['satker_rujukan']];
				unset($formLaporan['form_title']);
				unset($formRekamMedis['form_title']);
				unset($formRekamMedisRujukan['form_title']);
				$result = $this->db->save_update('tref_rekam_medis', $formRekamMedis);
				$result = $this->db->save_update('tref_rekam_medis', $formRekamMedisRujukan);
				$result = $this->db->save_update('tref_laporan_kepolisian', $formLaporan);
				$this->showResponse($this->error_msg['rujukan'][$result['error']]);
				break;
			
			default:
				# code...
				break;
		}
	}

}

new rekam_medis($this->librarys, $this->params);
?>
