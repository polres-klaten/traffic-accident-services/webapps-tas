/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    loadTabel: function(){
        $("#table-content").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>/tabel", $("#frmData").serialize(), function (data){
            $("#table-content").html(data);
        });
    },
    tabelPagging: function(number){
        $("#page").val(number);
        this.loadTabel();
        $(document).scrollTop(0);
    },
    showForm: function(id){
        $.post("<?= $url_path; ?>/form", {id: id}, function (data){
            $("#data-form-input").html(data);
            $("#frmInputModal").modal("show");
            $(".btnFooter").show();
            $(".loading").html("");
        });
    },
    simpan: function(obj){
        $(".btnFooter").hide();
        $(".loading").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        $.post("<?= $url_path; ?>/simpan", $(obj).serialize(), function (data){
            console.log(data);
            app.loadTabel();
            $("#frmInputModal").modal("hide");
            $.bootstrapGrowl("<h4><strong>"+data.title+"</strong></h4> <p>"+data.text+"</p>", {
                type: data.type,
                delay: 3000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 20}
            });
        }, "json");
    },
    hapus: function(id){
        swal({title: "Konfirmasi Hapus",text: "Yakin data ini akan dihapus ?",type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Ya",cancelButtonText: "Tidak",closeOnConfirm: false,closeOnCancel: true }, function(isConfirm){
            if(isConfirm){
                $.post("<?= $url_path; ?>/hapus", {id : id}, function(data){
                    swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},function(){ app.loadTabel(); });
                }, "json");
            }
        });
    },
};