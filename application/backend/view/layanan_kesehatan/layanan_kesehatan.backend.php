<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    
                    <div class="block">
                        <div class="block-title">
                            <h2 id="table-title">Data Layanan </h2><small id="j-table-title"></small>
                            <div class="block-options pull-right">
                                <button id="" class="btn btn-effect-ripple btn-info btnForm" data-toggle="tooltip" data-original-title="Tambah Data"><i class="fa fa-plus"></i> Tambah Layanan</button>
                            </div>
                        </div>
                        <form id="frmData" onsubmit="return false;" autocomplete="off">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="cari"><small>Cari Nama Layanan : </small></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        <?= Bootstrap::inputText('cari', 'text', '', 'class="form-control" placeholder="..."'); ?>
                                    </div>
                                </div>
                            </div>
                            <?= Bootstrap::inputKey('page', '1'); ?>
                        </form>
                        <div id="table-content"></div>
                    </div>
                    
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        app.loadTabel();
        $(document).on("click", ".paging", function(){
            app.tabelPagging($(this).attr("number-page"));
        });
        $(document).on("click", ".btnForm", function(){
            app.showForm(this.id);
        });
        $(document).on("click", ".btnDelete", function(){
            app.hapus(this.id);
        });
        $(document).on("submit", "#frmInput", function(){
            app.simpan(this);
        });
        $(document).on("change", "#cari", function(){
            $("#page").val(1);
            app.loadTabel();
        });
    </script>
</body>