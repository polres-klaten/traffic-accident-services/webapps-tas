<?= $query; ?>
<style>
	table{
		font-size: 10pt;
	}
</style>
<div class="table-responsive">
	<table class="table table-default table-hover">
	<thead>
		<tr>
			<th width="50px"><small>#</small></th>
			<th><small><strong>Layanan Kesehatan</strong></small></th>
			<th><small><strong>Alamat</strong></small></th>
			<th><small><strong>No. Telepon</strong></small></th>
			<th><small><strong>Website</strong></small></th>
			<th><small><strong>Email</strong></small></th>
			<th width="120px"></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach($dataTabel as $kol){
		echo '<tr>';
		echo '<td>'.$no++.'</td>';
		echo '<td>'.$kol['nama_satuan_kerja'].'</td>';
		echo '<td>'.$kol['alamat_satuan_kerja'].'</td>';
		echo '<td>'.$kol['telepon_satuan_kerja'].'</td>';
		echo '<td>'.$kol['website_satuan_kerja'].'</td>';
		echo '<td>'.$kol['email_satuan_kerja'].'</td>';
		echo '<td>
				<button id="'.$kol['id_satuan_kerja'].'" data-toggle="tooltip" data-original-title="Ubah Data" class="btn btn-effect-ripple btn-info btnForm"><i class="fa fa-pencil"></i></button>
				<button id="'.$kol['id_satuan_kerja'].'" data-toggle="tooltip" data-original-title="Hapus Data" class="btn btn-effect-ripple btn-danger btnDelete"><i class="fa fa-trash"></i></button>
			  </td>';
		echo '</tr>';
	}
	echo (empty($dataTabel)) ? '<tr><td colspan="7">Data tidak ditemukan</td></tr>' : '';
	?>
	</tbody>
	</table>
</div>
<?= ($jmlData > 0)? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Layanan");
	$("[data-toggle='tooltip']").tooltip();
</script>
