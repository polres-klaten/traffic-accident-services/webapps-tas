<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    
                    <div class="block">
                        <div class="block-title">
                            <h2 id="table-title">Profil </h2>
                        </div>
                        <form id="frmProfil" onsubmit="return false;" class="form-bordered" autocomplete="off">
                            <?php extract($dataForm); ?>
                            <div class="form-group">
                                <label for="nama_satuan_kerja">Nama satuan_kerja. : </label>
                                <?= Bootstrap::inputKey('id_satuan_kerja', $id_satuan_kerja); ?>
                                <?= Bootstrap::inputKey('unit_kerja_id', $unit_kerja_id); ?>
                                <?= Bootstrap::inputText('nama_satuan_kerja', 'text', $nama_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                            <div class="form-group">
                                <label for="alamat_satuan_kerja">Alamat : </label>
                                <?= Bootstrap::inputText('alamat_satuan_kerja', 'text', $alamat_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                            <div class="form-group">
                                <label for="telepon_satuan_kerja">No. Telepon/Fax : </label>
                                <?= Bootstrap::inputText('telepon_satuan_kerja', 'text', $telepon_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                            <div class="form-group">
                                <label for="facebook_satuan_kerja">Facebook : </label>
                                <?= Bootstrap::inputText('facebook_satuan_kerja', 'text', $facebook_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                            <div class="form-group">
                                <label for="instagram_satuan_kerja">Instagram : </label>
                                <?= Bootstrap::inputText('instagram_satuan_kerja', 'text', $instagram_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                            <div class="form-group">
                                <label for="isi_profil">Isi Profil</label>
                                <?= Bootstrap::inputTextArea('profil_satuan_kerja', $profil_satuan_kerja, 'class="form-control" required'); ?>
                            </div>
                        </form>
                        <div style="padding: 10px;">
                            <button id="" class="btn btn-effect-ripple btn-info btnUpdate" data-toggle="tooltip" data-original-title="Update Data"><i class="fa fa-refresh"></i> Simpan</button>
                        </div>
                    </div>
                    
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        CKEDITOR.replace("profil_satuan_kerja",{
            height : 400,
            shiftEnterMode : CKEDITOR.ENTER_P,
            toolbar: [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
                { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
                { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
                // { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
                '/',
                { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                { name: 'others', items: [ '-' ] },
                { name: 'about', items: [ 'About' ] }
                ]
        });
        CKEDITOR.on('instanceReady', function (ev){
            ev.editor.on("change", function(){
                $('#profil_satuan_kerja').val(ev.editor.getData());
            });
        });
        $(document).on("click", ".btnUpdate", function(){
            $("#frmProfil").submit();
        });
        $(document).on("submit", "#frmProfil", function(){
            app.simpan(this);
        });
    </script>
</body>