/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    simpan: function(obj){
        $.post("<?= $url_path; ?>/simpan", $(obj).serialize(), function (data){
            $("#frmInputModal").modal("hide");
            console.log(data);
            $.bootstrapGrowl("<h4><strong>"+data.title+"</strong></h4> <p>"+data.text+"</p>", {
                type: data.type,
                delay: 3000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 20}
            });
        }, "json");
    },
};