<?php extract($dataForm); ?>
<div class="form-group">
    <label for="nrp_operator">NRP/NIP. : </label>
    <?= Bootstrap::inputKey('id_operator', $id_operator); ?>
    <?= Bootstrap::inputText('nrp_nip_operator', 'text', $nrp_nip_operator, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="pangkat_operator">Pangkat/Jabatan : </label>
    <?= Bootstrap::inputText('pangkat_jabatan_operator', 'text', $pangkat_jabatan_operator, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="nama_operator">Nama operator : </label>
    <?= Bootstrap::inputText('nama_operator', 'text', $nama_operator, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="password_operator">Password : </label>
    <?= Bootstrap::inputText('password_operator', 'text', FUNC::decryptor($password_operator), 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="nohp_operator">No. Handphone : </label>
    <?= Bootstrap::inputText('nohp_operator', 'text', $nohp_operator, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="unit_kerja_id">Unit Kerja : </label>
    <?= Bootstrap::inputSelect('unit_kerja_id', $pil_unit, $unit_kerja, 'class="select-chosen filter-option" data-link="'.$link_pilihan_satker.'" data-target="satuan_kerja_id" data-value="'.$satuan_kerja_id.'" required'); ?>
</div>
<div class="form-group">
    <label for="satuan_kerja_id">Satuan Kerja : </label>
    <div class="satuan_kerja_id">
        <?= Bootstrap::inputSelect('satuan_kerja_id', $pil_satker, '', 'class="select-chosen" required'); ?>
    </div>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
    $("#unit_kerja_id").trigger("change");
    $(".select-chosen").chosen({width: "100%"});
</script>