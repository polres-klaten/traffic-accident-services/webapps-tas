<body>
    <img src="<?= $this->templatePath; ?>img/bg-login.png" alt="Full Background" class="full-bg animation-pulseSlow">
    <!-- Login Container -->
    <div id="login-container" style="top: 0px;">
        <!-- Login Header -->
        <h1 class="h2 text-light text-center push-top-bottom animation-pullDown" style="margin-bottom: 0px;">
            <img src="<?= $this->templatePath; ?>img/logo-login.png" alt="" class="push-top-bottom animation-pullDown">
        </h1>
        <!-- END Login Header -->

        <!-- Login Block -->
        <div class="block animation-fadeInQuick" style="opacity: .8">
            <!-- Login Title -->
            <div class="block-title">
                <h2>Please Login</h2>
            </div>
            <!-- END Login Title -->

            <!-- Login Form -->
            <form id="frmSignin" class="form-horizontal" onsubmit="return false" autocomplete="off">
                <div class="form-group">
                    <label for="username" class="col-xs-12">NRP/NIP</label>
                    <div class="col-xs-12">
                        <?= Bootstrap::inputText('username', 'text', $username, 'class="form-control" required'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="col-xs-12">Password</label>
                    <div class="col-xs-12">
                        <?= Bootstrap::inputText('password', 'Password', $password, 'class="form-control" required'); ?>
                    </div>
                </div>
                <div class="form-group form-actions">
                    <div class="col-xs-8"><div class="loading"></div></div>
                    <div class="col-xs-4 text-right">
                        <button type="submit" class="btn btn-effect-ripple btn-sm btn-success btnLogin">Log In</button>
                    </div>
                </div>
            </form>
            <!-- END Login Form -->

        </div>
        <!-- END Login Block -->

        <!-- Footer -->
        <footer class="text-muted text-center animation-pullUp">
            <small><?= $this->web_footer; ?></small>
        </footer>
        <!-- END Footer -->
    </div>
    <!-- END Login Container -->

    <!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
    <script src="<?= $this->templatePath; ?>js/vendor/jquery-2.2.4.min.js"></script>
    <script src="<?= $this->templatePath; ?>js/vendor/bootstrap.min.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script>
        $(document).on("submit", "#frmSignin", function(){
            // alert($(this).serialize());
            validate(this);
        });
        var validate = function(obj){
            $(".btnLogin").hide();
            $(".loading").html("<span style='font-style:italic;'>Authenticating..</span>");
            $.post("<?= $link_validate; ?>", $(obj).serialize(), function(data){
                // console.log(data);
                $(".loading").html(data.pesan);
                if(data.status === 1){
                    setTimeout(function(){window.location.reload();}, 500);
                }
                else{
                    setTimeout(function(){$(".btnLogin").show();$(".loading").html("");}, 500);
                }
            }, "json");
        };
    </script>
</body>