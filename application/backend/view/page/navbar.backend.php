<style>
    .sidebar-nav ul {display: block;}
    .chosen-container-active.chosen-with-drop .chosen-single div b {
        background-position: -18px 5px;
    }
</style>
<div id="sidebar">
    <div id="sidebar-brand" class="themed-background">
        <a class="sidebar-title">
            <!-- <img src="<?= $this->templatePath; ?>img/logo-tas.png" alt="avatar"> -->
            <img src="<?= $this->templatePath; ?>img/logo-polres.png" width="100px" alt="avatar">
        </a>
    </div>
    <div id="sidebar-scroll">
        <div class="sidebar-content">
            <ul class="sidebar-nav">
                <li><a><div align="center"><img alt="" src="<?= $this->templatePath; ?>img/logo-login.png" width="100px"></div></a></li>
                <li><a class="active"><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide"><?= $this->admin_header; ?></span></a></li>
                <li class="sidebar-separator"><i class="fa fa-ellipsis-h"></i></li>
                <li>
                    <a>Tahun Laporan</a>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?= Bootstrap::inputSelect('tahunAktif', $this->pilihanTahun, $this->tahunAktif, 'class="panel-konfigurasi select-chosen" data-placeholder="Pilih Tahun.."'); ?>
                        </div>
                    </div>
                </li>
                <li>
                    <a>Bulan Laporan</a>
                    <div class="form-group">
                        <div class="col-md-12">
                            <?php $this->pilihanBulan += array('' => 'Semua Bulan'); ?>
                            <?= Bootstrap::inputSelect('bulanAktif', $this->pilihanBulan, $this->bulanAktif, 'class="panel-konfigurasi select-chosen" data-placeholder="Pilih Bulan.."'); ?>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="sidebar-section sidebar-nav-mini-hide">
                <br>
                <table class="table table-bordered" style="font-size: 8pt">
                    <tr>
                        <th><small><strong>Info Laka </strong></small></th>
                        <th><small><strong>Jumlah</strong></small></th>
                    </tr>
                    <tr>
                        <td><small><strong>Luka Ringan</strong></small></td>
                        <td><small><strong id="jumlah_lr">0</strong></small></td>
                    </tr>
                    <tr>
                        <td><small><strong>Luka Berat</strong></small></td>
                        <td><small><strong id="jumlah_lb">0</strong></small></td>
                    </tr>
                    <tr>
                        <td><small><strong>Meninggal Dunia</strong></small></td>
                        <td><small><strong id="jumlah_md">0</strong></small></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
        <div class="text-center"><p><?= $this->admin_footer; ?></p></div>
    </div>
</div>
<script>
$(".panel-konfigurasi").trigger("change");
$(document).on("change", ".panel-konfigurasi", function(){
    var bln = $("#bulanAktif").val();
    var thn = $("#tahunAktif").val();
    $.post("<?= $link_backend.'/dashboard/statistikInfoLaka'; ?>", {bulan: bln, tahun: thn}, function (data){
        $("#jumlah_lr").html(data.jumlah_lr);
        $("#jumlah_lb").html(data.jumlah_lb);
        $("#jumlah_md").html(data.jumlah_md);
    }, "json");
});
</script>