<header class="navbar navbar-inverse navbar-fixed-top">
    <ul class="nav navbar-nav-custom">
        <li>
            <a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
                <i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
                <i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
            </a>
        </li>
        <li class="animation-fadeInQuick visible-xs"><a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar-alt');this.blur();"><i class="gi gi-settings"></i></a></li>
        <?php
            foreach($navbar as $nav => $value){
                $icon = $value['icon'].'&nbsp;';
                $link = $link_backend.'/'.$value['link'];
                echo '<li class="animation-fadeInQuick hidden-xs"><a href="'.$link.'">'.$icon.$nav.'</a></li>';
            }
        ?>
    </ul>
    <ul class="nav navbar-nav-custom pull-right">
        <li class="dropdown">
            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                <span><?= $session_login['nama_satuan_kerja']; ?>&nbsp;&nbsp;</span>
                <img src="<?= $this->templatePath; ?>img/placeholders/avatars/avatar.jpg" alt="avatar">
            </a>
            <ul class="dropdown-menu dropdown-menu-right">
                <li class="dropdown-header"><strong><?= $session_login['nama_operator']; ?></strong></li>
                <li><a href="<?= $link_frontend; ?>" target="_blank"><i class="fa fa-globe pull-right"></i>View Web</a></li>
                <li><a href="<?= $link_logout; ?>"><i class="gi gi-lock fa-fw pull-right"></i>Keluar</a></li>
            </ul>
        </li>
    </ul>
</header>

<div id="sidebar-alt" tabindex="-1" aria-hidden="true" style="background: rgba(255, 255, 255, 1);">
    <a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i class="fa fa-times"></i></a>
    <div id="sidebar-scroll-alt">
        <div class="sidebar-content">
            <div class="sidebar-section">
                <h2 class="text-dark">Main Menu</h2>
                <ul class="nav navbar-nav-custom">
                    <?php
                        foreach($navbar as $nav => $value){
                            $icon = $value['icon'].'&nbsp;';
                            $link = $link_backend.'/'.$value['link'];
                            echo '<li class="animation-fadeInQuick"><a href="'.$link.'">'.$icon.$nav.'</a></li>';
                        }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</div>