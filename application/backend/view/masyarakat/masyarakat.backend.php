<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    <div class="block">
                        <div class="block-title">
                            <h2 id="table-title"><?= $page_title; ?> </h2><small id="j-table-title"></small>
                        </div>
                        <form id="frmData" onsubmit="return false;" autocomplete="off">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="cari"><small>Cari Laporan : </small></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        <?= Bootstrap::inputText('cari', 'text', '', 'class="form-control" placeholder="Cari berdasarkan nama, no ktp, no hp pelapor dan isi laporan"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="satker"><small>Status Laporan : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('status', $pil_status, '', 'class="select-chosen filter-option"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="satker"><small>Publish Laporan : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('publish', $pil_publish, '', 'class="select-chosen filter-option"'); ?>
                                    </div>
                                </div>
                            </div>
                            <?= Bootstrap::inputKey('page', '1'); ?>
                            <?= Bootstrap::inputKey('bulan', $bulanAktif); ?>
                            <?= Bootstrap::inputKey('tahun', $tahunAktif); ?>
                        </form>
                        <div id="table-content"></div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $("#publish").val("");
        app.loadTabel();
        $(document).on("click", ".paging", function(){
            app.tabelPagging($(this).attr("number-page"));
        });
        $(document).on("click", ".btnForm", function(){
            app.showForm(this.id);
        });
        $(document).on("click", ".btnDelete", function(){
            app.hapus(this.id);
        });
        $(document).on("submit", "#frmInput", function(){
            app.simpan(this);
        });
    </script>
</body>