<?= $query; ?>
<style>
.widget-content {
    padding: 15px 0px;
}
</style>
<div class="widget-content">
    <ul class="media-list">
    	<?php
		foreach($dataTabel as $kol){
		    echo '<li class="media">';
		    echo '	<div class="row">';
		    echo '		<div class=" col-md-10">';
		    echo '			<a href="javascript:void(0)" class="pull-left">';
		    echo '				<img src="'.$link_file_image.$kol['gambar_laporan_masyarakat'].'" alt="Gambar" width="150px" class="img-thumbnail">';
			echo '			</a>';
		    echo '			<div class="media-body">';
			echo '				<a href="javascript:void(0)"><strong>'.$kol['nama_masyarakat'].'</strong></a><br>';
			echo '				<span class="label label-'.$this->pilihanClassColorStatusLaporanMasyarakat[$kol['status_laporan_masyarakat']].'">'.$this->pilihanStatusLaporanMasyarakat[$kol['status_laporan_masyarakat']].'</span>';
			echo '				<small><i class="fa fa-calendar"></i> '.FUNC::tanggal($kol['tanggal_laporan_masyarakat'], 'long_date').' | <i class="fa fa-phone-square"></i> '.$kol['nohp_masyarakat'].' | '.$this->pilihanInconPublishLaporanMasyarakat[$kol['publish_laporan_masyarakat']].'</small><br>';
		    echo '				<p>'.substr(strip_tags($kol['isi_laporan_masyarakat']), 0, 200).'...</p>';
		    echo '			</div>';
		    echo '		</div>';
		    echo '		<div class="col-md-2">';
		    echo '			<button id="'.$kol['id_laporan_masyarakat'].'" data-toggle="tooltip" data-original-title="Lihat Data" class="btn btn-effect-ripple btn-info btnForm"><i class="fa fa-eye"></i></button>';
		    echo '			<button id="'.$kol['id_laporan_masyarakat'].'" data-toggle="tooltip" data-original-title="Hapus Data" class="btn btn-effect-ripple btn-danger btnDelete"><i class="fa fa-trash"></i></button>';
		    echo '		</div>';
		    echo '	</div>';
		    echo '</li>';
	    }
	    if(empty($dataTabel)){
	    	echo '<li class="media">';
		    echo '	<div class="media-body">';
		    echo '		<a href="javascript:void(0)"><strong>Maaf</strong></a>';
		    echo '		<p>Data masih kosong</p>';
		    echo '	</div>';
		    echo '</li>';
	    }
	    ?>
    </ul>
</div>
<?= ($jmlData > 0) ? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Laporan");
	$("[data-toggle='tooltip']").tooltip();
</script>
