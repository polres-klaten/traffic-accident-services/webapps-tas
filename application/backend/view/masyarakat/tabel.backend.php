<?= $query; ?>
<style>
	table{
		font-size: 10pt;
	}
</style>
<div class="table-responsive">
	<table class="table table-default table-hover">
	<thead>
		<tr>
			<th width="50px"><small>#</small></th>
			<th><small><strong>Tanggal Laporan</strong></small></th>
			<th><small><strong>Nama Pelapor</strong></small></th>
			<th><small><strong>No. KTP</strong></small></th>
			<th><small><strong>No. Handphone</strong></small></th>
			<th><small><strong>Isi Laporan</strong></small></th>
			<th><small><strong>Status Laporan</strong></small></th>
			<th width="100px"></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach($dataTabel as $kol){
		echo '<tr>';
		echo '<td>'.$no++.'</td>';
		echo '<td>'.FUNC::tanggal($kol['tanggal_laporan_masyarakat'], 'long_date').'</td>';
		echo '<td>'.$kol['nama_masyarakat'].'</td>';
		echo '<td>'.$kol['noktp_masyarakat'].'</td>';
		echo '<td>'.$kol['nohp_masyarakat'].'</td>';
		echo '<td>'.substr(strip_tags($kol['isi_laporan_masyarakat']), 0, 100).'...</td>';
		echo '<td>'.$this->pilihanStatusLaporanMasyarakat[$kol['status_laporan_masyarakat']].'</td>';
		echo '<td>
				<button id="'.$kol['id_laporan_masyarakat'].'" data-toggle="tooltip" data-original-title="Cek Laporan" class="btn btn-effect-ripple btn-info btnForm"><i class="fa fa-eye"></i></button>
				<button id="'.$kol['id_laporan_masyarakat'].'" data-toggle="tooltip" data-original-title="Hapus Data" class="btn btn-effect-ripple btn-danger btnDelete"><i class="fa fa-trash"></i></button>
			  </td>';
		echo '</tr>';
	}
	echo (empty($dataTabel)) ? '<tr><td colspan="8">Data tidak ditemukan</td></tr>' : '';
	?>
	</tbody>
	</table>
</div>
<?= ($jmlData > 0)? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Laporan");
	$("[data-toggle='tooltip']").tooltip();
</script>
