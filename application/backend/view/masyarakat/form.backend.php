<style>
input[type="file"].form-control {
    padding: 0px;
}
</style>
<?php extract($dataForm); ?>
<?= Bootstrap::inputKey('id_laporan_masyarakat', $id_laporan_masyarakat); ?>
<div class="row">
    <div class="col-md-8">
        <blockquote class="pull-left">
            <p><?= $isi_laporan_masyarakat; ?></p>
            <small><?= $nama_masyarakat; ?> | <i class="fa fa-phone-square"></i> <?= $nohp_masyarakat; ?></small>
        </blockquote>
        <label for="balasan_laporan_masyarakat">Balasan : </label>
        <?= Bootstrap::inputTextArea('balasan_laporan_masyarakat', $balasan_laporan_masyarakat, 'class="form-control" rows="4" requireds'); ?>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <img src="<?= $link_file_image.$gambar_laporan_masyarakat; ?>" class="img-thumbnail" alt="">
        </div>
        <div class="form-group">
            <label for="publish_laporan_masyarakat">Publish Laporan : </label>
            <?= Bootstrap::inputSelect('publish_laporan_masyarakat', $pil_publish, $publish_laporan_masyarakat, 'class="select-chosen" data-placeholder="Publish Laporan" required'); ?>
        </div>
        <div class="form-group">
            <label for="status_laporan_masyarakat">Validasi Laporan : </label>
            <?= Bootstrap::inputSelect('status_laporan_masyarakat', $pil_status, $status_laporan_masyarakat, 'class="select-chosen" data-placeholder="Validasi Laporan" required'); ?>
        </div>
    </div>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
    $(".select-chosen").chosen({width: "100%"});
</script>