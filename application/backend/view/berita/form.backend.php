<style>
input[type="file"].form-control {
    padding: 0px;
}
</style>
<?php extract($dataForm); ?>
<div class="form-group">
    <label for="judul_berita">Judul Berita : </label>
    <?= Bootstrap::inputKey('id_berita', $id_berita); ?>
    <?= Bootstrap::inputText('judul_berita', 'text', $judul_berita, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="keterangan_laporan">Gambar Berita : </label>
    <?= Bootstrap::inputText('files', 'file', '', 'class="form-control" requireds accept="'.$mimes_image.'"'); ?>
    <small><?= $keterangan_upload_image; ?></small>
</div>
<div class="form-group">
    <label for="isi_berita">Isi Berita</label>
    <?= Bootstrap::inputTextArea('isi_berita', $isi_berita, 'class="form-control" required'); ?>
</div>
<script>
    $("#formBeritaLabel").html("<?= $form_title; ?>");
    CKEDITOR.replace("isi_berita",{
        height : 400,
        shiftEnterMode : CKEDITOR.ENTER_P,
        toolbar: [
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
            { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
            { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
            // { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
            '/',
            { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
            { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
            { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
            { name: 'others', items: [ '-' ] },
            { name: 'about', items: [ 'About' ] }
            ]
    });
    CKEDITOR.on('instanceReady', function (ev){
        ev.editor.on("change", function(){
            $('#isi_berita').val(ev.editor.getData());
        });
    });
</script>