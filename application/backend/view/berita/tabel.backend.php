<?= $query; ?>
<style>
.widget-content {
    padding: 15px 0px;
}
</style>
<div class="widget-content">
    <ul class="media-list">
    	<?php
		foreach($dataTabel as $kol){
		    echo '<li class="media">';
		    echo '	<div class="row">';
		    echo '		<div class=" col-md-10">';
		    echo '			<a href="javascript:void(0)" class="pull-left">';
		    echo '				<img src="'.$link_file_image.$kol['gambar_berita'].'" alt="Gambar" width="150px" class="img-thumbnail">';
		    echo '			</a>';
		    echo '			<div class="media-body">';
		    echo '				<a href="javascript:void(0)"><strong>'.$kol['judul_berita'].'</strong></a>';
		    echo '				<p>'.substr(strip_tags($kol['isi_berita']), 0, 200).'...</p>';
		    echo '			</div>';
		    echo '		</div>';
		    echo '		<div class="col-md-2">';
		    echo '			<button id="'.$kol['id_berita'].'" data-toggle="tooltip" data-original-title="Ubah Data" class="btn btn-effect-ripple btn-info btnForm"><i class="fa fa-pencil"></i></button>';
		    echo '			<button id="'.$kol['id_berita'].'" data-toggle="tooltip" data-original-title="Hapus Data" class="btn btn-effect-ripple btn-danger btnDelete"><i class="fa fa-trash"></i></button>';
		    echo '		</div>';
		    echo '	</div>';
		    echo '</li>';
	    }
	    if(empty($dataTabel)){
	    	echo '<li class="media">';
		    echo '	<div class="media-body">';
		    echo '		<a href="javascript:void(0)"><strong>Maaf</strong></a>';
		    echo '		<p>Data masih kosong</p>';
		    echo '	</div>';
		    echo '</li>';
	    }
	    ?>
    </ul>
</div>
<?= ($jmlData > 0) ? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Berita");
	$("[data-toggle='tooltip']").tooltip();
</script>
