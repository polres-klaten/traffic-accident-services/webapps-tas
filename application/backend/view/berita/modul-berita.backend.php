<div class="block formBerita">
    <div class="block-title">
        <h2 id="formBeritaLabel"></h2>
    </div>
    <form id="frmBerita" onsubmit="return false;" class="form-bordered" autocomplete="off">
        <div id="form-content"></div>
        <button id="btnSimpan" type="submit"></button>
    </form>
    <div style="padding: 10px;">
        <div class="loading"></div>
        <button onclick="javascript:app.loadTabel();" class="btn btn-effect-ripple btn-danger btnFooter" data-toggle="tooltip" data-original-title="Tutup"> Tutup</button>
        <label for="btnSimpan" class="btn btn-effect-ripple btn-info btnFooter" data-toggle="tooltip" data-original-title="Simpan Data"><i class="fa fa-save"></i> Simpan</label>
    </div>
</div>
<div class="block listBerita">
    <div class="block-title">
        <h2 id="table-title"><?= $page_title; ?> </h2><small id="j-table-title"></small>
        <div class="block-options pull-right">
            <button id="" class="btn btn-effect-ripple btn-info btnForm" data-toggle="tooltip" data-original-title="Tambah Berita"><i class="fa fa-plus"></i> Tambah Berita</button>
        </div>
    </div>
    <form id="frmData" onsubmit="return false;" autocomplete="off">
        <div class="row">
            <div class="col-md-4">
                <label for="cari"><small>Cari Judul Berita : </small></label>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-search"></i></span>
                    <?= Bootstrap::inputText('cari', 'text', '', 'class="form-control" placeholder="..."'); ?>
                </div>
            </div>
        </div>
        <?= Bootstrap::inputKey('page', '1'); ?>
    </form>
    <div id="table-content"></div>
    </div>
</div>