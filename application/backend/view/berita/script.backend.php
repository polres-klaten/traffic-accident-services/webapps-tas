/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    loadTabel: function(){
        $(".formBerita").hide();
        $(".listBerita").show();
        $("#table-content").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>/tabel", $("#frmData").serialize(), function (data){
            $("#table-content").html(data);
        });
    },
    tabelPagging: function(number){
        $("#page").val(number);
        this.loadTabel();
        $(document).scrollTop(0);
    },
    showForm: function(id){
        $(".listBerita").hide();
        $(".formBerita").show();
        $.post("<?= $url_path; ?>/form", {id: id}, function (data){
            $("#form-content").html(data);
            $(".btnFooter").show();
            $(".loading").html("");
        });
    },
    simpan: function(obj){
        var data = new FormData($(obj)[0]);
        $(".btnFooter").hide();
        $(".loading").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        $.ajax({
            type: "POST",
            enctype: "multipart/form-data",
            url: "<?= $url_path; ?>/simpan",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            success: function (data){
                if(data.type === "error"){
                    swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},function(){ $(".btnFooter").show(); $(".loading").html("");});
                }else{
                    app.loadTabel();
                    $.bootstrapGrowl("<h4><strong>"+data.title+"</strong></h4> <p>"+data.text+"</p>", {
                        type: data.type,
                        delay: 3000,
                        allow_dismiss: true,
                        offset: {from: 'top', amount: 20}
                    });
                    console.log(data);
                }
            },
            error: function (e) {
                //
            }
        });
    },
    hapus: function(id){
        swal({title: "Konfirmasi Hapus",text: "Yakin data ini akan dihapus ?",type: "warning",showCancelButton: true,confirmButtonColor: "#DD6B55",confirmButtonText: "Ya",cancelButtonText: "Tidak",closeOnConfirm: false,closeOnCancel: true }, function(isConfirm){
            if(isConfirm){
                $.post("<?= $url_path; ?>/hapus", {id : id}, function(data){
                    swal({title: data.title, text: data.text, type: data.type, confirmButtonClass: "btn-primary", confirmButtonText: "OK",},function(){ app.loadTabel(); });
                }, "json");
            }
        });
    },
};