<?= $query; ?>
<style>
	table{
		font-size: 10pt;
	}
</style>
<div class="table-responsive">
	<table class="table table-default table-hover">
	<thead>
		<tr>
			<th width="50px"><small>#</small></th>
			<th><small><strong>Jumlah LR</strong></small></th>
			<th><small><strong>Jumlah LB</strong></small></th>
			<th><small><strong>Jumlah MD</strong></small></th>
			<th><small><strong>Bulan</strong></small></th>
			<th><small><strong>Tahun</strong></small></th>
			<th width="120px"></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach($dataTabel as $kol){
		echo '<tr>';
		echo '<td>'.$no++.'</td>';
		echo '<td>'.$kol['jumlah_lr'].'</td>';
		echo '<td>'.$kol['jumlah_lb'].'</td>';
		echo '<td>'.$kol['jumlah_md'].'</td>';
		echo '<td>'.$kol['bulan'].'</td>';
		echo '<td>'.$kol['tahun'].'</td>';
		echo '<td>
				<button id="'.$kol['id_info_laka'].'" data-toggle="tooltip" data-original-title="Ubah Data" class="btn btn-effect-ripple btn-info btnForm"><i class="fa fa-pencil"></i></button>
				<button id="'.$kol['id_info_laka'].'" data-toggle="tooltip" data-original-title="Hapus Data" class="btn btn-effect-ripple btn-danger btnDelete"><i class="fa fa-trash"></i></button>
			  </td>';
		echo '</tr>';
	}
	echo (empty($dataTabel)) ? '<tr><td colspan="7">Data tidak ditemukan</td></tr>' : '';
	?>
	</tbody>
	</table>
</div>
<?= ($jmlData > 0)? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Jasa");
	$("[data-toggle='tooltip']").tooltip();
</script>
