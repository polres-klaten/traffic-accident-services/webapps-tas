/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    loadTabel: function(){
        $("#info_laka").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>/tabel", $("#frmData").serialize(), function (data){
            $("#info_laka").html(data);
        });
    },
    showCharts: function(){
        $("#info_laka").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>/charts", $("#frmData").serialize(), function (laporanLaka){
            // $("#info_laka").html(laporanLaka);
            Highcharts.chart({
                chart: { type: 'bar', height: laporanLaka.height, renderTo: 'info_laka' },
                title: { text: laporanLaka.title },
                subtitle: { text: laporanLaka.subtitle },
                xAxis: {
                    categories: laporanLaka.category,
                    crosshair: true
                },
                yAxis: {
                    min: 0,
                    title: { text: 'Jumlah Laka Lantas' }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} '+laporanLaka.tooltip+'</b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                credits: { enabled: false },
                series: laporanLaka.series
            });
        });
    },
    showForm: function(){
        $.post("<?= $url_path; ?>/form", $("#frmData").serialize(), function (data){
            $("#data-form-input").html(data);
            $("#frmInputModal").modal("show");
            $(".btnFooter").show();
            $(".loading").html("");
        });
    },
    simpan: function(obj){
        $(".btnFooter").hide();
        $(".loading").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        $.post("<?= $url_path; ?>/simpan", $(obj).serialize(), function (data){
            // alert(data);
            app.showCharts();
            $("#frmInputModal").modal("hide");
            $.bootstrapGrowl("<h4><strong>"+data.title+"</strong></h4> <p>"+data.text+"</p>", {
                type: data.type,
                delay: 3000,
                allow_dismiss: true,
                offset: {from: 'top', amount: 20}
            });
        }, "json");
    },
    validate: function (e) {
        if (e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) return true;
        else if (e.charCode >= 48 && e.charCode <= 57) return true;
        else return false;
    }
};