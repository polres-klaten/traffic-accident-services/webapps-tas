<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    <form id="frmData" onsubmit="return false;" autocomplete="off">
                        <div class="block">
                            <div class="block-title">
                                <h2 id="table-title">Data Info Laka </h2><small id="j-table-title"></small>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                <p class="well well-sm"><strong>Catatan</strong><br>Untuk menambah/mengubah data info laka, pilih option tahun, bulan, kecamatan lalu klik tombol Update Data</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label for="tahun"><small>Tahun : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('tahun', $pil_tahun, $this->tahunAktif, 'class="select-chosen"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label for="bulan"><small>Bulan : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('bulan', $pil_bulan, $this->bulanAktif, 'class="select-chosen"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label for="kecamatan"><small>Kecamatan : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('kecamatan', $pil_kecamatan, '', 'class="select-chosen"'); ?>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <label for="kecamatan"><small>&nbsp;</small></label>
                                    <div class="form-group">
                                        <button class="btn btn-effect-ripple btn-info btnForm" data-toggle="tooltip" data-original-title="Tambah Data"><i class="fa fa-bar-chart-o"></i> Update Data</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block">
                            <div class="block-title">
                                <h2 id="table-title">Grafik Info Laka </h2><small id="j-table-title"></small>
                            </div>
                            <div class="row">
                                <div class="col-md-9">
                                    <p class="well well-sm"><strong>Catatan</strong><br>Pilih pilihan pada option Info Grafis, untuk parameter tahun dan bulan, mengikuti pilihan pada Data Info Laka diatas</p>
                                </div>
                                <div class="col-md-3">
                                    <label for="tahun"><small>Info Grafis : </small></label>
                                    <div class="form-group">
                                        <?= Bootstrap::inputSelect('info_grafis', $this->pilihanInfoGrafis, '', 'class="select-chosen"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="grafik-informasi" style="width: 100%; height: 400px; margin: 30px auto; overflow-y: auto;">
                                <div id="info_laka" style="width: 100%; height: auto; margin: 0 auto"></div>
                            </div>
                        </div>
                    </form>      
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        app.showCharts();
        $(document).on("change", "#info_grafis", function(){
            app.showCharts();
        });
        $(document).on("click", ".btnForm", function(){
            app.showForm();
        });
        $(document).on("submit", "#frmInput", function(){
            app.simpan(this);
        });
    </script>
</body>