<?php extract($dataForm); ?>
<ul class="nav nav-tabs" data-toggle="tabs">
    <li class="active"><a href="#block-tabs-kondisi"><i class="fa fa-pencil-square-o"></i> <strong>Kondisi</strong></a></li>
    <li><a href="#block-tabs-sim"><i class="fa fa-pencil-square-o"></i> <strong>SIM</strong></a></li>
    <li><a href="#block-tabs-usia"><i class="fa fa-pencil-square-o"></i> <strong>Usia</strong></a></li>
    <li><a href="#block-tabs-waktu"><i class="fa fa-pencil-square-o"></i> <strong>Waktu</strong></a></li>
</ul>
<?php
foreach ($form as $key => $value) {
    if(!is_array($value)) echo Bootstrap::inputKey($key, $value);;
}
?>
<div class="tab-content">
    <div class="tab-pane active" id="block-tabs-kondisi">
        <table class="table table-default table-bordereds">
            <tbody>
            <?php foreach ($form['info_kondisi_korban'] as $key => $value) {
                echo '<tr>';
                echo '<td>'.$pilihan['info_kondisi_korban'][$key].'</td>';
                echo '<td>'.Bootstrap::inputText('info_kondisi_korban[]', 'text', $value, 'class="form-control" onKeyPress="return app.validate(event);" required').'</td>';
                echo '<tr>';
            } ?>
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="block-tabs-sim">
        <table class="table table-default table-bordereds">
            <tbody>
            <?php foreach ($form['info_jenis_sim'] as $key => $value) {
                echo '<tr>';
                echo '<td>'.$pilihan['info_jenis_sim'][$key].'</td>';
                echo '<td>'.Bootstrap::inputText('info_jenis_sim[]', 'text', $value, 'class="form-control" onKeyPress="return app.validate(event);" required').'</td>';
                echo '<tr>';
            } ?>
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="block-tabs-usia">
        <table class="table table-default table-bordereds">
            <tbody>
            <?php foreach ($form['info_jenis_usia'] as $key => $value) {
                echo '<tr>';
                echo '<td>'.$pilihan['info_jenis_usia'][$key].'</td>';
                echo '<td>'.Bootstrap::inputText('info_jenis_usia[]', 'text', $value, 'class="form-control" onKeyPress="return app.validate(event);" required').'</td>';
                echo '<tr>';
            } ?>
            </tbody>
        </table>
    </div>
    <div class="tab-pane" id="block-tabs-waktu">
        <table class="table table-default table-bordereds">
            <tbody>
            <?php foreach ($form['info_rentang_waktu'] as $key => $value) {
                echo '<tr>';
                echo '<td>'.$pilihan['info_rentang_waktu'][$key].'</td>';
                echo '<td>'.Bootstrap::inputText('info_rentang_waktu[]', 'text', $value, 'class="form-control" onKeyPress="return app.validate(event);" required').'</td>';
                echo '<tr>';
            } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
    // Initialize Tabs
    $('[data-toggle="tabs"] a, .enable-tabs a').click(function(e){ e.preventDefault(); $(this).tab('show'); });
</script>