<?php extract($dataForm); ?>
<div class="form-group">
    <label for="nama_satuan_kerja">Nama Jasa Asuransi : </label>
    <?= Bootstrap::inputKey('id_satuan_kerja', $id_satuan_kerja); ?>
    <?= Bootstrap::inputText('nama_satuan_kerja', 'text', $nama_satuan_kerja, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="alamat_satuan_kerja">Alamat : </label>
    <?= Bootstrap::inputText('alamat_satuan_kerja', 'text', $alamat_satuan_kerja, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="telepon_satuan_kerja">No. Telepon : </label>
    <?= Bootstrap::inputText('telepon_satuan_kerja', 'text', $telepon_satuan_kerja, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="website_satuan_kerja">Website : </label>
    <?= Bootstrap::inputText('website_satuan_kerja', 'text', $website_satuan_kerja, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="email_satuan_kerja">Email : </label>
    <?= Bootstrap::inputText('email_satuan_kerja', 'text', $email_satuan_kerja, 'class="form-control" required'); ?>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
    $("#unit_kerja_id").trigger("change");
    $(".select-chosen").chosen({width: "100%"});
</script>