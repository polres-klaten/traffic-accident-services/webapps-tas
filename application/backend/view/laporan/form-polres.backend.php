<?php extract($dataForm['laporan_kepolisian']); ?>
<?php extract($dataForm['data_identitas']); ?>
<div id="laporan_kepolisian" class="formwizards-content">
    <div class="form-group">
        <div class="col-md-6">
            <div class="col-md-6">
                <label for="nomer_lp">Nomer LP : </label>
                <?= Bootstrap::inputKey('id_laporan_kepolisian', $id_laporan_kepolisian); ?>
                <?= Bootstrap::inputText('nomer_lp', 'text', $nomer_lp, 'class="form-control"'); ?>
            </div>
            <div class="col-md-6">
                <label for="tanggal_lp">Tanggal LP : </label>
                <?= Bootstrap::inputText('tanggal_lp', 'text', $tanggal_lp, 'class="form-control input-datepicker" data-date-format="yyyy-mm-dd"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="tanggal_lp">Tanggal Kejadian : </label>
                <?= Bootstrap::inputText('tanggal_waktu_kejadian[]', 'text', $tanggal_waktu_kejadian['tanggal'], 'class="form-control input-datepicker" data-date-format="yyyy-mm-dd" readonly'); ?>
            </div>
            <div class="col-md-6">
                <label for="tanggal_lp">Waktu Kejadian : </label>
                <div class="input-group bootstrap-timepicker">
                    <?= Bootstrap::inputText('tanggal_waktu_kejadian[]', 'text', $tanggal_waktu_kejadian['waktu'], 'class="form-control input-timepicker24" readonly'); ?>
                    <span class="input-group-btn">
                        <a href="javascript:void(0)" class="btn btn-effect-ripple btn-primary"><i class="fa fa-clock-o"></i></a>
                    </span>
                </div>
            </div>
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>Tempat kejadian (Kabupaten, Kecamatan, Desa, Jalan)</label>
            </div>
            <div class="col-md-6">
                <label for="tempat_kejadian">Jalan : </label>
                <?= Bootstrap::inputText('tempat_kejadian[]', 'text', $tempat_kejadian['jalan'], 'class="form-control" form-validate="Jalan tidak boleh kosong"'); ?>
            </div>
            <div class="col-md-6">
                <label for="tempat_kejadian">Kabupaten : </label>
                <?= Bootstrap::inputSelect('tempat_kejadian[]', $pil_kabupaten_tempat_kejadian, $tempat_kejadian['kabupaten'], 'class="select-chosen filter-option filter-default" data-link="'.$link_pilihan_kecamatan.'" data-target="tempat_kejadian_kecamatan"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="tempat_kejadian">Kecamatan : </label>
                <div class="tempat_kejadian_kecamatan">
                    <?= Bootstrap::inputSelect('tempat_kejadian[]', $pil_kecamatan_tempat_kejadian, $tempat_kejadian['kecamatan'], 'class="select-chosen filter-option" data-link="'.$link_pilihan_kecamatan.'" data-target="tempat_kejadian_desa"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <label for="tempat_kejadian">Desa : </label>
                <div class="tempat_kejadian_desa">
                    <?= Bootstrap::inputSelect('tempat_kejadian[]', $pil_desa_tempat_kejadian, $tempat_kejadian['kel_desa'], 'class="select-chosen"'); ?>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="jenis_kecelakaan">Jenis Kecelakaan : </label>
                <?= Bootstrap::inputSelect('jenis_kecelakaan', $this->pilihanJenisKecelakaan, $jenis_kecelakaan, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-6">
                <label for="ketegori_ranmor">Kategori Ranmor : </label>
                <?= Bootstrap::inputSelect('ketegori_ranmor', $this->pilihanKategoriRanmor, $ketegori_ranmor, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="jenis_ranmor">Jenis Ranmor : </label>
                <?= Bootstrap::inputSelect('jenis_ranmor', $this->pilihanJenisRanmor, $jenis_ranmor, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-6">
                <label for="status_pelapor">Status Pelapor : </label>
                <?= Bootstrap::inputSelect('status_pelapor', $this->pilihanStatusPelapor, $status_pelapor, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-12">
                <label for="nohp_pemohon">No. Handphone Pemohon : </label>
                <?= Bootstrap::inputText('nohp_pemohon', 'text', $nohp_pemohon, 'class="form-control" form-validate="No. Handphone pemohon tidak boleh kosong"'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12">
                <label for="kondisi_korban">Kondisi Korban : </label>
                <?= Bootstrap::inputSelect('kondisi_korban', $this->pilihanKondisiKorban, $kondisi_korban, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>*) Jika kondisi korban meninggal dunia, maka input isian Ahli Waris</label>
            </div>
            <div class="col-md-12">
                <label for="norek_ahli_waris">No. Rekening a/n Ahli Waris : </label>
                <?= Bootstrap::inputText('norek_ahli_waris', 'text', $norek_ahli_waris, 'class="form-control"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-12">
                <label for="berkas_ahli_waris">Surat Lampiran a/n Ahli Waris : <?= $berkas_ahli_waris; ?></label>
                <?= Bootstrap::inputText('berkas_ahli_waris', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"'); ?>
                <small><?= $keterangan_upload_lampiran; ?></small><br>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-12">
                <label for="kronologi_kejadian">Kronologi Kejadian</label>
                <?= Bootstrap::inputTextArea('kronologi_kejadian', $kronologi_kejadian, 'class="form-control" rows="10" form-validate="Kronologi kejadian tidak boleh kosong" '); ?>
                </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-12">
                <label for="berkas_laporan_kepolisian">Upload File LP : <?= $berkas_laporan_kepolisian; ?></label>
                <?= Bootstrap::inputText('berkas_laporan_kepolisian', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"'); ?>
                <small><?= $keterangan_upload_lampiran; ?></small>
            </div>
        </div>
    </div>
</div>

<div id="data_identitas" class="formwizards-content">
    <div class="form-group">
        <div class="col-md-6">
            <div class="col-md-12">
                <label for="nama_lengkap">Nama Lengkap : </label>
                <?= Bootstrap::inputKey('id_data_identitas', $id_data_identitas); ?>
                <?= Bootstrap::inputText('nama_lengkap', 'text', $nama_lengkap, 'class="form-control" form-validate="Nama lengkap tidak boleh kosong"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="jenis_kelamin">Jenis Kelamin : </label>
                <?= Bootstrap::inputSelect('jenis_kelamin', $this->pilihanJenisKelamin, $jenis_kelamin, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-6">
                <label for="jenis_golda">Golongan Darah : </label>
                <?= Bootstrap::inputSelect('jenis_golda', $this->pilihanJenisGolda, $jenis_golda, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="tempat_tanggal_lahir">Tempat Lahir: </label>
                <?= Bootstrap::inputText('tempat_tanggal_lahir[]', 'text', $tempat_tanggal_lahir['tempat'], 'class="form-control"'); ?>
            </div>
            <div class="col-md-6">
                <label for="tempat_tanggal_lahir">Tanggal Lahir: </label>
                <?= Bootstrap::inputText('tempat_tanggal_lahir[]', 'text', $tempat_tanggal_lahir['tanggal'], 'class="form-control input-datepicker" data-date-format="yyyy-mm-dd"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="jenis_agama">Jenis Agama : </label>
                <?= Bootstrap::inputSelect('jenis_agama', $this->pilihanJenisAgama, $jenis_agama, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-6">
                <label for="status_perkawinan">Status Perkawinan : </label>
                <?= Bootstrap::inputSelect('status_perkawinan', $this->pilihanJenisPerkawinan, $status_perkawinan, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="jenis_pekerjaan">Profesi Pekerjaan : </label>
                <?= Bootstrap::inputSelect('jenis_pekerjaan', $this->pilihanJenisProfesi, $jenis_pekerjaan, 'class="select-chosen"'); ?>
            </div>
            <div class="col-md-6">
                <label for="status_kewarganegaraan">Status Kewarganegaraan : </label>
                <?= Bootstrap::inputSelect('status_kewarganegaraan', $this->pilihanStatusKewarganegaraan, $status_kewarganegaraan, 'class="select-chosen"'); ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>*) Alamat, Kabupaten, Kecamatan, Desa, RT/RW</label>
            </div>
            <div class="col-md-12">
                <label for="alamat_data_identitas">Alamat : </label>
                <?= Bootstrap::inputText('alamat_data_identitas[]', 'text', $alamat_data_identitas['alamat'], 'class="form-control" form-validate="Alamat tidak boleh kosong"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="alamat_data_identitas">Kabupaten : </label>
                <?= Bootstrap::inputSelect('alamat_data_identitas[]', $pil_kabupaten_alamat_data_identitas, $alamat_data_identitas['kabupaten'], 'class="select-chosen filter-option filter-default" data-link="'.$link_pilihan_kecamatan.'" data-target="alamat_data_identitas_kecamatan"'); ?>
            </div>
            <div class="col-md-6 ">
                <label for="alamat_data_identitas">Kecamatan : </label>
                <div class="alamat_data_identitas_kecamatan">
                    <?= Bootstrap::inputSelect('alamat_data_identitas[]', $pil_kecamatan_alamat_data_identitas, $alamat_data_identitas['kecamatan'], 'class="select-chosen filter-option" data-link="'.$link_pilihan_desa.'" data-target="alamat_data_identitas_desa"'); ?>
                </div>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-6">
                <label for="alamat_data_identitas">Desa : </label>
                <div class="alamat_data_identitas_desa">
                    <?= Bootstrap::inputSelect('alamat_data_identitas[]', $pil_desa_alamat_data_identitas, $alamat_data_identitas['kel_desa'], 'class="select-chosen"'); ?>
                </div>
            </div>
            <div class="col-md-6">
                <label for="alamat_data_identitas">RT/RW : </label>
                <?= Bootstrap::inputText('alamat_data_identitas[]', 'text', $alamat_data_identitas['rt_rw'], 'class="form-control"'); ?>
            </div>
            <div class="col-md-12" style="margin-bottom: 15px;"></div>
            <div class="col-md-12">
                <label for="berkas_data_identitas">Upload File KTP : <?= $berkas_data_identitas; ?></label>
                <?= Bootstrap::inputText('berkas_data_identitas', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"'); ?>
                <small><?= $keterangan_upload_lampiran; ?></small>
            </div>
        </div>
    </div>
</div>

<div id="surat_pengantar" class="formwizards-content">
    <div class="form-group">
        <table class="table table-default table-hover surat_pengantar">
            <thead>
                <tr>
                    <th>Keterangan</th>
                    <th>File Lampiran</th>
                    <th colspan="2" style="text-align: right;"><button type="button" class="btn btn-effect-ripple btn-primary btnTambah">Tambah Lampiran</button></th>
                </tr>
            </thead>
            <tbody>
                <?php 
                // Multiple Rows
                foreach ($dataForm['surat_pengantar'] as $key => $value) { 
                    if(!empty($value['laporan_kepolisian_id'])){
                        echo '<tr>';
                        echo '<td>'.Bootstrap::inputKey('id_surat_pengantar[]', $value['id_surat_pengantar']).Bootstrap::inputTextArea('keterangan_surat_pengantar[]', $value['keterangan_surat_pengantar'], 'class="form-control" ').'</td>';
                        echo '<td>'.Bootstrap::inputText('berkas_surat_pengantar[]', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"').'<small>'.$keterangan_upload_lampiran.'</small></td>';
                        echo '<td>'.$value['berkas_surat_pengantar'].'<br>Last Update: <small>'.FUNC::tanggal($value['datetime'], 'long_date_time').'</small></td>';
                        echo '<td><button type="button" id="'.$value['id_surat_pengantar'].'" class="btn btn-effect-ripple btn-default btnHapus"><i class="fa fa-trash-o"></i></button></td>';
                        echo '</tr>';
                    }else{
                        echo '<tr>';
                        echo '<td>'.Bootstrap::inputKey('id_surat_pengantar[]', $value['id_surat_pengantar']).Bootstrap::inputTextArea('keterangan_surat_pengantar[]', $value['keterangan_surat_pengantar'], 'class="form-control" ').'</td>';
                        echo '<td colspan="3">'.Bootstrap::inputText('berkas_surat_pengantar[]', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"').'<small>'.$keterangan_upload_lampiran.'</small></td>';
                        echo '</tr>';
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>