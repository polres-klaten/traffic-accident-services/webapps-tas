<style>
.table thead > tr > th{
    font-size: 10pt!important;
}
.table tbody > tr > td{
    font-size: 10pt!important;
}
input[type="file"].form-control {
    padding: 0px;
}
</style>
<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    <div class="block wizard-content">
                        <!-- Clickable Wizard Title -->
                        <div class="block-title">
                            <div class="block-options pull-right">
                            <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-default " data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Kembali"><i class="fa fa-reply"></i></a>
                            </div>
                            <h2><?= $page_title; ?></h2>
                        </div>
                        <!-- END Clickable Wizard Title -->

                        <form id="formInput-wizard" onsubmit="return false;" class="form-horizontal form-bordered formwizards" autocomplete="off">
                            <div class="form-group">
                                <div class="col-xs-12">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="javascript:void(0)" data-gotostep="laporan_kepolisian"><i class="fa fa-pencil-square-o"></i> <strong>Laporan Polisi</strong></a></li>
                                        <li><a href="javascript:void(0)" data-gotostep="data_identitas"><i class="fa fa-pencil-square-o"></i> <strong>Data Identitas Korban</strong></a></li>
                                        <li><a href="javascript:void(0)" data-gotostep="surat_pengantar"><i class="fa fa-pencil-square-o"></i> <strong>Surat Pengantar</strong></a></li>
                                        <li><a href="javascript:void(0)" data-gotostep="rekam_medis"><i class="fa fa-pencil-square-o"></i> <strong>Surat Keterangan Laka</strong></a></li>
                                        <li><a href="javascript:void(0)" data-gotostep="jasa_asuransi"><i class="fa fa-pencil-square-o"></i> <strong>Jasa Asuransi</strong></a></li>
                                    </ul>
                                </div>
                            </div>

                            <?php $this->subView('laporan/form-polres', $data); ?>
                            <?php $this->subView('laporan/form-kesehatan', $data); ?>
                            <?php $this->subView('laporan/form-asuransi', $data); ?>

                            <div class="form-group formwizards-actions">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-effect-ripple btn-danger" id="btnPrev"></button>
                                    <button type="button" class="btn btn-effect-ripple btn-primary" id="btnNext"></button>
                                </div>
                            </div>
                        </form>
                        <?php
                            // echo '<pre style="background: none;">';
                            // print_r($dataForm);
                            // echo '</pre>';
                        ?>
                    </div>
                    

                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        app.showFormWizard($("#formInput-wizard"));
        $(document).on("change", ".filter-option", function(){
            var link = this.getAttribute("data-link");
            var target = this.getAttribute("data-target");
            $.post(link, {id: this.id, value: this.value}, function(data){
                $("."+target).html(data);
                $("."+target+" select").trigger("change");
                $(".select-chosen").chosen({width: "100%"});
            });
            // console.log(target);
        });
        $(document).on("click", ".btnDelete", function(){
            app.hapus(this.id);
        });
        var tbody = $("table.surat_pengantar tbody");
        var data = $("table.surat_pengantar tbody tr:last-child").html();
        $(document).on("click", ".btnTambah", function(){
            var rows = $("<tr>").html(data);
            tbody.append(rows);
        });
        $(document).on("click", ".btnHapus", function(){
            app.hapus_surat_pengantar(this);
        });
        // $(".filter-default").trigger("change");
    </script>
</body>