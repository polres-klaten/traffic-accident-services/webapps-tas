<?php extract($dataForm['rekam_medis']); ?>
<?php $width = '200px'; ?>
<?php if(!($status_rekam_medis == 'baru')){ ?>
<ul class="breadcrumb">
    <li><?= $pil_jasa_kesehatan[$satker_asal_rekam_medis]; ?> </li><li><a href="">Surat Keterangan Laka</a></li>
    <?php if($session_login['unit_kerja_id'] == 'UK.003' && $active_modul == 'laporan'){ ?>
    <div class="block-options pull-right">
        <a href="javascript:void(0)" id="<?= $id_rekam_medis; ?>" class="btn btn-effect-ripple btn-info btnFormRekamMedis" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i> Edit</a>
        <?php
        /**
         * Rujukan RM aktif, jika RM memiliki laporan kepolisian dan kwintansi belum terbit
         */
        if(!empty($dataForm['laporan_kepolisian']['rekam_medis_id']) && empty($dataForm['kwintansi'][0]['laporan_kepolisian_id'])) {
        ?>
        <a href="javascript:void(0)" id="<?= $dataForm['laporan_kepolisian']['id_laporan_kepolisian']; ?>" class="btn btn-effect-ripple btn-info btnFormRujukan" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i> Rujuk ke RS Lainnya</a>
    </div>
        <?php } ?>
    <?php } ?>
</ul>
<table class="table table-default table-bordered">
    <tr><td width="<?= $width; ?>">Nomer RM</td><td>: <?= $no_rekam_medis; ?></td></tr>
    <tr><td width="<?= $width; ?>">Nama Pasien</td><td>: <?= $nama_pasien; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tanggal dan Umur Pasien</td><td>: <?= $tanggal_umur_pasien['tanggal']; ?></td></tr>
    <tr><td width="<?= $width; ?>">Diagnosa</td><td>: <?= $diagnosa_pasien; ?></td></tr>
    <tr><td width="<?= $width; ?>">Ruang/Bangsal</td><td>: <?= $ruang_bangsal; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tanggal Masuk</td><td>: <?= FUNC::tanggal($tanggal_masuk, 'long_date'); ?></td></tr>
    <tr><td width="<?= $width; ?>">Dokter Pemeriksa</td><td>: <?= $dokter_pemeriksa; ?></td></tr>
    <tr><td width="<?= $width; ?>">Lampiran RM</td><td>: <?= $berkas_rekam_medis; ?></td></tr>
</table>
<ul class="breadcrumb">
    <li><?= $pil_jasa_kesehatan[$satker_asal_rekam_medis]; ?> </li><li><a href="">Kwintansi Pembayaran</a></li>
    <?php if($session_login['unit_kerja_id'] == 'UK.003' && $active_modul == 'laporan'){ ?>
    <div class="block-options pull-right">
        <a href="javascript:app.showInbox('<?= $dataForm['laporan_kepolisian']['id_laporan_kepolisian']; ?>','kwintansi');" class="btn btn-effect-ripple btn-info" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i> Upload/Edit</a>
    </div>
    <?php } ?>
</ul>
<table class="table table-default table-bordered">
    <thead>
        <tr><th>Nominal Pembayaran</th><th>File Lampiran</th></tr>
    </thead>
    <tbody>
        <?php 
        foreach ($dataForm['kwintansi'] as $key => $value) { 
            if(!empty($value['laporan_kepolisian_id'])){
                echo '<tr>';
                echo '<td>Rp. '.FUNC::rupiah($value['nominal_kwintansi']).'</td>';
                echo '<td>'.$value['berkas_kwintansi'].'<br>Last Update: <small>'.FUNC::tanggal($value['datetime'], 'long_date_time').'</small></td>';
                echo '</tr>';
            }
        }
        ?>
    </tbody>
</table>
<?php
}else{ ?>
<p class="well well-sm"><strong>Informasi</strong><br>Permintaan Surat Keterangan Laka telah dikirim ke <?= $pil_jasa_kesehatan[$satker_asal_rekam_medis]; ?></p>
<?php } ?>