<?php extract($dataForm['laporan_kepolisian']); ?>
<div id="jasa_asuransi" class="formwizards-content">
    <div class="form-group">
        <div class="col-md-6">
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>*) Pilih Jasa Asuransi yang akan dikirim laporan kepolisian</label>
            </div>
            <div class="col-md-12">
                <label for="satker_tujuan_laporan_kepolisian">Jasa Asuransi : </label><br>
                <?php 
                // Ubah kode satker dari database menjadi nilai satker
                if(!empty($satker_tujuan_laporan_kepolisian)){
                    $arrData = explode(',', $satker_tujuan_laporan_kepolisian);
                    foreach ($arrData as $key => $value) {$arrData[$key] = $pil_jasa_asuransi[$value];}
                    $satker_tujuan_laporan_kepolisian = implode(',', $arrData);
                }
                ?>
                <?= Bootstrap::inputCheckBox('satker_tujuan_laporan_kepolisian[]', $pil_jasa_asuransi, $satker_tujuan_laporan_kepolisian); ?>
            </div>
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>*) Jika pilihan jaminan asuransi BPJS Ketenagakerjaan, maka perlu upload berkas SKKL</label>
            </div>
            <div class="col-md-12">
                <label for="berkas_laporan_skkl">Upload File SKKL : <?= $berkas_laporan_skkl; ?></label>
                <?= Bootstrap::inputText('berkas_laporan_skkl', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"'); ?>
                <small><?= $keterangan_upload_lampiran; ?></small>
            </div>
        </div>
    </div>
</div>