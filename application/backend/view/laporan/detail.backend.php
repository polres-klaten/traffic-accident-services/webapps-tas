<style>
.table thead > tr > th{
    font-size: 10pt!important;
}
.table tbody > tr > td{
    font-size: 10pt!important;
}
input[type="file"].form-control {
    padding: 0px;
}
</style>
<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
        
                    <div class="block full">
                        <!-- Block Tabs Title -->
                        <div class="block-title">
                            <div class="block-options pull-left">
                                <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-default " data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Kembali"><i class="fa fa-reply"></i></a>
                            </div>
                            <?php extract($dataForm['laporan_kepolisian']); ?>
                            <?php if($session_login['unit_kerja_id'] == 'UK.001'){ ?>
                            <div class="block-options pull-right">
                                <a href="javascript:void(0)" id="<?= $id_laporan_kepolisian; ?>" class="btn btn-effect-ripple btn-default btnFormLaporan" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" id="<?= $id_laporan_kepolisian; ?>" class="btn btn-effect-ripple btn-default btnDelete" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Hapus Data"><i class="fa fa-trash"></i></a>
                            </div>
                            <?php } ?>
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active"><a href="#block-tabs-polres"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Polres</strong></a></li>
                                <li><a href="#block-tabs-kesehatan"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Jasa Layanan Kesehatan</strong></a></li>
                                <li><a href="#block-tabs-asuransi"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Jasa Asuransi</strong></a></li>
                            </ul>
                        </div>
                        <!-- END Block Tabs Title -->

                        <!-- Tabs Content -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="block-tabs-polres">
                                <?php $this->subView('laporan/berkas-polres', $data); ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-kesehatan">
                                <?php $this->subView('laporan/berkas-kesehatan', $data); ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-asuransi">
                                <?php $this->subView('laporan/berkas-asuransi', $data); ?>
                            </div>
                        </div>
                        <!-- END Tabs Content -->
                        <?php
                            // echo '<pre style="background: none;">';
                            // print_r($dataForm);
                            // echo '</pre>';
                        ?>
                    </div>

                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $(document).on("click", ".btnFormLaporan", function(){
            app.showFormLaporan(this.id);
        });
        $(document).on("click", ".btnFormRekamMedis", function(){
            app.showFormRekamMedis(this.id);
        });
        $(document).on("click", ".btnFormRujukan", function(){
            app.showFormRujukan(this.id);
        });
        $(document).on("submit", "#frmInput", function(){
            app.simpanRujukan(this);
        });
        $(document).on("click", ".btnDelete", function(){
            app.hapus(this.id);
        });
    </script>
</body>