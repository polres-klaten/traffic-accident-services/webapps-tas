<?php 
// Ubah kode satker dari database menjadi nilai satker
extract($dataForm['laporan_kepolisian']);
if(!empty($satker_tujuan_laporan_kepolisian)){
?>
    <ul class="breadcrumb"><li>Berkas Jasa Asuransi </li><li><a href="">Surat Jaminan</a></li></ul>
    <table class="table table-default table-bordered">
        <thead><tr><th>Jasa Asuransi</th><th>Keterangan</th><th colspan="2">File Lampiran</th></tr></thead>
        <tbody>
        <?php 
            foreach ($dataForm['surat_jaminan'] as $key => $value) { 
                $action = ($value['id_satker'] == $session_login['id_satuan_kerja'] && $active_modul == 'laporan') ? $value['action'] : '';
                echo '<tr>';
                echo '<td>'.$value['nama_asuransi'].'</td>';
                echo '<td>'.$value['keterangan'].'</td>';
                echo '<td>'.$value['file_lampiran'].'</td>';
                echo '<td>'.$action.'</td>';
                echo '</tr>';
            }
        ?>
        </tbody>
    </table>
    <ul class="breadcrumb"><li>Berkas Jasa Asuransi </li><li><a href="">Bukti Pembayaran</a></li></ul>
    <table class="table table-default table-bordered">
        <thead><tr><th>Jasa Asuransi</th><th>Nominal Pembayaran</th><th colspan="2">File Lampiran</th></tr></thead>
        <tbody>
        <?php 
            foreach ($dataForm['bukti_pembayaran'] as $key => $value) { 
                $action = ($value['id_satker'] == $session_login['id_satuan_kerja'] && $active_modul == 'laporan') ? $value['action'] : '';
                echo '<tr>';
                echo '<td>'.$value['nama_asuransi'].'</td>';
                echo '<td>'.$value['nominal'].'</td>';
                echo '<td>'.$value['file_lampiran'].'</td>';
                echo '<td>'.$action.'</td>';
                echo '</tr>';
            }
        ?>
        </tbody>
    </table>
<?php
}else{ ?>
<p class="well well-sm"><strong>Informasi</strong><br>Daftar Asuransi belum dipilih...</p>
<?php } ?>