<?= $query; ?>
<style>
	table{
		font-size: 10pt;
	}
	table > thead > tr > th{
		vertical-align: middle!important;
		text-align: center;
	}
</style>
<div class="table-responsive">
	<table class="table table-default table-hover table-bordered">
	<thead>
		<tr>
			<th width="50px" rowspan="2"><small>#</small></th>
			<th width="300px" rowspan="2"><small><strong>Laporan A/n</strong></small></th>
			<th><small><strong>Berkas Polres</strong></small></th>
			<th colspan="2"><small><strong>Berkas Jasa Kesehatan</strong></small></th>
			<th colspan="2"><small><strong>Berkas Jasa Asuransi</strong></small></th>
			<th rowspan="2"><small><strong>Status</strong></small></th>
			<!-- <th rowspan="2"><small><strong>Kirim Pesan</strong></small></th> -->
		</tr>
		<tr>
			<th><small><strong>LP/SP/DD/SKKL</strong><br><?= Bootstrap::inputSelect('pil_status', $pil_status, '', 'class="select-chosen filter-option" data-target="status_polres" style="cursor: pointer;"'); ?></small></th>
			<th><small><strong>Suket Laka</strong><br><?= Bootstrap::inputSelect('pil_status', $pil_status, '', 'class="select-chosen filter-option" data-target="status_rekam_medis" style="cursor: pointer;"'); ?></small></th>
			<th><small><strong>Kwintansi</strong><br><?= Bootstrap::inputSelect('pil_status', $pil_status, '', 'class="select-chosen filter-option" data-target="status_kwintansi" style="cursor: pointer;"'); ?></small></th>
			<th><small><strong>Surat Jaminan</strong><br><?= Bootstrap::inputSelect('pil_status', $pil_status, '', 'class="select-chosen filter-option" data-target="status_surat_jaminan" style="cursor: pointer;"'); ?></small></th>
			<th><small><strong>Bukti Pembayaran</strong><br><?= Bootstrap::inputSelect('pil_status', $pil_status, '', 'class="select-chosen filter-option" data-target="status_pembayaran" style="cursor: pointer;"'); ?></small></th>
		</tr>
	</thead>
	<tbody>
	<?php
	
	$customLabelUpload = function($data, $content){
		$content = implode("<br>", $content);
		$flag = ($data ) ? 'success' : 'danger';
		return '<button class="btn btn-effect-ripple btn-block btn-'.$flag.'" data-toggle="popover" data-content="'.$content.'" data-placement="top" title="Detail">'.$this->pilihanStatusUploadBerkas[$data].'</button>';
	};

	$customLabelProgress= function($data){
		$content = '';
		$flag = ($data ) ? 'success' : 'warning';
		return '<button class="btn btn-effect-ripple btn-block btn-'.$flag.'" data-toggle="popovers" data-content="'.$content.'" data-placement="top" title="Detail">'.$this->pilihanProgressUploadBerkas[$data].'</button>';
	};

	foreach($dataTabel as $kol){
		$status_berkas_polres = array(
			'Laporan Polisi : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_laporan_kepolisian']],
			'Surat Pengantar : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_surat_pengantar']],
			'Data Identitas : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_data_identitas']],
			'SKKL : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_laporan_skkl']],
		);
		$status_berkas_rekam_medis = array(
			'Surat Keterangan Laka : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_rekam_medis']],
		);
		$status_berkas_kwintansi = array(
			'Kwintansi : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_kwintansi']],
		);
		$status_berkas_surat_jaminan = array(
			'Surat Jaminan : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_surat_jaminan']],
		);
		$status_berkas_pembayaran = array(
			'Bukti Pembayaran : '.$this->pilihanStatusUploadBerkas[$kol['status_berkas_pembayaran']],
		);

		echo '<tr>';
		echo '<td>'.$no++.'</td>';
		echo '<td style="cursor: pointer" id="'.$kol['id_laporan_kepolisian'].'" class="btnDetail">'.$kol['nama_lengkap'].'</td>';
		echo '<td>'.$customLabelUpload($kol['status_berkas_polres'], $status_berkas_polres).'</td>';
		echo '<td>'.$customLabelUpload($kol['status_berkas_rekam_medis'], $status_berkas_rekam_medis).'</td>';
		echo '<td>'.$customLabelUpload($kol['status_berkas_kwintansi'], $status_berkas_kwintansi).'</td>';
		echo '<td>'.$customLabelUpload($kol['status_berkas_surat_jaminan'], $status_berkas_surat_jaminan).'</td>';
		echo '<td>'.$customLabelUpload($kol['status_berkas_pembayaran'], $status_berkas_pembayaran).'</td>';
		echo '<td>'.$customLabelProgress($kol['status_progress']).'</td>';
		// echo '<td><button class="btn btn-effect-ripple btn-block btn-default btnSMS" data-pin="'.$kol['pin_akses'].'" data-nohp="'.$kol['nohp_pemohon'].'"><i class="gi gi-message_new"></i></button></td>';
		echo '</tr>';
	}
	// echo (empty($dataTabel)) ? '<tr><td colspan="9">Data tidak ditemukan</td></tr>' : '';
	echo (empty($dataTabel)) ? '<tr><td colspan="8">Data tidak ditemukan</td></tr>' : '';
	?>
	</tbody>
	</table>
</div>
<?= ($jmlData > 0)? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<?php
	// echo '<pre style="background: none;">';
	// print_r($data);
	// echo '</pre>';
?>
<script>
	$("[data-target='status_polres']").val("<?= $status_polres; ?>");
	$("[data-target='status_rekam_medis']").val("<?= $status_rekam_medis; ?>");
	$("[data-target='status_kwintansi']").val("<?= $status_kwintansi; ?>");
	$("[data-target='status_surat_jaminan']").val("<?= $status_surat_jaminan; ?>");
	$("[data-target='status_pembayaran']").val("<?= $status_pembayaran; ?>");
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Laporan");
	$("[data-toggle='tooltip']").tooltip();
	$("[data-toggle='popover'], .enable-popover").popover({container: "body", animation: true, html: true});
</script>
