<?php extract($dataForm['laporan_kepolisian']); ?>
<?php extract($dataForm['data_identitas']); ?>
<?php $width = '200px'; ?>
<ul class="breadcrumb"><li>Berkas Polres</li><li><a href="">Laporan Polisi</a></li></ul>
<table class="table table-default table-bordered">
    <tr><td width="<?= $width; ?>">Nomer LP</td><td>: <?= $nomer_lp; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tanggal LP</td><td>: <?= $nomer_lp; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tanggal dan Waktu Kejadian</td><td>: <?= FUNC::tanggal($tanggal_waktu_kejadian['tanggal'], 'long_date'); ?>, pukul : <?= $tanggal_waktu_kejadian['waktu']; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tempat Kejadian</td><td>: <?= $tempat_kejadian['jalan']; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kel/Desa</td><td>: <?= $pil_desa_tempat_kejadian[$tempat_kejadian['kel_desa']]; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kecamatan</td><td>: <?= $pil_kecamatan_tempat_kejadian[$tempat_kejadian['kecamatan']]; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kabupaten</td><td>: <?= $pil_kabupaten_tempat_kejadian[$tempat_kejadian['kabupaten']]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Jenis Kecelakaan</td><td>: <?= $this->pilihanJenisKecelakaan[$jenis_kecelakaan]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Kategori Ranmor</td><td>: <?= $this->pilihanKategoriRanmor[$ketegori_ranmor]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Jenis Ranmor</td><td>: <?= $this->pilihanJenisRanmor[$jenis_ranmor]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Status Pelapor</td><td>: <?= $this->pilihanStatusPelapor[$status_pelapor]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Kondisi Korban</td><td>: <?= $this->pilihanKondisiKorban[$kondisi_korban]; ?></td></tr>
    <tr><td width="<?= $width; ?>">No. Rekeing a/n Ahli Waris</td><td>: <?= $norek_ahli_waris; ?></td></tr>
    <tr><td width="<?= $width; ?>">Lampiran Ahli Waris</td><td>: <?= $berkas_ahli_waris; ?></td></tr>
    <tr><td width="<?= $width; ?>">Kronologi Kejadian</td><td>: <?= $kronologi_kejadian; ?></td></tr>
    <tr><td width="<?= $width; ?>">Lampiran LP</td><td>: <?= $berkas_laporan_kepolisian; ?></td></tr>
    <tr><td width="<?= $width; ?>">Lampiran SKKL</td><td>: <?= $berkas_laporan_skkl; ?></td></tr>
</table>
<hr>
<ul class="breadcrumb"><li>Berkas Polres</li><li><a href="">Data Identitas</a></li></ul>
<table class="table table-default table-bordered">
    <tr><td width="<?= $width; ?>">Nama Lengkap</td><td>: <?= $nama_lengkap; ?></td></tr>
    <tr><td width="<?= $width; ?>">Jenis Kelamin</td><td>: <?= $this->pilihanJenisKelamin[$jenis_kelamin]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Golongan Darah</td><td>: <?= $this->pilihanJenisGolda[$jenis_golda]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Tempat Tanggal Lahir</td><td>: <?= FUNC::tanggal($tempat_tanggal_lahir['tanggal'], 'long_date'); ?></td></tr>
    <tr><td width="<?= $width; ?>">Jenis Agama</td><td>: <?= $this->pilihanJenisAgama[$jenis_agama]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Status Perkawinan</td><td>: <?= $this->pilihanJenisPerkawinan[$status_perkawinan]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Profesi Pekerjaan</td><td>: <?= $this->pilihanJenisProfesi[$jenis_pekerjaan]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Status Kewarganegaraan</td><td>: <?= $this->pilihanStatusKewarganegaraan[$status_kewarganegaraan]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Alamat</td><td>: <?= $alamat_data_identitas['alamat']; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">RT/RW</td><td>: <?= $alamat_data_identitas['rt_rw']; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kel/Desa</td><td>: <?= $pil_desa_alamat_data_identitas[$alamat_data_identitas['kel_desa']]; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kecamatan</td><td>: <?= $pil_kecamatan_alamat_data_identitas[$alamat_data_identitas['kecamatan']]; ?></td></tr>
    <tr><td width="<?= $width; ?>" align="right">Kabupaten</td><td>: <?= $pil_kabupaten_alamat_data_identitas[$alamat_data_identitas['kabupaten']]; ?></td></tr>
    <tr><td width="<?= $width; ?>">Lampiran KTP</td><td>: <?= $berkas_data_identitas; ?></td></tr>
</table>
<hr>
<ul class="breadcrumb"><li>Berkas Polres</li><li><a href="">Surat Pengantar</a></li></ul>
<table class="table table-default table-bordered">
    <thead>
        <tr><th>Keterangan</th><th>File Lampiran</th></tr>
    </thead>
    <tbody>
        <?php 
        foreach ($dataForm['surat_pengantar'] as $key => $value) { 
            if(!empty($value['laporan_kepolisian_id'])){
                echo '<tr>';
                echo '<td>'.$value['keterangan_surat_pengantar'].'</td>';
                echo '<td>'.$value['berkas_surat_pengantar'].'<br>Last Update: <small>'.FUNC::tanggal($value['datetime'], 'long_date_time').'</small></td>';
                echo '</tr>';
            }
        }
        ?>
    </tbody>
</table>