/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    loadTabel: function () {
        $("#table-content").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>laporan/tabel", $("#frmData").serialize(), function (data) {
            $("#table-content").html(data);
        });
    },
    tabelPagging: function (number) {
        $("#page").val(number);
        this.loadTabel();
        $(document).scrollTop(0);
    },
    showFormLaporan: function (id) {
        $.ajax({
            url: "<?= $url_path; ?>laporan/form/action",
            data: {
                id: id
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    showFormRekamMedis: function (id) {
        $.ajax({
            url: "<?= $url_path; ?>rekam_medis/form/action",
            data: {
                id: id
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    showFormRujukan: function(id){
        $.post("<?= $url_path; ?>rekam_medis/rujukan/form", {id: id}, function (data){
            $("#data-form-input").html(data);
            $("#frmInputModal").modal("show");
            $(".btnFooter").show();
            $(".loading").html("");
        });
    },
    showInbox: function (id, action) {
        $.ajax({
            url: "<?= $url_path; ?>dashboard/inbox/action",
            data: {
                id: id,
                action: action
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    showDetail: function (id) {
        $.ajax({
            url: "<?= $url_path; ?>laporan/detail/action",
            data: {
                id: id
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    // window.open(result.dataUrl, "_blank");
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    showFormWizard: function (form) {
        var formNav = form.find("ul.nav");
        var formContent = form.find(".formwizards-content");
        var formActions = form.find(".formwizards-actions");

        formNav.find("li").first().addClass("active");
        formContent.hide().first().show();
        formActions.find("#btnPrev").text("Kembali").prop("disabled", true);
        formActions.find("#btnNext").text("Lanjut").prop("disabled", false);

        var activeContent = formNav.find("li").first().find("a").data("gotostep")
        var prevContent = formNav.find("li.active").prev().find("a").data("gotostep");
        var nextContent = formNav.find("li.active").next().find("a").data("gotostep");

        function formValidate() {
            var arrInp = [];
            var retVal = true;
            // Collect Form Validate
            $.each(formContent, function(idx, frm) {
                var inputs = $(frm).find("[form-validate]");
                $.each(inputs, function(idxs, inp) {
                    var fv = {
                        input: $(inp), 
                        content: formNav.find("a[data-gotostep='" + frm.id + "']"), 
                        message: $(inp).attr("form-validate")
                    };
                    arrInp.push(fv);
                });
            });
            // Check empty value
            for (var obj in arrInp){
                if(arrInp[obj].input.val() == "") {
                    alert(arrInp[obj].message);
                    arrInp[obj].content.trigger("click");
                    arrInp[obj].input.focus();
                    retVal = false;
                    return false;
                }
            }
            
            return retVal;
        }

        $(form).on("submit", function () {
            if(formValidate()){
                $(".formwizards-actions").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
                var data = new FormData(form[0]);
                $.ajax({
                    type: "POST",
                    enctype: "multipart/form-data",
                    url: "<?= $url_path; ?>laporan/simpan",
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    timeout: 600000,
                    datatype: "json",
                    success: function (data) {
                        // alert(data);
                        swal({
                            title: data.title,
                            text: data.text,
                            type: data.type,
                            confirmButtonClass: "btn-primary",
                            confirmButtonText: "OK",
                        }, function () {
                            // alert(data);
                            window.location = "<?= $url_path; ?>laporan";
                        });
                    },
                    error: function (e) {
                        //
                    }
                });
            }
        });

        formNav.find("li").each(function () {
            var contents = $(this).find("a").data("gotostep");
            $(this).find("a[data-gotostep='" + contents + "']").on("click", function () {
                activeContent = contents;
                prevContent = $(this).parent().prev().find("a").data('gotostep');
                nextContent = $(this).parent().next().find("a").data('gotostep');
                formNav.find("li").removeClass("active");
                formContent.hide();
                $(this).parent().addClass("active");
                $("#" + contents).show();

                if (prevContent === undefined) formActions.find("#btnPrev").prop("disabled", true);
                else formActions.find("#btnPrev").prop("disabled", false);

                if (nextContent === undefined) formActions.find("#btnNext").text("Simpan");
                else formActions.find("#btnNext").text("Lanjut");
            });
        });

        formContent.find("input").on("keypress", function (event) {
            if(event.keyCode === 13) $(form).trigger("submit");
        });

        formActions.find("#btnPrev").on("click", function () {
            if (prevContent !== undefined) formNav.find("a[data-gotostep='" + prevContent + "']").trigger("click");
        });

        formActions.find("#btnNext").on("click", function () {
            if (nextContent !== undefined) formNav.find("a[data-gotostep='" + nextContent + "']").trigger("click");
            else {
                $(form).trigger("submit");
            }
        });
    },
    kirimSMS: function (data) {
        swal({
            title: "PIN AKSES",
            text: "Kirim pin akses laporan, ke nomer berikut: ",
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: "Nomer Handphone",
            inputValue: data.nohp
        }, function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("Nomer Handphone tidak boleh kosong"); return false
            }
            swal.close();
            $.post("<?= $url_path; ?>laporan/sms", {
                nohp: inputValue,
                pin: data.pin
            }, function (data) {
                alert(data.status);
            }, "json");
        });
    },
    simpanRujukan: function(obj){
        $(".btnFooter").hide();
        $(".loading").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        $.post("<?= $url_path; ?>rekam_medis/rujukan/simpan", $(obj).serialize(), function (data){
            // console.log(data);
            $("#frmInputModal").modal("hide");
            swal({
                title: data.title,
                text: data.text,
                type: data.type,
                confirmButtonClass: "btn-primary",
                confirmButtonText: "OK",
            }, function () {
                window.location = "<?= $url_path; ?>laporan";
            });
        }, "json");
    },
    hapus: function (id) {
        swal({
            title: "Konfirmasi Hapus",
            text: "Yakin data ini akan dihapus ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post("<?= $url_path; ?>laporan/hapus", {
                    id: id
                }, function (data) {
                    swal({
                        title: data.title,
                        text: data.text,
                        type: data.type,
                        confirmButtonClass: "btn-primary",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location = "<?= $url_path; ?>laporan";
                    });
                }, "json");
            }
        });
    },
    hapus_surat_pengantar: function (obj) {
        var id = obj.id
        swal({
            title: "Konfirmasi Hapus",
            text: "Yakin data ini akan dihapus ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post("<?= $url_path; ?>laporan/hapus/surat_pengantar", {
                    id: id
                }, function (data) {
                    swal({
                        title: data.title,
                        text: data.text,
                        type: data.type,
                        confirmButtonClass: "btn-primary",
                        confirmButtonText: "OK",
                    }, function () {
                        $(obj).parent().parent().remove();
                    });
                }, "json");
            }
        });
    },
};