<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    <div class="block">
                        <div class="block-title">
                            <h2 id="table-title">Data Laporan </h2><small id="j-table-title"></small>
                            <?php if($session_login['unit_kerja_id'] == 'UK.001'){ ?>
                            <div class="block-options pull-right">
                                <button id="" class="btn btn-effect-ripple btn-info btnFormLaporan" data-toggle="tooltip" data-original-title="Buat Laporan"><i class="fa fa-plus"></i> Buat Laporan</button>
                            </div>
                            <?php } ?>
                        </div>

                        <form id="frmData" onsubmit="return false;" autocomplete="off">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="cari"><small>Cari Laporan : </small></label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        <?= Bootstrap::inputText('cari', 'text', '', 'class="form-control" placeholder="Cari berdasarkan nomer lp, tanggal lp, tempat kejadian, jenis kecelakaan, kategori ranmor, kondisi korban, kronologi kejadian ... "'); ?>
                                    </div>
                                </div>
                            </div>
                            <?= Bootstrap::inputKey('page', '1'); ?>
                            <?= Bootstrap::inputKey('bulan', $bulanAktif); ?>
                            <?= Bootstrap::inputKey('tahun', $tahunAktif); ?>
                            <?= Bootstrap::inputKey('status_polres', ''); ?>
                            <?= Bootstrap::inputKey('status_rekam_medis', ''); ?>
                            <?= Bootstrap::inputKey('status_kwintansi', ''); ?>
                            <?= Bootstrap::inputKey('status_surat_jaminan', ''); ?>
                            <?= Bootstrap::inputKey('status_pembayaran', ''); ?>
                        </form><br>
                        <div id="table-content"></div>
                        </div>
                    </div>
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        app.loadTabel();
        $(document).on("click", ".paging", function(){
            app.tabelPagging($(this).attr("number-page"));
        });
        $(document).on("click", ".btnFormLaporan", function(){
            app.showFormLaporan(this.id);
        });
        $(document).on("click", ".btnDetail", function(){
            app.showDetail(this.id);
        });
        $(document).on("click", ".btnSMS", function(){
            var data = {
                nohp: $(this).data("nohp"),
                pin: $(this).data("pin"),
            }
            app.kirimSMS(data);
        });
        $(document).on("change", ".filter-option", function(){
            var target = $(this).data("target");
            $("#"+target).val(this.value);
            $("#cari").trigger("change");
        });

        $(document).on("change", "#cari", function(){
            $("#page").val(1);
            app.loadTabel();
        });
    </script>
</body>