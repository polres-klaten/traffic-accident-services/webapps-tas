<?php extract($dataForm['rekam_medis']); ?>
<?= Bootstrap::inputKey('id_rekam_medis', $id_rekam_medis); ?>
<div id="rekam_medis" class="formwizards-content">
    <?php if(empty($satker_asal_rekam_medis)){ ?>
    <div class="form-group">
        <div class="col-md-6">
            <div class="col-md-12" style="margin: 15px 0px 0px; font-style: italic;">
                <label>*) Data Surat Keterangan Laka belum ada, Silahkan pilih Jasa Layanan Kesehatan yang akan mengirim data Surat Keterangan Laka</label>
            </div>
            <div class="col-md-12">
                <label for="satker_asal_rekam_medis">Jasa Layanan Kesehatan : </label>
                <?= Bootstrap::inputSelect('satker_asal_rekam_medis', $pil_jasa_kesehatan, '', 'class="select-chosen"'); ?>
            </div>
        </div>
    </div>
    <?php }else{ ?>
    <?php if($status_rekam_medis == 'baru') {?>
    <div class="form-group">
        <div class="col-md-12">
            <?= Bootstrap::inputKey('satker_asal_rekam_medis', $satker_asal_rekam_medis); ?>
            <p class="well well-sm"><strong>Informasi</strong><br>Permintaan Surat Keterangan Laka telah dikirim ke <?= $pil_jasa_kesehatan[$satker_asal_rekam_medis]; ?></p>
        </div>
    </div>
    <?php }
    else{ ?>
    <?php $width = '200px'; ?>
    <div class="form-group">
        <ul class="breadcrumb"><li><?= $pil_jasa_kesehatan[$satker_asal_rekam_medis]; ?> </li><li><a href="">Surat Keterangan Laka</a></li></ul>
        <div class="col-md-12">
            <?= Bootstrap::inputKey('satker_asal_rekam_medis', $satker_asal_rekam_medis); ?>
            <table class="table table-default table-bordered">
            <tr><td width="<?= $width; ?>">Nomer RM</td><td>: <?= $no_rekam_medis; ?></td></tr>
            <tr><td width="<?= $width; ?>">Nama Pasien</td><td>: <?= $nama_pasien; ?></td></tr>
            <tr><td width="<?= $width; ?>">Tanggal dan Umur Pasien</td><td>: <?= $tanggal_umur_pasien['tanggal']; ?></td></tr>
            <tr><td width="<?= $width; ?>">Diagnosa</td><td>: <?= $diagnosa_pasien; ?></td></tr>
            <tr><td width="<?= $width; ?>">Ruang/Bangsal</td><td>: <?= $ruang_bangsal; ?></td></tr>
            <tr><td width="<?= $width; ?>">Tanggal Masuk</td><td>: <?= FUNC::tanggal($tanggal_masuk, 'long_date'); ?></td></tr>
            <tr><td width="<?= $width; ?>">Dokter Pemeriksa</td><td>: <?= $dokter_pemeriksa; ?></td></tr>
            <tr><td width="<?= $width; ?>">Lampiran RM</td><td>: <?= $berkas_rekam_medis; ?></td></tr>
            <tr><td width="<?= $width; ?>">Keterangan</td><td>: <?= $keterangan_rekam_medis; ?></td></tr>
        </table>
        </div>
    </div>
    <?php } 
    }?>
</div>