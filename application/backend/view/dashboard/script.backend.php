/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    showInbox: function (id, action) {
        $.ajax({
            url: "<?= $url_path; ?>/inbox/action",
            data: {
                id: id,
                action: action
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    actionRekamMedisPolres: function (data) {
        $.ajax({
            url: "<?= $url_path; ?>/action/rekam-medis",
            data: data,
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    actionKwintansi: function (obj) {
        $(".form-actions").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        var data = new FormData($(obj)[0]);
        $.ajax({
            type: "POST",
            enctype: "multipart/form-data",
            url: "<?= $url_path; ?>/simpan/kwintansi",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            success: function (data) {
                swal({
                    title: data.title,
                    text: data.text,
                    type: data.type,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "OK",
                }, function () {
                    window.location = "<?= $url_path; ?>";
                });
            },
            error: function (e) {
                //
            }
        });
    },
    actionSuratJaminan: function (obj) {
        $(".form-actions").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        var data = new FormData($(obj)[0]);
        $.ajax({
            type: "POST",
            enctype: "multipart/form-data",
            url: "<?= $url_path; ?>/simpan/surat-jaminan",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            success: function (data) {
                swal({
                    title: data.title,
                    text: data.text,
                    type: data.type,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "OK",
                }, function () {
                    window.location = "<?= $url_path; ?>";
                });
            },
            error: function (e) {
                //
            }
        });
    },
    actionBuktiPembayaran: function (obj) {
        $(".form-actions").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        var data = new FormData($(obj)[0]);
        $.ajax({
            type: "POST",
            enctype: "multipart/form-data",
            url: "<?= $url_path; ?>/simpan/bukti-pembayaran",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            success: function (data) {
                swal({
                    title: data.title,
                    text: data.text,
                    type: data.type,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "OK",
                }, function () {
                    window.location = "<?= $url_path; ?>";
                });
            },
            error: function (e) {
                //
            }
        });
    },
    validate: function (e) {
        if (e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 13 || e.keyCode == 46 || e.keyCode == 37 || e.keyCode == 39) return true;
        else if (e.charCode >= 48 && e.charCode <= 57) return true;
        else return false;
    }
};