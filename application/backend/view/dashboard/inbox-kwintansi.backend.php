<style>
.table thead > tr > th{
    font-size: 10pt!important;
}
.table tbody > tr > td{
    font-size: 10pt!important;
}
input[type="file"].form-control {
    padding: 0px;
}
</style>
<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->

                    <?php extract($dataForm['formKwintansi']); ?>
                    <div class="block full">
                        <!-- Block Tabs Title -->
                        <div class="block-title">
                            <div class="block-options pull-left">
                                <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-default " data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Kembali"><i class="fa fa-reply"></i></a>
                            </div>
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active"><a href="#block-tabs-kesehatan"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Jasa Layanan Kesehatan</strong></a></li>
                                <li><a href="#block-tabs-asuransi"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Jasa Asuransi</strong></a></li>
                                <li><a href="#block-tabs-polres"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Polres</strong></a></li>
                            </ul>
                        </div>
                        <!-- END Block Tabs Title -->

                        <!-- Tabs Content -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="block-tabs-kesehatan">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form id="formInput" onsubmit="return false;" class="form-horizontal form-bordered" autocomplete="off">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label for="nominal_kwintansi">Nominal Kwintansi : </label>
                                                    <?= Bootstrap::inputKey('id_kwintansi', $id_kwintansi); ?>
                                                    <?= Bootstrap::inputKey('laporan_kepolisian_id', $laporan_kepolisian_id); ?>
                                                    <?= Bootstrap::inputText('nominal_kwintansi', 'text', $nominal_kwintansi, 'class="form-control" onKeyPress="return app.validate(event);"'); ?>
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                                <div class="col-md-12">
                                                    <label for="berkas_kwintansi">Upload File Kwintansi : </label>
                                                    <?= Bootstrap::inputText('berkas_kwintansi', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'" required'); ?>
                                                    <small><?= $keterangan_upload_lampiran; ?></small>
                                                </div>
                                                <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                                <div class="col-md-12">
                                                    <label for="keterangan_kwintansi">Keterangan : </label>
                                                    <?= Bootstrap::inputTextArea('keterangan_kwintansi', $keterangan_kwintansi, 'class="form-control" rows="6" requireds'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group form-actions">
                                                <div class="col-md-12">
                                                    <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-danger" id="btnBatal">Batal</a>
                                                    <button type="submit" class="btn btn-effect-ripple btn-primary" id="btnSimpan">Simpan</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-6">
                                        <?php $this->subView('laporan/berkas-kesehatan', $data); ?>
                                    </div>
                                </div>
                                <?php
                                    // echo '<pre style="background: none;">';
                                    // print_r($dataForm);
                                    // echo '</pre>';
                                ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-asuransi">
                                <?php $this->subView('laporan/berkas-asuransi', $data); ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-polres">
                                <?php $this->subView('laporan/berkas-polres', $data); ?>
                            </div>
                        </div>
                        <!-- END Tabs Content -->
                    </div>
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $(document).on("submit", "#formInput", function(){
            var nominal = $("#nominal_kwintansi").val();
            if(nominal == 0){
                alert("Nominal Kwintansi tidak boleh 0");
                $("#nominal_kwintansi").focus();
                return false;
            }
            app.actionKwintansi(this);
        });
    </script>
</body>