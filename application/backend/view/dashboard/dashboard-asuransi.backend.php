<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1>Dashboard</h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href="">Tahun <?= $this->tahunAktif; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->
                    <div id="modul-dashboard">

                        <!-- Widgets Info Laka -->
                        <div class="row">
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-info">
                                        <i class="fa fa-road text-light"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-warning">
                                            <strong>+ <span data-toggle="counter" data-to="<?= $infoLaka['jumlah_lr']; ?>"><?= $infoLaka['jumlah_lr']; ?></span></strong>
                                        </h2>
                                        <span class="text-muted">Luka Ringan</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-warning">
                                        <i class="fa fa-road text-light"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-warning">
                                            <strong>+ <span data-toggle="counter" data-to="<?= $infoLaka['jumlah_lb']; ?>"><?= $infoLaka['jumlah_lr']; ?></span></strong>
                                        </h2>
                                        <span class="text-muted">Luka Berat</span>
                                    </div>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a href="javascript:void(0)" class="widget">
                                    <div class="widget-content widget-content-mini text-right clearfix">
                                        <div class="widget-icon pull-left themed-background-danger">
                                        <i class="fa fa-road text-light"></i>
                                        </div>
                                        <h2 class="widget-heading h3 text-warning">
                                            <strong>+ <span data-toggle="counter" data-to="<?= $infoLaka['jumlah_md']; ?>"><?= $infoLaka['jumlah_lr']; ?></span></strong>
                                        </h2>
                                        <span class="text-muted">Meninggal Dunia</span>
                                    </div>
                                </a>
                            </div>
                        </div>

                        <!-- Widgets Operator -->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="block-title">
                                        <h2>Kiriman Laporan dari Polres</h2>
                                    </div>
                                    <div class="list-group" style="overflow-y: scroll; height: 250px;">
                                        <?php 
                                            // print_r($inboxLaporanKepolisian);
                                            foreach ($inboxLaporanKepolisian as $key => $value) { 
                                                $tanggal_waktu_kejadian = json_decode($value['tanggal_waktu_kejadian'], true);
                                        ?>
                                        <a href="javascript:app.showInbox('<?= $value['id_laporan_kepolisian']; ?>','laporan');" class="list-group-item">
                                            <span class="badge"><i class="fa fa-fw fa-inbox"></i></span>
                                            <h4 class="list-group-item-heading"><strong>Polres Klaten</strong></h4>
                                            <p class="list-group-item-text">A/n <?= $value['nama_lengkap']; ?> <small class="pull-right">Tanggal : <?= FUNC::tanggal($tanggal_waktu_kejadian['tanggal'], 'long_date'); ?></small></p>
                                        </a>
                                        <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="block-title">
                                        <h2>Permintaan Surat Jaminan</h2>
                                    </div>
                                    <div class="list-group" style="overflow-y: scroll; height: 250px;">
                                        <?php 
                                            // echo '<pre>';
                                            // print_r($inboxSuratJaminan);
                                            // echo '</pre>';
                                            foreach ($inboxSuratJaminan as $key => $value) { 
                                                $tanggal_waktu_kejadian = json_decode($value['tanggal_waktu_kejadian'], true);
                                        ?>
                                        <a href="javascript:app.showInbox('<?= $value['id_laporan_kepolisian']; ?>','surat-jaminan');" class="list-group-item">
                                            <span class="badge"><i class="fa fa-fw fa-inbox"></i></span>
                                            <h4 class="list-group-item-heading"><strong><?= $pil_jasa_kesehatan[$value['satker_asal_rekam_medis']]; ?></strong></h4>
                                            <p class="list-group-item-text">A/n <?= $value['nama_lengkap']; ?> <small class="pull-right">Tanggal : <?= FUNC::tanggal($tanggal_waktu_kejadian['tanggal'], 'long_date'); ?></small></p>
                                        </a>
                                        <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="block">
                                    <div class="block-title">
                                        <h2>Permintaan Bukti Pembayaran</h2>
                                    </div>
                                    <div class="list-group" style="overflow-y: scroll; height: 250px;">
                                        <?php 
                                            // print_r($inboxSuratJaminan);
                                            foreach ($inboxBuktiPembayaran as $key => $value) { 
                                                $tanggal_waktu_kejadian = json_decode($value['tanggal_waktu_kejadian'], true);
                                        ?>
                                        <a href="javascript:app.showInbox('<?= $value['id_laporan_kepolisian']; ?>','bukti-pembayaran');" class="list-group-item">
                                            <span class="badge"><i class="fa fa-fw fa-inbox"></i></span>
                                            <h4 class="list-group-item-heading"><strong><?= $pil_jasa_kesehatan[$value['satker_asal_rekam_medis']]; ?></strong></h4>
                                            <p class="list-group-item-text">A/n <?= $value['nama_lengkap']; ?> <small class="pull-right">Tanggal : <?= FUNC::tanggal($tanggal_waktu_kejadian['tanggal'], 'long_date'); ?></small></p>
                                        </a>
                                        <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $('[data-toggle="counter"]').each(function(){
            var $this = $(this);
            $this.countTo({
                speed: 1000,
                refreshInterval: 25,
                onComplete: function() {
                    if($this.data('after')) {
                        $this.html($this.html() + $this.data('after'));
                    }
                }
            });
        });
    </script>
</body>