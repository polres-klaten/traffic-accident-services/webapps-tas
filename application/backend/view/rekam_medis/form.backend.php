<style>
.table thead > tr > th{
    font-size: 10pt!important;
}
.table tbody > tr > td{
    font-size: 10pt!important;
}
input[type="file"].form-control {
    padding: 0px;
}
</style>
<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->

                    <?php extract($dataForm['rekam_medis']); ?>
                    <div class="block full">
                        <!-- Block Tabs Title -->
                        <div class="block-title">
                            <div class="block-options pull-left">
                                <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-default " data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Kembali"><i class="fa fa-reply"></i></a>
                            </div>
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active"><a href="#block-tabs-kesehatan"><i class="fa fa-pencil-square-o"></i> <strong><?= $page_title; ?></strong></a></li>
                                <li><a href="#block-tabs-polres"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Polres</strong></a></li>
                            </ul>
                        </div>
                        <!-- END Block Tabs Title -->

                        <!-- Tabs Content -->
                        <?php $width = '200px'; ?>
                        <div class="tab-content">
                            <div class="tab-pane active" id="block-tabs-kesehatan">
                                <form id="formInput" onsubmit="return false;" class="form-horizontal form-bordered" autocomplete="off">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <label for="no_rekam_medis">Nomer RM : </label>
                                                <?= Bootstrap::inputKey('id_rekam_medis', $id_rekam_medis); ?>
                                                <?= Bootstrap::inputKey('status_rekam_medis', $status_rekam_medis); ?>
                                                <?= Bootstrap::inputText('no_rekam_medis', 'text', $no_rekam_medis, 'class="form-control" required'); ?>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-12">
                                                <label for="nama_pasien">Nama Pasien : </label>
                                                <?= Bootstrap::inputText('nama_pasien', 'text', $nama_pasien, 'class="form-control" required'); ?>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-6">
                                                <label for="tanggal_umur_pasien">Tanggal Lahir: </label>
                                                <?= Bootstrap::inputText('tanggal_umur_pasien[]', 'text', $tanggal_umur_pasien['tanggal'], 'class="form-control input-datepicker" data-date-format="yyyy-mm-dd"'); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="tanggal_umur_pasien">Umur : </label>
                                                <?= Bootstrap::inputText('tanggal_umur_pasien[]', 'text', $tanggal_umur_pasien['umur'], 'class="form-control"'); ?>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-6">
                                                <label for="tanggal_masuk">Tanggal Masuk : </label>
                                                <?= Bootstrap::inputText('tanggal_masuk', 'text', $tanggal_masuk, 'class="form-control input-datepicker" data-date-format="yyyy-mm-dd"'); ?>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="ruang_bangsal">Ruang/Bangsal : </label>
                                                <?= Bootstrap::inputText('ruang_bangsal', 'text', $ruang_bangsal, 'class="form-control"'); ?>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-12">
                                                <label for="diagnosa_pasien">Diagnosa : </label>
                                                <?= Bootstrap::inputText('diagnosa_pasien', 'text', $diagnosa_pasien, 'class="form-control"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-12">
                                                <label for="dokter_pemeriksa">Dokter Pemeriksa : </label>
                                                <?= Bootstrap::inputText('dokter_pemeriksa', 'text', $dokter_pemeriksa, 'class="form-control"'); ?>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-12">
                                                <label for="berkas_rekam_medis">Upload File RM : <?= $berkas_rekam_medis; ?></label>
                                                <?= Bootstrap::inputText('berkas_rekam_medis', 'file', '', 'class="form-control" accept="'.$mimes_lampiran.'"'); ?>
                                                <small><?= $keterangan_upload_lampiran; ?></small>
                                            </div>
                                            <div class="col-md-12" style="margin-bottom: 15px;"></div>
                                            <div class="col-md-12">
                                                <label for="keterangan_rekam_medis">Keterangan : </label>
                                                <?= Bootstrap::inputTextArea('keterangan_rekam_medis', $keterangan_rekam_medis, 'class="form-control" rows="6" requireds'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group form-actions">
                                        <div class="col-md-12">
                                            <button type="reset" class="btn btn-effect-ripple btn-danger" id="btnBatal">Batal</button>
                                            <button type="submit" class="btn btn-effect-ripple btn-primary" id="btnSimpan">Simpan</button>
                                        </div>
                                    </div>
                                </form>
                                <?php
                                    // echo '<pre style="background: none;">';
                                    // print_r($dataForm);
                                    // echo '</pre>';
                                ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-polres">
                                <?php 
                                    if(!empty($dataForm['laporan_kepolisian']['rekam_medis_id'])) $this->subView('laporan/berkas-polres', $data); 
                                    else{ 
                                        echo '<div class="form-group"><p class="well well-sm"><strong>Informasi</strong><br>Belum ada laporan</p></div>';
                                    }
                                ?> 
                            </div>
                            
                        </div>
                        <!-- END Tabs Content -->
                    </div>

                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $(document).on("submit", "#formInput", function(){
            app.simpan(this);
        });
    </script>
</body>