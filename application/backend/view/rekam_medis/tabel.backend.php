<?= $query; ?>
<style>
	table{
		font-size: 10pt;
	}
</style>
<div class="table-responsive">
	<table class="table table-default table-hover">
	<thead>
		<tr>
			<th width="50px"><small>#</small></th>
			<th><small><strong>No. RM</strong></small></th>
			<th><small><strong>Nama Pasien</strong></small></th>
			<th><small><strong>Diagnosa</strong></small></th>
			<th><small><strong>Dokter Pemeriksa</strong></small></th>
			<th><small><strong>Ruang/Bangsal</strong></small></th>
		</tr>
	</thead>
	<tbody>
	<?php
	foreach($dataTabel as $kol){
		echo '<tr id="'.$kol['id_rekam_medis'].'" class="btnDetail" style="cursor:pointer;">';
		echo '<td>'.$no++.'</td>';
		echo '<td>'.$kol['no_rekam_medis'].'</td>';
		echo '<td>'.$kol['nama_pasien'].'</td>';
		echo '<td>'.$kol['diagnosa_pasien'].'</td>';
		echo '<td>'.$kol['dokter_pemeriksa'].'</td>';
		echo '<td>'.$kol['ruang_bangsal'].'</td>';
		echo '</tr>';
	}
	echo (empty($dataTabel)) ? '<tr><td colspan="7">Data tidak ditemukan</td></tr>' : '';
	?>
	</tbody>
	</table>
</div>
<?= ($jmlData > 0)? Bootstrap::pagging($page, $batas, $jmlData) : ''; ?>
<script>
	$("#j-table-title").html("Jumlah Data : <?= $jmlData; ?> Data");
	$("[data-toggle='tooltip']").tooltip();
</script>
