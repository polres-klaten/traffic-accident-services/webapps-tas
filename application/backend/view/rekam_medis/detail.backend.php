<style>
.table thead > tr > th{
    font-size: 10pt!important;
}
.table tbody > tr > td{
    font-size: 10pt!important;
}
input[type="file"].form-control {
    padding: 0px;
}
</style>
<body>
    <div id="page-wrapper" class="page-loading">
        <!-- Loader -->
        <?php $this->subView('page/loader'); ?>
        <!-- Modal -->
        <?php $this->subView('page/modal'); ?>
        <div id="page-container" class="header-fixed-top sidebar-visible-lg-full sidebar-light">
            <!-- Navbar -->
            <?php $this->subView('page/navbar', $data); ?>
            <!-- Main Container -->
            <div id="main-container">
                <!-- Header -->
                <?php $this->subView('page/header', $data); ?>
                <!-- Page content -->
                <div id="page-content">
                    <!-- Page Header -->
                    <div class="content-header">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="header-section">
                                    <h1><?= $page_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-sm-6 hidden-xs">
                                <div class="header-section">
                                    <ul class="breadcrumb breadcrumb-top">
                                        <li>Dashboard</li>
                                        <li><a href=""><?= $page_title; ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Page Header -->

                    <?php extract($dataForm['rekam_medis']); ?>
                    <div class="block full">
                        <!-- Block Tabs Title -->
                        <div class="block-title">
                            <div class="block-options pull-left">
                                <a href="<?= $url_path; ?>" class="btn btn-effect-ripple btn-default " data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Kembali"><i class="fa fa-reply"></i></a>
                            </div>
                            <div class="block-options pull-right">
                                <a href="javascript:void(0)" id="<?= $id_rekam_medis; ?>" class="btn btn-effect-ripple btn-default btnForm" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Edit Data"><i class="fa fa-pencil"></i></a>
                                <?php if(empty($dataForm['laporan_kepolisian']['rekam_medis_id'])) {?>
                                <a href="javascript:void(0)" id="<?= $id_rekam_medis; ?>" class="btn btn-effect-ripple btn-default btnDelete" data-toggle="tooltip" title="" style="overflow: hidden; position: relative;" data-original-title="Hapus Data"><i class="fa fa-trash"></i></a>
                                <?php } ?>
                            </div>
                            <ul class="nav nav-tabs" data-toggle="tabs">
                                <li class="active"><a href="#block-tabs-kesehatan"><i class="fa fa-pencil-square-o"></i> <strong>Data Surat Keterangan Laka</strong></a></li>
                                <li><a href="#block-tabs-polres"><i class="fa fa-pencil-square-o"></i> <strong>Berkas Polres</strong></a></li>
                            </ul>
                        </div>
                        <!-- END Block Tabs Title -->

                        <!-- Tabs Content -->
                        <?php $width = '200px'; ?>
                        <div class="tab-content">
                            <div class="tab-pane active" id="block-tabs-kesehatan">
                                <table class="table table-default table-bordered">
                                    <tr><td width="<?= $width; ?>">Nomer RM</td><td>: <?= $no_rekam_medis; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Nama Pasien</td><td>: <?= $nama_pasien; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Tanggal dan Umur Pasien</td><td>: <?= $tanggal_umur_pasien['tanggal']; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Diagnosa</td><td>: <?= $diagnosa_pasien; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Ruang/Bangsal</td><td>: <?= $ruang_bangsal; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Tanggal Masuk</td><td>: <?= FUNC::tanggal($tanggal_masuk, 'long_date'); ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Dokter Pemeriksa</td><td>: <?= $dokter_pemeriksa; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Lampiran RM</td><td>: <?= $berkas_rekam_medis; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Keterangan</td><td>: <?= $keterangan_rekam_medis; ?></td></tr>
                                    <tr><td width="<?= $width; ?>">Keterangan Lainnya</td><td>: <?= $keterangan_lainnya; ?></td></tr>
                                </table>
                                <?php
                                    // echo '<pre style="background: none;">';
                                    // print_r($dataForm);
                                    // echo '</pre>';
                                ?>
                            </div>
                            <div class="tab-pane" id="block-tabs-polres">
                                <?php 
                                    if(!empty($dataForm['laporan_kepolisian']['rekam_medis_id'])) $this->subView('laporan/berkas-polres', $data); 
                                    else{ 
                                        echo '<div class="form-group"><p class="well well-sm"><strong>Informasi</strong><br>Belum ada laporan</p></div>';
                                    }
                                ?>
                            </div>
                        </div>
                        <!-- END Tabs Content -->
                    </div>

                </div>
                <!-- END Page Content -->
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->
    </div>
    <!-- END Page Wrapper -->
    <script src="<?= $this->templatePath; ?>js/plugins.js"></script>
    <script src="<?= $this->templatePath; ?>js/app.js"></script>
    <script src="<?= $this->templatePath; ?>js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?= $link_script; ?>"></script>
    <script>
        $(document).on("click", ".btnForm", function(){
            app.showForm(this.id);
        });
        $(document).on("click", ".btnDelete", function(){
            app.hapus(this.id);
        });
    </script>
</body>