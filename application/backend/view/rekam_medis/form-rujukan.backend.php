<div class="form-group">
    <label for="satker_rujukan">Pilih Layanan Kesehatan Rujukan : </label>
    <div class="satker_rujukan">
        <?= Bootstrap::inputKey('id_laporan_kepolisian', $id_laporan_kepolisian); ?>
        <?= Bootstrap::inputSelect('satker_rujukan', $pil_rujukan, '', 'class="select-chosen" required'); ?>
    </div>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
    $(".select-chosen").chosen({width: "100%"});
</script>