/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */
app = {
    loadTabel: function () {
        $("#table-content").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Mengambil Data..</span>");
        $.post("<?= $url_path; ?>/tabel", $("#frmData").serialize(), function (data) {
            $("#table-content").html(data);
        });
    },
    tabelPagging: function (number) {
        $("#page").val(number);
        this.loadTabel();
        $(document).scrollTop(0);
    },
    showForm: function (id) {
        $.ajax({
            url: "<?= $url_path; ?>/form/action",
            data: {
                id: id
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    showDetail: function (id) {
        $.ajax({
            url: "<?= $url_path; ?>/detail/action",
            data: {
                id: id
            },
            type: "post",
            datatype: "json",
            success: function (result) {
                if (result.status == "success")
                    window.location = result.dataUrl;
            },
            async: false
        });
    },
    simpan: function (obj) {
        $(".form-actions").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>img/loader.gif' alt='' /> Menyimpan Data..</span>");
        var data = new FormData($(obj)[0]);
        $.ajax({
            type: "POST",
            enctype: "multipart/form-data",
            url: "<?= $url_path; ?>/simpan",
            data: data,
            processData: false,
            contentType: false,
            cache: false,
            timeout: 600000,
            datatype: "json",
            success: function (data) {
                // alert(data);
                swal({
                    title: data.title,
                    text: data.text,
                    type: data.type,
                    confirmButtonClass: "btn-primary",
                    confirmButtonText: "OK",
                }, function () {
                    window.location = "<?= $url_path; ?>";
                });
            },
            error: function (e) {
                //
            }
        });
    },
    hapus: function (id) {
        swal({
            title: "Konfirmasi Hapus",
            text: "Yakin data ini akan dihapus ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            closeOnConfirm: false,
            closeOnCancel: true
        }, function (isConfirm) {
            if (isConfirm) {
                $.post("<?= $url_path; ?>/hapus", {
                    id: id
                }, function (data) {
                    swal({
                        title: data.title,
                        text: data.text,
                        type: data.type,
                        confirmButtonClass: "btn-primary",
                        confirmButtonText: "OK",
                    }, function () {
                        window.location = "<?= $url_path; ?>";
                    });
                }, "json");
            }
        });
    },
};