<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class operator extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionPolres();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$this->link_script = $this->link_backend.'/'.__CLASS__.'/script';
		$this->link_pilihan_satker = $this->link_backend.'/'.__CLASS__.'/pilihan';
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Daftar Operator';
				$data['link_script'] = $this->link_script;
				$data['link_pilihan_satker'] = $this->link_pilihan_satker;
				$data['pil_unit'] = array('' => 'SEMUA UNIT KERJA') + $this->getPilihanUnitKerja();
				$data['pil_satker'] = array('' => 'SEMUA SATUAN KERJA');
				$this->mod->showView(__CLASS__.'/operator', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$data = $this->getTabelOperator($_POST);
		$this->mod->subView(__CLASS__.'/tabel', $data);
	}

	private function pilihan(){
		$data['target'] = $_POST['target'];
		$data['value'] = $_POST['value'];
		$data['pil_satker'] = ($data['target'] == 'satker') ? array('' => 'SEMUA SATUAN KERJA') : array('' => 'PILIH SATUAN KERJA');
		$data['pil_satker'] += $this->getPilihanSatuanKerja($_POST['unit']);
		$this->mod->subView(__CLASS__.'/pilihan-satker', $data);
	}

	private function form(){
		$data['dataForm'] = $this->getDataOperator($_POST['id']);
		$dataSatker = $this->getDataSatuanKerja($data['dataForm']['satuan_kerja_id']);
		$data['unit_kerja'] = $dataSatker['unit_kerja_id'];
		$data['link_pilihan_satker'] = $this->link_pilihan_satker;
		$data['pil_unit'] = array('' => 'PILIH UNIT KERJA') + $this->getPilihanUnitKerja();
		$data['pil_satker'] = array('' => 'PILIH SATUAN KERJA');
		$this->mod->subView(__CLASS__.'/form', $data);
	}
	
	private function simpan(){
		$data = $this->getDataOperator($_POST['id_operator']);
		foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
		$data['password_operator'] = FUNC::encryptor($data['password_operator']);
		unset($data['form_title']);
		$result = $this->db->save_update('tref_operator', $data);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
	}

	private function hapus(){
		$result = $this->db->delete('tref_operator', array('id_operator' => $_POST['id']));
		$this->showResponse($this->error_msg['hapus'][$result['error']]);
	}

}

new operator($this->librarys, $this->params);
?>
