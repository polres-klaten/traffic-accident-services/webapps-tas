<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

/**
 * Proses Bisnis Laporan Polisi
 * LP/SP/DD = Polres
 * RM/Kwintansi = Jasa Layanan Kesehatan
 * Surat Jamin/Bukti Pembayaran = Jasa Asuransi
 * 
 * LP/SP/DD + RM -> SP -> KW -> BP
 * 
 * Surat Pengantar:
 * 1. Pengantar Jaminan (masih perawatan)
 * 2. Penolakan (tidak memenuhi unsur)
 * 3. Rekam Medis (sudah pulang/klaim sendiri/bayar umum/telat)
 * 
 * Yang tidak diklaim Jasa Raharja
 * 1. Laka Tunggal
 * 2. Pejalan Kaki
 * 3. Hewan
 * 4. Sepeda Angin
 * 
 * Jika laporan dari rumah sakit (mengirim RM), maka action dipolres
 * 1. Buat LP (tidak lanjuti)
 * 2. Tolak (kirim pesan keterangan penolakan)
 * 3. Non LP (kirim pesan dibayar umum)
 * 
 * Jika kondisi korban MD (Meninggal Dunia), maka akan muncul form
 * 1. Input Surat Keterangan Ahli Waris
 * 2. Upload Berkas tersebut
 * 3. Input No. Rekening a/n Ahli Waris
 * 
 * Jika pilihan jaminan asuransi BPJS Ketenagakerjaan, maka perlu upload berkas SKKL (pdf/jpg)
 * 
 * Tempat Kejadian (Desa, Kecamatan, Kabupaten, Jalan)
 * 
 * Tempat Kejadian (Kel/Desa, Kecamatan, Kabupaten)
 * Alamat Data Diri (RT/RW, Kel/Desa, Kecamatan)
 * Tempat Tanggal Lahir (Tempat, Tanggal Lahir)
 * 
 */

class laporan extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$this->data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
		$this->data['link_pilihan_kecamatan'] = $this->link_backend.'/'.__CLASS__.'/pilihan/kecamatan';
		$this->data['link_pilihan_desa'] = $this->link_backend.'/'.__CLASS__.'/pilihan/desa';
		$this->data['url_path'] = $this->link_backend.'/'.__CLASS__;
		$method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Daftar Laporan';
				$data['bulanAktif'] = isset($_POST['bulan']) ? intval($_POST['bulan']) : intval($this->mod->bulanAktif);
				$data['tahunAktif'] = isset($_POST['tahun']) ? intval($_POST['tahun']) : intval($this->mod->tahunAktif);
				$this->mod->showView(__CLASS__.'/laporan', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
		$data['url_path'] = $this->link_backend.'/';
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$data = $this->getTabelLaporanKepolisian($_POST);
		$data['link_file_image'] = $this->link_file_image;
		$data['pil_status'] = array('' => 'Semua Status') + $this->mod->pilihanStatusUploadBerkas;
		$this->mod->subView(__CLASS__.'/tabel', $data);
	}

	private function pilihan($id){
		switch ($id) {
			case 'kecamatan':
				$pil_kecamatan = array('' => 'Pilih Kecamatan') + $this->getPilihanKecamatan($_POST['value']);
				$target = str_replace('[]', '_desa', $_POST['id']);
				echo Bootstrap::inputSelect($_POST['id'], $pil_kecamatan, '', 'class="select-chosen filter-option" data-link="'.$this->data['link_pilihan_desa'].'" data-target="'.$target.'"');
				break;

			case 'desa':
				$pil_desa = array('' => 'Pilih Desa') + $this->getPilihanKelDesa($_POST['value']);
				echo Bootstrap::inputSelect($_POST['id'], $pil_desa, '', 'class="select-chosen"');
				break;
			
			default:
				# code...
				break;
		}
	}

	private function form($id){
		$result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		/**
		 * Action: Buat Session Berisi ID Laporan
		 * Form: Ambil data pada database sesuai dengan ID Laporan pada Session (Jika Kosong => Buat | Jika Ada => Edit)
		 */

		switch ($id) {
			case 'action':
				if(isset($_POST['id'])){
					$this->mod->setSession('SESSION_FORM_LAPORAN', $_POST['id']);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/'.__CLASS__.'/form';
				}
				$this->showResponse($result);
				break;
			
			default:
				/**
				 * Langkah pembuatan laporan
				 * 1. Isi Laporan Kepolisian
				 * 2. Isi Data Identitas
				 * 3. Pilih Rumah Sakit untuk meminta RM, jika RM sudah ada tampilkan data RM tersebut
				 * 4. Pilih Jasa Layanan Asuransi
				 */
				$this->checkSessionPolres();
				$IDLaporan = $this->mod->getSession('SESSION_FORM_LAPORAN');
				$formLaporan = $this->getDataLaporanKepolisian($IDLaporan);
				// $this->debugResponse($_SESSION); die;
				if(empty($formLaporan)){
					// Check jika data laporan kosong maka redirect ke menu laporan
					echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'/'.__CLASS__.'\'" />'; die;
				}
				$dataForm = $this->berkasPolres($formLaporan);
				$dataForm += $this->berkasKesehatan($formLaporan);
				
				$data = $this->data;
				$data['dataForm'] = $dataForm;
				$data['page_title'] = $dataForm['laporan_kepolisian']['form_title'];
				$data['pil_kabupaten_tempat_kejadian'] = array('' => 'Pilih Kabupaten') + $this->getPilihanKabupaten();
				$data['pil_kecamatan_tempat_kejadian'] = array('' => 'Pilih Kecamatan') + $this->getPilihanKecamatan($dataForm['laporan_kepolisian']['tempat_kejadian']['kabupaten']);
				$data['pil_desa_tempat_kejadian'] = array('' => 'Pilih Desa') + $this->getPilihanKelDesa($dataForm['laporan_kepolisian']['tempat_kejadian']['kecamatan']);
				$data['pil_kabupaten_alamat_data_identitas'] = array('' => 'Pilih Kabupaten') + $this->getPilihanKabupaten();
				$data['pil_kecamatan_alamat_data_identitas'] = array('' => 'Pilih Kecamatan') + $this->getPilihanKecamatan($dataForm['data_identitas']['alamat_data_identitas']['kabupaten']);
				$data['pil_desa_alamat_data_identitas'] = array('' => 'Pilih Desa') + $this->getPilihanKelDesa($dataForm['data_identitas']['alamat_data_identitas']['kecamatan']);
				// $this->debugResponse($dataForm); die;
				$this->mod->showView(__CLASS__.'/form', $data);
				break;
		}
	}

	private function detail($id){
		$result = array(
			'status' => 'error',
			'dataUrl' => ''
		);

		switch ($id) {
			case 'action':
				if(isset($_POST['id'])){
					$this->mod->setSession('SESSION_FORM_LAPORAN', $_POST['id']);
					$result['status'] = 'success';
					$result['dataUrl'] = $this->link_backend.'/'.__CLASS__.'/detail';
				}
				$this->showResponse($result);
				break;
			
			default:
				$IDLaporan = $this->mod->getSession('SESSION_FORM_LAPORAN');
				$formLaporan = $this->getDataLaporanKepolisian($IDLaporan);
				if(empty($formLaporan)){
					// Check jika data laporan kosong maka redirect ke menu laporan
					echo '<meta http-equiv="refresh" content="0;URL=\''.$this->link_backend.'/'.__CLASS__.'\'" />'; die;
				}
				$dataForm = $this->berkasPolres($formLaporan);
				$dataForm += $this->berkasKesehatan($formLaporan);
				$dataForm += $this->berkasAsuransi($formLaporan);
				
				$data = $this->data;
				$data += $this->pilInfoKabKecDes($dataForm);
				$data['dataForm'] = $dataForm;
				$data['page_title'] = 'Detail Laporan';
				// $this->debugResponse($data['dataForm']);die;
				$this->mod->showView(__CLASS__.'/detail', $data);
				break;
		}
	}

	private function simpan(){
		$result = array();

		// Ubah nama satker dari checkbox satker tujuan menjadi kode satker
		if(isset($_POST['satker_tujuan_laporan_kepolisian'])){
			$kode_satker = array();
			$pil_jasa_asuransi = $this->getPilihanSatuanKerja('UK.002');
			foreach ($pil_jasa_asuransi as $key => $value) {if(in_array($value, $_POST['satker_tujuan_laporan_kepolisian'])) $kode_satker[] = $key;}
			$_POST['satker_tujuan_laporan_kepolisian'] = implode(',', $kode_satker);
		}

		$formRekamMedis = $this->db->getDataTabel('tref_rekam_medis', array('id_rekam_medis', $_POST['id_rekam_medis']));
		// Check jika asal dan tujuan rekam medis kosong, maka polres merequest rm ke rs
		if(empty($formRekamMedis['satker_asal_rekam_medis']) && empty($formRekamMedis['satker_tujuan_rekam_medis'])){
			$formRekamMedis['status_rekam_medis'] = 'baru';
			$formRekamMedis['jenis_rekam_medis'] = 'permintaan';
			$formRekamMedis['nama_pasien'] = $_POST['nama_lengkap'];
			$formRekamMedis['satker_asal_rekam_medis'] = $_POST['satker_asal_rekam_medis'];
			$formRekamMedis['satker_tujuan_rekam_medis'] = $this->session_login['id_satuan_kerja'];
		}
		$_POST['rekam_medis_id'] = $formRekamMedis['id_rekam_medis'];
		$dataForm['rekam_medis'] = $this->extractJSON($formRekamMedis, false);
		foreach ($dataForm['rekam_medis'] as $key => $value) {
			// Check jika data adalah array
			if(is_array($value)) $dataForm['rekam_medis'][$key] = json_encode($dataForm['rekam_medis'][$key]);
		}
		array_push($result, array(
			'tabel' => 'tref_rekam_medis',
			'data' => $dataForm['rekam_medis'],
			'result' => '',
		));
		
		$laporan_kepolisian_id = $_POST['id_laporan_kepolisian'];
		$formLaporan = $this->db->getDataTabel('tref_laporan_kepolisian', array('id_laporan_kepolisian', $_POST['id_laporan_kepolisian']));
		$dataForm['laporan_kepolisian'] = $this->extractJSON($formLaporan, false);
		$dataForm['laporan_kepolisian']['satker_asal_laporan_kepolisian'] = $this->session_login['id_satuan_kerja'];
		foreach($dataForm['laporan_kepolisian'] as $key => $value){
			// Check input post
			if(is_array($value)){
				$dataForm['laporan_kepolisian'][$key] = $this->convertIndexToAssoc($dataForm['laporan_kepolisian'][$key], $_POST[$key]);
				$dataForm['laporan_kepolisian'][$key] = json_encode($dataForm['laporan_kepolisian'][$key]);
			}else{
				if(isset($_POST[$key])) $dataForm['laporan_kepolisian'][$key] = $_POST[$key];
				// Check jika ada inputan yang masih array (ex. array indexing bisa diubah ke json atau string) = OPTIONAL
				if(is_array($dataForm['laporan_kepolisian'][$key])){
					// $dataForm['laporan_kepolisian'][$key] = implode(',', $dataForm['laporan_kepolisian'][$key]);
					// $dataForm['laporan_kepolisian'][$key] = json_encode($dataForm['laporan_kepolisian'][$key]);
				}
			}
			// check input files
			if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
				$upload = $this->uploadLampiran($_FILES[$key]);
				$dataForm['laporan_kepolisian'][$key] = $upload['UploadFile'];
			}
		}
		array_push($result, array(
			'tabel' => 'tref_laporan_kepolisian',
			'data' => $dataForm['laporan_kepolisian'],
			'result' => '',
		));
		
		$formLaporan = $this->db->getDataTabel('tref_data_identitas', array('id_data_identitas', $_POST['id_data_identitas']));
		$dataForm['data_identitas'] = $this->extractJSON($formLaporan, false);
		$dataForm['data_identitas']['laporan_kepolisian_id'] = $laporan_kepolisian_id;
		foreach($dataForm['data_identitas'] as $key => $value){
			// Check input post
			if(is_array($value)){
				$dataForm['data_identitas'][$key] = $this->convertIndexToAssoc($dataForm['data_identitas'][$key], $_POST[$key]);
				$dataForm['data_identitas'][$key] = json_encode($dataForm['data_identitas'][$key]);
			}else{
				if(isset($_POST[$key])) $dataForm['data_identitas'][$key] = $_POST[$key];
			}
			// check input files
			if(isset($_FILES[$key]['name']) && !empty($_FILES[$key]['name'])){
				$upload = $this->uploadLampiran($_FILES[$key]);
				$dataForm['data_identitas'][$key] = $upload['UploadFile'];
			}
		}
		array_push($result, array(
			'tabel' => 'tref_data_identitas',
			'data' => $dataForm['data_identitas'],
			'result' => '',
		));
		
		/**
		 * Multiple Upload
		 * Buat Template Form Upload 
		 * array('name' =>'', 'type' =>'', 'tmp_name' =>'', 'error' =>'', 'size' =>'');
		 * Default $_FILES => object = array('name' =>'', 'type' =>'', 'tmp_name' =>'', 'error' =>'', 'size' =>'');
		 * Karena Object bertipe array ex. object => array[0]['name'] dst.., maka harus diubah menjadi default tipe $_FILES
		 * Data Acuan id_surat_pengantar
		 */
		
		$dataForm['surat_pengantar'] = array();
		$jumlahData = count($_POST['id_surat_pengantar']);
		$defaultID = $_POST['id_surat_pengantar'][$jumlahData - 1]; // Jika ada penambahan data diawali dengan id ini
		for ($i=0; $i < $jumlahData; $i++) { 
			$form_surat_pengantar = $this->db->getDataTabel('tref_surat_pengantar', array('id_surat_pengantar', $_POST['id_surat_pengantar'][$i]));
			foreach ($form_surat_pengantar as $key => $value) {
				// check input post
				if(isset($_POST[$key][$i])) $form_surat_pengantar[$key] = $_POST[$key][$i];
				// check input files
				if(isset($_FILES[$key]['name'][$i]) && !empty($_FILES[$key]['name'][$i])){
					$form_upload = array('name' =>'', 'type' =>'', 'tmp_name' =>'', 'error' =>'', 'size' =>'');
					foreach ($form_upload as $keys => $values) {$form_upload[$keys] = $_FILES[$key][$keys][$i];}
					$upload = $this->uploadLampiran($form_upload);
					$form_surat_pengantar[$key] = $upload['UploadFile'];
				}
			}
			// check jika ada penambahan data, maka isi id laporan
			if(empty($form_surat_pengantar['laporan_kepolisian_id'])){
				// check jika file berkas terupload, maka masukan kedalam daftar simpan
				if(!empty($form_surat_pengantar['berkas_surat_pengantar'])){
					$form_surat_pengantar['laporan_kepolisian_id'] = $laporan_kepolisian_id;
					$form_surat_pengantar['id_surat_pengantar'] = $defaultID++;
					$dataForm['surat_pengantar'][$i] = $form_surat_pengantar;
				}
			}else{
				$dataForm['surat_pengantar'][$i] = $form_surat_pengantar;
			}
		}
		array_push($result, array(
			'tabel' => 'tref_surat_pengantar',
			'data' => $dataForm['surat_pengantar'],
			'result' => '',
		));
		
		// Simpan data pada daftar simpan
		foreach ($result as $key => $val) {
			if($this->isAssoc($val['data'])){$result[$key]['result'] = $this->db->save_update($val['tabel'], $val['data']);
			}else{
				$sub_result = array();
				foreach ($val['data'] as $keys => $value) {$sub_result[$keys] = $this->db->save_update($val['tabel'], $value);}
				$result[$key]['result'] = $sub_result;
			}
		}
		
		$this->showResponse($this->error_msg['simpan'][1]);
		// print_r($_POST);
		// print_r($result);
	}

	private function sms(){
		$nohp = $_POST['nohp'];
		$pin = $_POST['pin'];
		$result = $this->sendSMS($nohp, $pin);
		$this->showResponse($result);
	}

	private function hapus($id){
		switch ($id) {
			case 'surat_pengantar':
				$result = $this->db->delete('tref_surat_pengantar', array('id_surat_pengantar' => $_POST['id']));
				$this->showResponse($this->error_msg['hapus'][$result['error']]);
				break;
			
			default:
				$dataLaporan = $this->getDataLaporanKepolisian($_POST['id']);
				// Hapus data Rekam Medis
				$result = $this->db->delete('tref_rekam_medis', array('id_rekam_medis' => $dataLaporan['rekam_medis_id']));
				$result = $this->db->delete('tref_laporan_kepolisian', array('id_laporan_kepolisian' => $_POST['id']));
				$result = $this->db->delete('tref_data_identitas', array('laporan_kepolisian_id' => $_POST['id']));
				$result = $this->db->delete('tref_surat_pengantar', array('laporan_kepolisian_id' => $_POST['id']));
				$result = $this->db->delete('tref_surat_jaminan', array('laporan_kepolisian_id' => $_POST['id']));
				$result = $this->db->delete('tref_kwintansi', array('laporan_kepolisian_id' => $_POST['id']));
				$result = $this->db->delete('tref_pembayaran', array('laporan_kepolisian_id' => $_POST['id']));
				$this->showResponse($this->error_msg['hapus'][$result['error']]);
				break;
		}
	}

}

new laporan($this->librarys, $this->params);
?>