<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class masyarakat extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionPolres();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Laporan Masyarakat';
				$data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
				$data['bulanAktif'] = isset($_POST['bulan']) ? intval($_POST['bulan']) : intval($this->mod->bulanAktif);
				$data['tahunAktif'] = isset($_POST['tahun']) ? intval($_POST['tahun']) : intval($this->mod->tahunAktif);
				$data['pil_status'] = array('' => 'Semua Status') + $this->mod->pilihanStatusLaporanMasyarakat;
				$data['pil_publish'] = array('' => 'Publish/Non-Publish') + $this->mod->pilihanPublishLaporanMasyarakat;
				$this->mod->showView(__CLASS__.'/masyarakat', $data);
				break;
				
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$data = $this->getTabelLaporanMasyarakat($_POST);
		$data['link_file_image'] = $this->link_file_image;
		$this->mod->subView(__CLASS__.'/list', $data);
	}

	private function form(){
		$data['dataForm'] = $this->getDataLaporanMasyarakat($_POST['id']);
		$data['link_file_image'] = $this->link_file_image;
		$data['pil_status'] = array('' => 'Pilih Status') + $this->mod->pilihanStatusLaporanMasyarakat;
		$data['pil_publish'] = $this->mod->pilihanPublishLaporanMasyarakat;
		$data['dataForm']['form_title'] = str_replace('Edit', '', $data['dataForm']['form_title']);
		$this->mod->subView(__CLASS__.'/form', $data);
	}
	
	private function simpan(){
		$data = $this->getDataLaporanMasyarakat($_POST['id_laporan_masyarakat']);
		foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
		unset($data['form_title']);
		$result = $this->db->save_update('tref_laporan_masyarakat', $data);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
	}

	private function hapus(){
		$result = $this->db->delete('tref_laporan_masyarakat', array('id_laporan_masyarakat' => $_POST['id']));
		$this->showResponse($this->error_msg['hapus'][$result['error']]);
	}

}

new masyarakat($this->librarys, $this->params);
?>