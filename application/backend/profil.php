<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class profil extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Profil';
				$data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
				$data['dataForm'] = $this->getInfoProfil($this->session_login['satuan_kerja_id']);
                $this->mod->showView(__CLASS__.'/profil', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
        header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}
	
	private function simpan(){
        $data = $this->db->getTabel('tref_satuan_kerja');
		foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
		$result = $this->db->save_update('tref_satuan_kerja', $data);
		$this->showResponse($this->error_msg['simpan'][$result['error']]);
    }

}

new profil($this->librarys, $this->params);
?>

