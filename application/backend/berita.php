<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class berita extends backend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->checkSessionPolres();
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
        $method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$data['page_title'] = 'Daftar Berita';
				$data['link_script'] = $this->link_backend.'/'.__CLASS__.'/script';
				$this->mod->showView(__CLASS__.'/berita', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
    }

    private function script(){
		header('Content-Type: application/javascript');
        $data['url_path'] = $this->link_backend.'/'.__CLASS__;
        $this->mod->subView(__CLASS__.'/script', $data);
	}

	private function tabel(){
		$data = $this->getTabelBerita($_POST);
		$data['link_file_image'] = $this->link_file_image;
		$this->mod->subView(__CLASS__.'/tabel', $data);
	}

	private function form(){
		$data['dataForm'] = $this->getDataBerita($_POST['id']);
		$data['mimes_image'] = $this->file->getMimeTypes($this->mod->file_type_image);
		$data['keterangan_upload_image'] = '*) File Type : '.$this->mod->file_type_image.', Max Size : '.($this->mod->max_size / 1024 /1024).'Mb';
		$this->mod->subView(__CLASS__.'/form', $data);
	}
	
	private function simpan(){
		$upload = $this->uploadImage($_FILES);
		if($upload['status'] == 'success'){
			$data = $this->getDataBerita($_POST['id_berita']);
			foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
			$data['link_berita'] = FUNC::slug($data['judul_berita']);
			$data['gambar_berita'] = empty($upload['UploadFile']) ? $data['gambar_berita'] : $upload['UploadFile'];
			if(empty($data['gambar_berita'])) $data['gambar_berita'] = 'noimages.png';
			unset($data['form_title']);
			$result = $this->db->save_update('tref_berita', $data);
			$this->showResponse($this->error_msg['simpan'][$result['error']]);
		}else{
			$this->error_msg['upload']['text'] = $upload['errorMsg'];
			$this->showResponse($this->error_msg['hapus'][$result['error']]);
		}
	}

	private function hapus(){
		$result = $this->db->delete('tref_berita', array('id_berita' => $_POST['id']));
		$this->showResponse($this->error_msg['hapus'][$result['error']]);
	}

}

new berita($this->librarys, $this->params);
?>