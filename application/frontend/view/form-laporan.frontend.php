<style>
input[type="file"].form-control {
	padding: 0px;
}
</style>
<?php extract($dataForm); ?>
<?= Bootstrap::inputKey('id_laporan_masyarakat', $id_laporan_masyarakat); ?>
<div class="form-group">
    <label for="nama_masyarakat">Nama laporan_masyarakat</label>
    <?= Bootstrap::inputText('nama_masyarakat', 'text', $nama_masyarakat, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="noktp_masyarakat">Nomer KTP : </label>
    <?= Bootstrap::inputText('noktp_masyarakat', 'text', $noktp_masyarakat, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="nohp_masyarakat">Nomer HP : </label>
    <?= Bootstrap::inputText('nohp_masyarakat', 'text', $nohp_masyarakat, 'class="form-control" required'); ?>
</div>
<div class="form-group">
    <label for="files">Gambar : </label>
    <?= Bootstrap::inputText('files', 'file', '', 'class="form-control" required accept="'.$mimes_image.'"'); ?>
    <small><?= $keterangan_upload_image; ?></small>
</div>
<div class="form-group">
    <label for="isi_laporan_masyarakat">Isi Laporan : </label>
    <?= Bootstrap::inputTextArea('isi_laporan_masyarakat', '', 'class="form-control" rows="4" required'); ?>
</div>
<script>
    $("#frmInputModalLabel").html("<?= $form_title; ?>");
</script>