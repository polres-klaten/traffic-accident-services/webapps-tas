<div class="homepage-area modul-informasi">
    <div class="container-fluid acurate">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="<?= $this->templatePath; ?>assets/images/logo-tas.png">
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 base-form">
                <h3 class="media-heading"><strong><em>INFO PERKEMBANGAN LAYANAN ASURANSI</em></strong></h3>
                <table class="table table-bordered">
                    <tr><td width="30%">Nama Pasien</td><td>: <?= $nama_pasien; ?></td></tr>
                    <tr><td width="30%">Berkas Polres</td><td>: <?= $status_berkas_polres; ?></td></tr>
                    <tr><td width="30%">Berkas Rekam Medis</td><td>: <?= $status_berkas_rekam_medis; ?></td></tr>
                    <tr><td width="30%">Berkas Kwintansi</td><td>: <?= $status_berkas_kwintansi; ?></td></tr>
                    <tr><td width="30%">Berkas Surat Jaminan</td><td>: <?= $status_berkas_surat_jaminan; ?></td></tr>
                    <tr><td width="30%">Berkas Bukti Pembayaran</td><td>: <?= $status_berkas_pembayaran; ?></td></tr>
                </table>
                <button class="btn btn-default" type="button" onclick="window.open('<?= $link_frontend.'layanan' ?>', '_self')">Keluar</button>
            </div>
        </div>
    </div>
</div>