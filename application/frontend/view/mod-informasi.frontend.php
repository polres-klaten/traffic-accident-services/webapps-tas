<?php $this->subView('modal', $data); ?>
<div class="slider-bottom-area modul-informasi">
  <div class="container">
    <div class="row">
        <form id="frmLaporan" class="base-form" method="post" onsubmit="return false;" autocomplete="off">
              <label for="tahun">Pilih Tahun : </label>
              <div class="form-group row">
                <div class="col-md-6">
                  <?= Bootstrap::inputSelect('tahun', $this->pilihanTahun, $this->tahunAktif, 'class="panel-konfigurasi selectpicker" data-live-search="true" data-placeholder="Pilih Tahun.." style="width: 100%;"'); ?>
                  <?= Bootstrap::inputSelect('info_grafis', $this->pilihanInfoGrafis, '', 'class="panel-konfigurasi selectpicker" data-live-search="true" data-placeholder="Pilih Tahun.." style="width: 100%;"'); ?>
                </div>
                <div class="col-md-6">
                  <button onclick="javascript:showForm('');" class="pull-right">Kirim Laporan</button>
                </div>
              </div>
        </form>
    </div>

    <div class="grafik-informasi" style="width: 100%; height: 400px; margin: 30px auto; overflow-y: auto;">
        <div id="info_laka" style="width: 100%; height: auto; margin: 0 auto"></div>
    </div>

    <div class="slider-bottom">
      <h3 class="media-heading">LAPORAN MASYARAKAT</h3>
      <div class="total-business">
        <?php foreach ($laporanMasyarakat['dataTabel'] as $key => $value) { ?>
        <div class="single-business">
          <span class="flaticon-presentation"></span>
          <img src="<?= $link_file_image.$value['gambar_laporan_masyarakat']; ?>" alt="single news Image">
          <p class="question">
            <strong><?= $value['nama_masyarakat']; ?></strong><br>
            <small><?= FUNC::tanggal($value['tanggal_laporan_masyarakat'], 'long_date'); ?></small> | <small class="label label-<?= $this->pilihanClassColorStatusLaporanMasyarakat[$value['status_laporan_masyarakat']]; ?>"><?= $this->pilihanStatusLaporanMasyarakat[$value['status_laporan_masyarakat']]; ?></small><br>
            <?= nl2br($value['isi_laporan_masyarakat']); ?></p>
          <p class="answer">
            <strong>Admin : </strong><br>
            <?= nl2br($value['balasan_laporan_masyarakat']); ?></p>
        </div>
        <?php } ?>
      </div>
    </div>
  </div>
</div>
<script>
  showCharts();
  $(document).on("submit", "#frmInput", function(){
    simpan(this);
  });
  $(document).on("change", "#info_grafis", function(){
    showCharts();
  });
  function showForm(id){
    $.post("<?= $link_frontend.'masyarakat'; ?>/form", {id: id}, function (data){
        $("#data-form-input").html(data);
        $("#frmInputModal").modal("show");
        $(".btnFooter").show();
        $(".loading").html("");
    });
  };
  function showCharts(){
    $.post("<?= $link_frontend.'masyarakat'; ?>/chart", $("#frmLaporan").serialize(), function (laporanLaka){
        Highcharts.chart({
            chart: { type: 'bar', height: laporanLaka.height, renderTo: 'info_laka' },
            title: { text: laporanLaka.title },
            subtitle: { text: laporanLaka.subtitle },
            xAxis: {
                categories: laporanLaka.category,
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: { text: 'Jumlah Laka Lantas' }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} '+laporanLaka.tooltip+'</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            credits: { enabled: false },
            series: laporanLaka.series
        });
    });
  };
  function simpan(obj){
    var data = new FormData($(obj)[0]);
    $(".btnFooter").hide();
    $(".loading").html("<span style='font-style:italic;'><img src='<?= $this->templatePath; ?>assets/images/loader.gif' alt='' /> Menyimpan Data..</span>");
    $.ajax({
        type: "POST",
        enctype: "multipart/form-data",
        url: "<?= $link_frontend.'masyarakat'; ?>/simpan",
        data: data,
        processData: false,
        contentType: false,
        cache: false,
        timeout: 600000,
        datatype: "json",
        success: function (data){
            console.log(data);
            alert(data)
            window.location.reload();
        },
        error: function (e) {
            //
        }
    });
  };
</script>

