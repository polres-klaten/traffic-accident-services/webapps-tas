<div class="main-news-page-section-area">
	<div class="container">
		<div class="row">
	        <div class="col-md-8"><h3 class="media-heading">SATLANTAS NEWS</h3></div>
		    <form id="frmBerita" class="base-form" method="post" onsubmit="return false;" autocomplete="off">
	            <div class="form-group col-md-4">
	            	<div class="input-group">
	            		<?= Bootstrap::inputText('cari', 'text', $session_berita['cari'], 'class="form-control" placeholder="Cari Berita"'); ?>
                    	<?= Bootstrap::inputKey('page', $session_berita['page']); ?>
                        <div class="input-group-btn">
                            <button class="btn btn-default" type="submit">CARI</button>
                        </div>
                    </div>
	            </div>
	        </form>
 	 	</div>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
				<div class="news-page-content-section-area">
					<div class="row single-news-area">
					<?php if(empty($berita['dataBerita'])){ ?>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<div class="media-body news-body">
								<h3 class="media-heading">Maaf</h3>
								<p class="news-content">Berita yang Anda cari tidak ditemukan</p><!-- <div class="read-more"><a href="<?= $link_berita.$value['link_berita']; ?>.html">Read More  <i class="fa fa-angle-right" aria-hidden="true"></i></a></div> -->
							</div>
						</div>
					<?php 
					}else{
					foreach ($berita['dataBerita'] as $key => $value) { ?>
						<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
							<div class="new-featured-image">
								<a href="<?= $link_berita.$value['link_berita']; ?>.html">
									<img class="media-object" src="<?= $link_file_image.$value['gambar_berita']; ?>" alt="Generic placeholder image">
								</a>
							</div>
						</div>
						<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
							<div class="media-body news-body">
								<h3 class="media-heading"><a href="<?= $link_berita.$value['link_berita']; ?>.html"><?= $value['judul_berita']; ?></a></h3>
								<p class="meta"><?= FUNC::tanggal($value['datetime'], 'long_date'); ?> - By Administrator</p>
								<p class="news-content"><?= substr(strip_tags($value['isi_berita']), 0, 200); ?>...</p>
								<div class="read-more"><a href="<?= $link_berita.$value['link_berita']; ?>.html">Read More  <i class="fa fa-angle-right" aria-hidden="true"></i></a></div>
							</div>
						</div>
					<?php }
					} ?>
					</div>
				</div>
				<div class="pagination-area">
					<?= ($berita['jmlData'] > 0) ? Bootstrap::paggingFrontend($berita['page'], $berita['batas'], $berita['jmlData']) : ''; ?>
			  	</div>
			</div>
		</div>
	</div>
</div>