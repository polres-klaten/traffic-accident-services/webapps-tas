<footer class="footer-area">
  <div class="footer-buttom-area">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-4 hidden-xs footer-logo">
          <img src="<?= $this->templatePath; ?>assets/images/footer-logo.png">
        </div>
        <div class="col-lg-6 col-md-6 col-sm-5 col-xs-12 footer-address">
          <?= $dataProfil['alamat_satuan_kerja']; ?><br>No. Telepon : <?= $dataProfil['telepon_satuan_kerja']; ?>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
          <div class="footer-social-media">
            <ul>
              <li><a href="https://www.facebook.com/<?= $dataProfil['facebook_satuan_kerja']; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://instagram.com/<?= $dataProfil['instagram_satuan_kerja']; ?>" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>