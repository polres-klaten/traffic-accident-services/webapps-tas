<div class="homepage-area modul-informasi">
    <div class="container-fluid acurate">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="<?= $this->templatePath; ?>assets/images/jempol.png">
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3><strong><em>DAPATKAN PERKEMBANGAN INFORMASI PERKARA<br>YANG DITANGANI SATRESKRIM POLRES BONE</em></strong></h3>
                <form id="frmMember" class="base-form" method="post" onsubmit="return false;" autocomplete="off">
                    <div class="form-group col-md-12">
                        <label for="masyarakat">Masyarakat (Pelapor/Korban)
                        <input id="masyarakat" type="radio" name="jenis_laporan" value="masyarakat" checked /></label>
                        <label for="jpu">JPU
                        <input id="jpu" type="radio" name="jenis_laporan" value="jpu" /></label>
                        <label for="pengadilan">Pengadilan Negeri
                        <input id="pengadilan" type="radio" name="jenis_laporan" value="pengadilan" /></label>
                    </div>
                    <div class="form-group col-md-6">
                        <input d="pin" name="pin" type="text" placeholder="Masukan PIN" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <button class="btn btn-default btn-block" type="submit">CHECK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>