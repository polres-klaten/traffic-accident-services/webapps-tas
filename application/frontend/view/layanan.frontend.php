<div class="homepage-area modul-informasi">
    <div class="container-fluid acurate">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img src="<?= $this->templatePath; ?>assets/images/logo-tas.png">
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h3><strong><em>MASUKAN PIN UNTUK DAPATKAN PERKEMBANGAN INFORMASI LAYANAN ASURANSI ANDA</em></strong></h3>
                <form id="frmMember" class="base-form" method="post" onsubmit="" autocomplete="off">
                    <div class="form-group col-md-6">
                        <input d="pin_akses" name="pin_akses" type="text" placeholder="Masukan PIN" class="form-control" required>
                    </div>
                    <div class="form-group col-md-6">
                        <button class="btn btn-default btn-block" type="submit">CHECK</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>