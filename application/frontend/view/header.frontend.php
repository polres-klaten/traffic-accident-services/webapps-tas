<!--  Header Area Start Here -->
<header>
<img src="<?= $this->templatePath; ?>assets/images/header-bg.png">
	<div class="header-area hidden-xs" id="sticker">
  		<div class="container">
  			<div class="row">
  				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="main-menu-area">
						<nav>
							<ul id="nav">
								<?php
						            foreach($navbar as $nav => $value){
						                $link = $link_frontend.$value['link'];
						                echo '<li><a href="'.$link.'">'.$nav.'</a></li>';
						            }
						        ?>
							</ul>
						</nav>
					</div>
  				</div>
  			</div>
  		</div>
  	</div>
</header>
<!--  Header Area End Here -->