<!-- PopUp Form Action -->
<div class="modal fade" id="frmInputModal" tabindex="-1" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="frmInputModalLabel"></h4>
            </div>
            <form id="frmInput" onsubmit="return false;" class="form-horizontal" autocomplete="off">
                <div class="modal-body">
                    <div id="data-form-input" style="padding: 10px;"></div>
                </div>
                <div class="modal-footer">
                    <div class="loading"></div>
                    <button type="button" class="btn btn-effect-ripple btn-danger btnFooter" data-dismiss="modal">Tutup</button>
                    <button submit="button" class="btn btn-effect-ripple btn-primary btnFooter"><i class="fa fa-check"></i> Simpan</button><br>
                </div>
            </form>
        </div>
    </div>
</div>