<div class="homepage-area modul-informasi">
    <div class="container-fluid acurate">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <h1 class="huge">404</h1>
                <hr class="sm">
                <p><strong>Sorry - Page Not Found!</strong></p>
                <p>The page you are looking for was moved, removed, renamed<br>or might never existed. You stumbled upon a broken link :(</p>
            </div>
        </div>
    </div>
</div>