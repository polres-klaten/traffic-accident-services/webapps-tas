<div class="homepage-area modul-headline">
  <div class="container">
    <div class="row">
        <div class="col-md-8"><h3 class="media-heading">SATLANTAS NEWS UPDATE</h3></div>
        <form id="frmBerita" class="base-form" method="post" onsubmit="return false;" autocomplete="off">
            <div class="form-group col-md-4">
                <div class="input-group">
                    <?= Bootstrap::inputText('cari', 'text', '', 'class="form-control" placeholder="Cari Berita"'); ?>
                    <?= Bootstrap::inputKey('page', 1); ?>
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit">CARI</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <?php
        $slidePreview = '';
        $slideThumbs = '';
        foreach ($beritaTerbaru as $key => $value) {
            $slidePreview .= '<a href="'.$link_berita.$value['link_berita'].'.html"><img class="image-preview animate-fading" src="'.$link_file_image.$value['gambar_berita'].'" data-text="'.$value['judul_berita'].'" /></a>';
            $slideThumbs .= '<div class="thumbs">
                                <a href="'.$link_berita.$value['link_berita'].'.html">
                                    <div class="preview-slide col-md-4 hidden-xs">
                                        <img src="'.$link_file_image.$value['gambar_berita'].'" alt="" />
                                    </div>
                                    <div class="content-slide col-md-8">'.$value['judul_berita'].'</div>
                                </a>
                            </div>';
        }
    ?>
    <div class="row">
      <div id="slidePreview" class="col-md-8">
        <?= $slidePreview; ?>
        <div class="text-preview"></div>
      </div>
      <div id="slideThumbs" class="col-md-4">
        <?= $slideThumbs; ?>
      </div>
    </div>
  </div>
</div>
