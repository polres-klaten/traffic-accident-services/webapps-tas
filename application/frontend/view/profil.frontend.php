<div class="main-news-page-section-area">
	<div class="container">
	  <div class="row">
	    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
	      <div class="news-page-content-section-area">
	        <div class="single-news-area">
	          <div class="news-body">
	            <h3 class="news-title"><a href="#">Profil Satlantas <?= $dataProfil['nama_satuan_kerja']; ?></a></h3>
	            <p class="news-content"><?= $dataProfil['profil_satuan_kerja']; ?></p>
	          </div> 
	        </div>
	      </div>
	    </div>
	    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 hidden-lg">
	      <div class="page-sidebar-area">
	        <div class="single-sidebar">
	          <h3>Menu Utama</h3>
	          <ul>
	            <?php
		            foreach($navbar as $nav => $value){
		                $link = $link_frontend.$value['link'];
		                echo '<li><a href="'.$link.'">'.$nav.'</a></li>';
		            }
		        ?>
	          </ul>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>
</div>