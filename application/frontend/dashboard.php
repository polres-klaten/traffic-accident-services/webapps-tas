<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class dashboard extends frontend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$method = $this->method;
		$data = $this->data;
		$data['beritaTerbaru'] = $this->getBeritaTerbaru();
		$this->mod->showView('beranda', $data);
    }

}

new dashboard($this->librarys, $this->params);
?>