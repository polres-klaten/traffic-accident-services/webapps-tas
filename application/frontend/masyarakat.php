<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class masyarakat extends frontend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				$category = array();
				$params = array(
					'page' => 1,
					'cari' => '',
					'status' => '',
					'publish' => 1,
					'bulan' => intval(date('m')),
					'tahun' => intval(date('Y')),
				);
				foreach ($this->mod->pilihanBulan as $key => $value) {array_push($category, $value);}
				$data['laporanMasyarakat'] = $this->getTabelLaporanMasyarakat($params); // Aduan Masyarakat
				$data['link_file_image'] = $this->link_file_image;
				$this->mod->showView('masyarakat', $data);
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
	}

	private function chart(){
		$result = $this->getInfoGrafisLaka($_POST);
		$this->showResponse($result);
	}
	
	private function form(){
		$data['dataForm'] = $this->getDataLaporanMasyarakat($_POST['id']);
		$data['dataForm']['form_title'] = str_replace('Tambah Laporan', 'Kirim Aduan', $data['dataForm']['form_title']);
		$data['mimes_image'] = $this->file->getMimeTypes($this->mod->file_type_image);
		$data['keterangan_upload_image'] = '*) File Type : '.$this->mod->file_type_image.', Max Size : '.($this->mod->max_size / 1024 /1024).'Mb';
		$this->mod->subView('form-laporan', $data);
	}
	
	private function simpan(){
		$upload = $this->uploadImage($_FILES);
		if($upload['status'] == 'success'){
			$data = $this->getDataLaporanMasyarakat($_POST['id_laporan_masyarakat']);
			unset($data['form_title']);
			foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
			$data['gambar_laporan_masyarakat'] = empty($upload['UploadFile']) ? $data['gambar_laporan_masyarakat'] : $upload['UploadFile'];
			if(empty($data['gambar_laporan_masyarakat'])) $data['gambar_laporan_masyarakat'] = 'noimages.png';
			$result = $this->db->save_update('tref_laporan_masyarakat', $data);
			echo ($result['error']) ? 'Laporan telah dikirim' : 'Laporan gagal dikirim';
		}else{
			echo 'Lampiran gagal diunggah';
		}
	}

}

new masyarakat($this->librarys, $this->params);
?>