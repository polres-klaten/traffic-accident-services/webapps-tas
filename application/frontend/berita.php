<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class berita extends frontend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$method = $this->method;
		$data = $this->data;
		switch ($method) {
			case '':
			$session_berita = array();
				$session_berita['page'] = 1;
				$session_berita['cari'] = '';
				if(!empty($_SESSION['session_berita'])){
					$session_berita = $_SESSION['session_berita'];
				}
		
				if(isset($_POST['cari'])){
					$session_berita = $_POST;
					$_SESSION['session_berita'] = $session_berita;
				}else{
					$data['berita'] = $this->getTabelBerita($session_berita);
					$data['berita']['dataBerita'] = $data['berita']['dataTabel'];
					$data['session_berita'] = $session_berita;
					$this->mod->showView('mod-list-berita', $data);
				}
				break;
			
			default:
				$data['dataBerita'] = $this->getDetailBerita($this->method);
				if(!empty($data['dataBerita'])){
					$this->mod->showView('mod-berita', $data);
				}else{
					$this->mod->showView('error', $data);
				}
				break;
		}
    }

}

new berita($this->librarys, $this->params);
?>