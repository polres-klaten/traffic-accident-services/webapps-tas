<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class layanan extends frontend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		// $this->error_msg = $this->mod->Config['errMsg'][__CLASS__];
		$method = $this->method;
		$data = $this->data;
        switch ($method) {
            case '':
				if(empty($_POST['pin_akses']))
					$this->mod->showView('layanan', $data);
				else{
					$dataAsuransi = $this->checkPIN($_POST['pin_akses']);
					// $this->debugResponse($dataAsuransi); die;
					if(empty($dataAsuransi)){
						$this->mod->showView('layanan', $data);
					}else{
						$data['nama_pasien'] = $dataAsuransi['nama_pasien'];
						$data['status_berkas_polres'] = ($dataAsuransi['status_berkas_polres'] == 1) ? 'Sudah' : 'Belum';
						$data['status_berkas_rekam_medis'] = ($dataAsuransi['status_berkas_rekam_medis'] == 1) ? 'Sudah' : 'Belum';
						$data['status_berkas_kwintansi'] = ($dataAsuransi['status_berkas_kwintansi'] == 1) ? 'Sudah' : 'Belum';
						$data['status_berkas_surat_jaminan'] = ($dataAsuransi['status_berkas_surat_jaminan'] == 1) ? 'Sudah' : 'Belum';
						$data['status_berkas_pembayaran'] = ($dataAsuransi['status_berkas_pembayaran'] == 1) ? 'Sudah' : 'Belum';
						$this->mod->showView('result', $data);
					}
					unset($_POST['pin_akses']);
				}
                break;
            
            default:
                if((int)method_exists($this, $method) > 0){
                    $this->$method($this->id, $data);
                }else{
                    $this->mod->showView('page/error', $data);
                }
                break;
        } 
	}

}

new layanan($this->librarys, $this->params);
?>