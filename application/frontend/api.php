<?php
/**
 * CMSDuz v1
 *
 * @Author  	: M. Hanif Afiatna <hanif.softdev@gmail.com>
 * @Since   	: version 4.1.0
 * @Date		: 29 April 2017
 * @package 	: system
 * @Description : 
 */

class api extends frontend{

    public function __construct($librarys, $params){
		parent::__construct($librarys, $params);
		$this->errorMsg = array('status' => 'error', 'data' => 'missing parameter...');
		$method = $this->method;
		$data = $this->data;
		switch ($method) {
			case '':
				$this->showResponse($this->errorMsg);
				break;
			
			default:
				if((int)method_exists($this, $method) > 0){
					$this->$method($this->id, $data);
				}else{
					$this->showResponse($this->errorMsg);
				}
				break;
		}
	}
	
	private function listBerita(){
		$dataBerita = $this->getTabelBerita($_POST);
		foreach ($dataBerita['dataTabel'] as $key => $value) {
			$dataBerita['dataTabel'][$key]['short_berita'] = substr(strip_tags($value['isi_berita']), 0, 200).'...';
			$dataBerita['dataTabel'][$key]['gambar_berita'] = $this->link_file_image.$value['gambar_berita'];
			$dataBerita['dataTabel'][$key]['datetime'] = FUNC::tanggal($value['datetime'], 'long_date');
		}
		$this->errorMsg = array('status' => 'success', 'data' => $dataBerita);
		$this->showResponse($this->errorMsg);
	}

	private function listLaporanMasyarakat(){
		$_POST['tahun'] = date('Y');
		$_POST['bulan'] = '';
		$_POST['status'] = '';
		$_POST['publish'] = '';
		$dataLaporan = $this->getTabelLaporanMasyarakat($_POST);
		foreach ($dataLaporan['dataTabel'] as $key => $value) {
			$dataLaporan['dataTabel'][$key]['gambar_laporan_masyarakat'] = $this->link_file_image.$value['gambar_laporan_masyarakat'];
			$dataLaporan['dataTabel'][$key]['tanggal_laporan_masyarakat'] = FUNC::tanggal($value['tanggal_laporan_masyarakat'], 'long_date');
		}
		unset($dataLaporan['query']);
		$this->errorMsg = array('status' => 'success', 'data' => $dataLaporan);
		$this->showResponse($this->errorMsg);
	}

	private function formLaporanMasyarakat(){
		$dataForm = $this->getDataLaporanMasyarakat();
		$dataForm['files'] = '';
		unset($dataForm['balasan_laporan_masyarakat']);
		unset($dataForm['tanggal_laporan_masyarakat']);
		unset($dataForm['status_laporan_masyarakat']);
		unset($dataForm['publish_laporan_masyarakat']);
		unset($dataForm['form_title']);
		$this->errorMsg = array('status' => 'success', 'data' => $dataForm);
		$this->showResponse($this->errorMsg);
	}

	private function sendLaporanMasyarakat(){
		$upload = $this->uploadImage($_FILES);
		if($upload['status'] == 'success'){
			$data = $this->getDataLaporanMasyarakat();
			unset($data['form_title']);
			foreach($data as $key => $value){if(isset($_POST[$key])) $data[$key] = $_POST[$key];}
			$data['gambar_laporan_masyarakat'] = empty($upload['UploadFile']) ? $data['gambar_laporan_masyarakat'] : $upload['UploadFile'];
			if(empty($data['gambar_laporan_masyarakat'])) $data['gambar_laporan_masyarakat'] = 'noimages.png';
			$result = $this->db->save_update('tref_laporan_masyarakat', $data);
			$message = ($result['error']) ? 'Laporan telah dikirim' : 'Laporan gagal dikirim';
		}else{
			$message = 'Lampiran gagal diunggah';
		}
		$this->errorMsg = array('status' => 'success', 'data' => $message);
		$this->showResponse($this->errorMsg);
	}

	private function checkLaporanAsuransi(){
		$dataAsuransi = $this->checkPIN($_POST['pin_akses']);
		if(empty($dataAsuransi)){
			$this->errorMsg = array('status' => 'error', 'data' => array());
		}else{
			$data['nama_pasien'] = $dataAsuransi['nama_pasien'];
			$data['status_berkas_polres'] = ($dataAsuransi['status_berkas_polres'] == 1) ? 'Sudah' : 'Belum';
			$data['status_berkas_rekam_medis'] = ($dataAsuransi['status_berkas_rekam_medis'] == 1) ? 'Sudah' : 'Belum';
			$data['status_berkas_kwintansi'] = ($dataAsuransi['status_berkas_kwintansi'] == 1) ? 'Sudah' : 'Belum';
			$data['status_berkas_surat_jaminan'] = ($dataAsuransi['status_berkas_surat_jaminan'] == 1) ? 'Sudah' : 'Belum';
			$data['status_berkas_pembayaran'] = ($dataAsuransi['status_berkas_pembayaran'] == 1) ? 'Sudah' : 'Belum';
			$this->errorMsg = array('status' => 'success', 'data' => $data);
		}
		$this->showResponse($this->errorMsg);
	}

}

new api($this->librarys, $this->params);
?>