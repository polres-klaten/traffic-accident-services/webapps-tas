(function ($) {
 "use strict";

/*----------------------------
 owl active
------------------------------ */  
  // $('.homepage-our-service').owlCarousel({
  //   autoPlay: false, 
  //   slideSpeed:2000,
  //   pagination:false,
  //   navigation:true,    
  //   items : 4,
  //   // /* transitionStyle : "fade", */     [This code for animation ] 
  //   navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  //   itemsDesktop : [1199,4],
  //   itemsDesktopSmall : [980,3],
  //   itemsTablet: [768,2],
  //   itemsMobile : [479,1],
  // });
/*----------------------------
 Testimonial active
------------------------------ */  
  // $('.homepage-testimonial').owlCarousel({
  //   autoPlay: false, 
  //   slideSpeed:2000,
  //   pagination:true,
  //   navigation:false,   
  //   items : 2,
  //   /* transitionStyle : "fade", */    /* [This code for animation ] */
  //   navigationText:[""],
  //   itemsDesktop : [1199,2],
  //   itemsDesktopSmall : [980,2],
  //   itemsTablet: [768,2],
  //   itemsMobile : [479,1],
  // });
/*----------------------------
 Home page Team active
------------------------------ */  
// $('.member-slideshow img').on('click', function(event) {
//     var displayTarget = $("#product-1");
//     displayTarget.find('.single-team').removeClass('active');
//     var id = $(this).attr('data-id');
//     var targetClass = ".product-gallery-img-"+id;
//     console.log(targetClass);
//     displayTarget.find(targetClass).addClass('active');

// });
     
/*--------------------------
 scrollUp
---------------------------- */ 
  $.scrollUp({
        scrollText: '<i class="fa fa-angle-up"></i>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    }); 
/*----------------------------
 News active
------------------------------ */  
  $('.homepage-total-news-area').owlCarousel({
    autoPlay: false, 
    slideSpeed:2000,
    pagination:false,
    navigation:true,    
    items : 5,
    // /* transitionStyle : "fade", */     [This code for animation ] 
    navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
    itemsDesktop : [1199,3],
    itemsDesktopSmall : [980,3],
    itemsTablet: [768,2],
    itemsMobile : [479,1],
  });
  
//jquery Stiky Menu activation code
   $(window).on('scroll', function(){
      if( $(window).scrollTop()>100 ){
        $('#sticker').addClass('stick');
      } else {
        $('#sticker').removeClass('stick');
      }
    });
/*----------------------------
Business active
------------------------------ */  
 // $('.total-business').owlCarousel({
  // autoPlay: false, 
  // slideSpeed:2000,
  // pagination:false,
  // navigation:true,    
  // items : 4,
   /* transitionStyle : "fade", */    /* [This code for animation ] */
  // navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  // itemsDesktop : [1199,3],
  // itemsDesktopSmall : [991,2],
  // itemsTablet: [768,2],
  // itemsMobile : [480,1],
 // });

/*----------------------------
  About Us2 active
 ------------------------------ */  
   // $('.about-us-slider').owlCarousel({
   //  autoPlay: true, 
   //  slideSpeed:2000,
   //  pagination:false,
   //  navigation:true,    
   //  items : 1,
   //  /* transitionStyle : "fade", */    /* [This code for animation ] */
   //  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
   //  itemsDesktop : [1199,1],
   //  itemsDesktopSmall : [980,1],
   //  itemsTablet: [768,1],
   //  itemsMobile : [479,1],
   // });

/*----------------------------
 About Us3 active
------------------------------ */  
  // $('.about-us-3-area').owlCarousel({
  //   autoPlay: true, 
  //   slideSpeed:2000,
  //   pagination:true,
  //   navigation:false,    
  //   items : 1,
  //   /* transitionStyle : "fade", */    /* [This code for animation ] */
  //   navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  //   itemsDesktop : [1199,1],
  //   itemsDesktopSmall : [980,1],
  //   itemsTablet: [768,1],
  // itemsMobile : [479,1],
  // });

/*---------------------
accordion
--------------------- */ 

// $('#accordion').children('.panel').children('.panel-collapse').each(function(){
//   if($(this).hasClass('in')){
//     $(this).parent('.panel').children('.panel-heading').addClass('active');
//   }
// });
// $('#accordion')
//   .on('show.bs.collapse', function(e) {
//   $(e.target).prev('.panel-heading').addClass('active');
//   })
//   .on('hide.bs.collapse', function(e) {
//   $(e.target).prev('.panel-heading').removeClass('active');
//   });

/*----------------------------
Business active
------------------------------ */  
  // $('.homepage-team').owlCarousel({
  // autoPlay: false, 
  // slideSpeed:2000,
  // pagination:false,
  // navigation:true,    
  // items : 4,
  // /* transitionStyle : "fade", */    /* [This code for animation ] */
  // navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  // itemsDesktop : [1199,4],
  // itemsDesktopSmall : [980,3],
  // itemsTablet: [768,2],
  // itemsMobile : [479,1],
  // });
  
/*----------------------------
 Partner Logo
------------------------------ */ 

  // $('.client-logo').owlCarousel({
  //   autoPlay: false, 
  //   slideSpeed:2000,
  //   pagination:false,
  //   navigation:true,    
  //   items : 5,
  //   /* transitionStyle : "fade", */    /* [This code for animation ] */
  //   navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  //   itemsDesktop : [1199,5],
  //   itemsDesktopSmall : [980,3],
  //   itemsTablet: [768,3],
  //   itemsMobile : [479,1],

  // });

/*----------------------------
 Home 4 News active
------------------------------ */  
  // $('.homepage-total-news-area2').owlCarousel({
  //   autoPlay: false, 
  //   slideSpeed:2000,
  //   pagination:false,
  //   navigation:true,    
  //   items : 1,
  //   /* transitionStyle : "fade", */     [This code for animation ] 
  //   navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  //   itemsDesktop : [1199,1],
  //   itemsDesktopSmall : [980,1],
  //   itemsTablet: [768,1],
  //   itemsMobile : [479,1],
  // });

/*-------------------------------------
Jquery Fixed Header Menu 
-----------------------------------*/
  $(window).scroll(function() {
    var s = $("#sticker");
    var w = $(".wrapper");
    //alert(pos.top);
    var windowpos = $(window).scrollTop();
    if(windowpos > 0){
      s.addClass("stick");
      var h;
      if($(window).width() > 767){
        h = $(".header-top-area").outerHeight();
      }else{
        $(".mean-container .mean-bar").css('position', 'fixed');
        h = $(".mean-bar").outerHeight();
      }
      w.css('padding-top', h+"px");
    } else {
      $(".mean-container .mean-bar").css('position', 'relative');
      s.removeClass("stick");
      w.css('padding-top', 0);
    }
  });

/***************************************
Contact Form activation code
***************************************/
if($('#contact-form').length){
  $('#contact-form').validator().on('submit', function (e) {
       var $this = $(this),
          $target = $(".form-response");
     if (e.isDefaultPrevented()) {
        $target.html("<div class='alert alert-success'><p>Please select all required field.</p></div>");
     } else {
      var name = $("#form-name").val();
      var email = $("#form-email").val();
      var message = $("#form-message").val();
      // ajax call
       $.ajax({
         url: 'php/form-process.php',
         type: 'POST',
         data: "name=" + name + "&email=" + email + "&message=" + message,
         beforeSend: function() {
            $target.html("<div class='alert alert-info'><p>Loading ...</p></div>");
        },
         success: function( text ){
                if(text == "success"){
                    $this[0].reset();
                    $target.html("<div class='alert alert-success'><p>Message Has Been Sent.</p></div>");
                } else {
                    $target.html("<div class='alert alert-success'><p>"+text+"</p></div>");
                }
             }
       });

       return false;
     }
  });
}

$('.total-business').owlCarousel({
  autoPlay: false, 
  slideSpeed:2000,
  pagination:false,
  navigation:true,    
  items : 4,
   /* transitionStyle : "fade", */    /* [This code for animation ] */
  navigationText:["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
  itemsDesktop : [1199,3],
  itemsDesktopSmall : [991,2],
  itemsTablet: [768,2],
  itemsMobile : [480,1],
});

$(function () {
    var url = window.location.toString();
    $('ul#nav a').each(function() {
      if(url === this.href) $(this).addClass('active');
    });
});


})(jQuery); 

/*--------------------------
 slideNews by Hanif Ganteng
---------------------------- */ 
var slideIndex = 0;
if(document.getElementById("slidePreview") !== null){
  slideSize();
  slideNews();
}

function slideSize() {
  var slidePreview = document.getElementById("slidePreview");
  var slideThumbs = document.getElementById("slideThumbs").clientHeight;
  slidePreview.style.height = "350px"; 
  // if(screen.width > 480) slidePreview.style.height = slideThumbs+"px";
  // else slidePreview.style.height = "auto"; 
}
function slideNews() {
  var img = document.getElementsByClassName("image-preview");
  var txt = document.getElementsByClassName("text-preview");
  var thumbs = document.getElementsByClassName("thumbs");
  for(var i = 0; i < img.length; i++){
    img[i].style.display = "none";
    thumbs[i].className = "thumbs";
  }
  if(slideIndex > img.length - 1){slideIndex = 0;}
  var dataText = img[slideIndex].getAttribute("data-text");
  img[slideIndex].style.display = "block";
  thumbs[slideIndex].className += " active";
  txt[0].innerHTML = "<span class='animate-top'>"+dataText+"</span>";
  slideIndex++;
  setTimeout(slideNews, 3000);
}
window.addEventListener("slideSize", onresize);

/*--------------------------
 Admin Path
---------------------------- */ 
$(document).on("click", "#adm", function() {
  $.ajax({
    url: this.getAttribute("data-url"),
    data: {id: ""},
    type: "post",
    datatype: "json",
    success: function(result){
      window.open(result.url, "_blank");},
    async: false
  })
});

/*--------------------------
 Berita
---------------------------- */ 
var link_berita = "";
$(document).on("change", "#cari", function() {
    loadBerita();
});
$(document).on("click", ".paging", function(){
    paggingBerita($(this).attr("number-page"));
});
$(document).on("submit", "#frmBerita", function() {
    loadBerita();
});
function linkBerita(link){
  link_berita = link;
};
function loadBerita(){
  $.post(link_berita, $("#frmBerita").serialize(), function (data){
      window.location = link_berita;
  });
};
function paggingBerita(number){
  $("#page").val(number);
  loadBerita();
  $(document).scrollTop(0);
};

/*--------------------------
 Member
---------------------------- */ 
var link_member = "";
$(document).on("submit", "#frmMember", function() {
    checkLaporan();
});
function linkMember(link){
  link_member = link;
};
function checkLaporan(){
  $.post(link_member+"/check", $("#frmMember").serialize(), function (data){
      if(data.status === "success"){
        window.location.reload();
      }else{
        alert(data.message);
      }
  }, "json");
};