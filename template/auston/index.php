<!DOCTYPE html>
<html class="no-js">
    <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= $this->web_title; ?></title>
    <meta name="description" content="<?= $this->web_description; ?>" />
    <meta name="keywords" content="" />
    <meta name="author" content="<?= $this->web_author; ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon -->        
    <link rel="shortcut icon" href="<?= $this->templatePath; ?>assets/images/favicon.ico">
    <!-- all css here -->
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/css/owl.carousel.css">
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/css/style.css">
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/css/custom.css">
    <link rel="stylesheet" href="<?= $this->templatePath; ?>assets/js/bootstrap-select/css/bootstrap-select.min.css">
    <!-- all js here -->
    <script src="<?= $this->templatePath; ?>assets/js/vendor/modernizr-2.8.3.min.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/vendor/jquery-1.12.0.min.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/owl.carousel.min.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/jquery.scrollUp.min.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/highcharts/highcharts.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/highcharts/exporting.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/highcharts/export-data.js"></script>
    <script src="<?= $this->templatePath; ?>assets/js/bootstrap-select/js/bootstrap-select.min.js"></script>
    </head>
    <body>
    <?php $this->subView('header', $data); ?>
    <?php require_once $this->viewPath; ?>
    <?php $this->subView('footer', $data); ?>
    <script src="<?= $this->templatePath; ?>assets/js/main.js"></script>
    <script>linkBerita("<?= $link_frontend.'berita'; ?>");</script>
    <script>linkMember("<?= $link_frontend.'member'; ?>");</script>
    </body>
</html>