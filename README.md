# Aplikasi TAS Polres Klaten
## Asset:
- [File Resource]()
- [Mockup]()

## Backup Database
```sh
docker exec mysql.server /usr/bin/mysqldump -u root --password=root dbweb_tas > backup/database.sql
```

## Restore Database
```sh
cat backup/database.sql | docker exec -i mysql.server /usr/bin/mysql -u root --password=root dbweb_tas
```